import {action, configure, runInAction, observable, toJS, } from 'mobx';
import {
  changeQuestionRequest,
  createQuestionRequest,
  deleteQuestionRequest,
  getQuestionsRequest,
  filterQuestionsRequest
} from '../requests/riskMapIssuesRequests';
import {valueForSelectAnswers} from "../constants/selectItems";
import {isAdminGetFromLocal, removeCreateEditFromRoutePage, setParams} from "../utils";
import * as keys from '../keys';

configure({enforceActions: 'observed'});

export default class RiskMapIssuesStore {

  @observable isSuperuser = isAdminGetFromLocal();
  @observable questions = [];
  @observable request = false;
  @observable activeQuestion = undefined;
  @observable activeQuestionName = '';
  @observable activeAnswers = [];
  @observable visibleModalCreateAnswer = false;
  @observable createQuestionObject = {};
  @observable selectedAnswerType = undefined;
  @observable searchValue = '';
  @observable showSaveButton = false;
  @observable editItem = false;
  @observable page = 1;
  @observable totalPages = 1;
  @observable showErrorModal = false;
  @observable errorText = '';

  @action
  reset = () => {
  // this.questions = [];
  this.request = false;
  this.activeQuestion = undefined;
  this.activeQuestionName = '';
  this.activeAnswers = [];
  this.visibleModalCreateAnswer = false;
  this.createQuestionObject = {};
  this.selectedAnswerType = undefined;
  this.searchValue = '';
  this.showSaveButton = false;
  this.editItem = false;
  this.error = '';
  }


  @action
  toggleModalCreateAnswer = (value) => {


    runInAction(() => {
      this.visibleModalCreateAnswer = value;
    });
  }

  @action
  setSelectedAnswerType = selectedAnswerType => {

    if (selectedAnswerType === valueForSelectAnswers[3].value) {
      this.activeAnswers = [];
    }
    this.selectedAnswerType = selectedAnswerType;
  }

  @action
  setShowModalError = show => this.showErrorModal = show;

  @action
  deleteAnswer = async (id, i) => {

    //если вопрос только у юзера (не отправлен на бек)
    if(id === undefined){
      runInAction(()=>{
        this.activeAnswers.splice(i,1);
      })
      return ;

    }

    this.request = true;

    const currentQuestionIndex = this.questions.findIndex(item => item.id === this.activeQuestion);

    if (currentQuestionIndex === -1) {
      const answersTemp = this.activeAnswers.filter(item => item.id !== id);
      this.activeAnswers = answersTemp;
    } else {
      const currentQuestion = this.questions[currentQuestionIndex];
      const newAnswers = currentQuestion.answers.filter(item => item.id !== id);
      currentQuestion.answers = newAnswers;
      this.activeAnswers = newAnswers;
      const result = await changeQuestionRequest(currentQuestion);
      runInAction(() => {
        this.questions[currentQuestionIndex] = result;
      });
    }

    runInAction(()=>{
      this.request = false;
    });
  }

  @action
  changeQuestion = async () => {
    this.request = true;
    try {
      await changeQuestionRequest({});
    } catch (error) {
      if (error && error.errors && error.errors) {
        const errorText = Object.values(error.errors)[0][0];
        runInAction(() => {
          this.errorText = errorText;
          this.showErrorModal = true;
        })
      }
    } finally {
      this.request = false;
    }
  }

  @action
  createQuestionObjectLocal = name => {
    this.createQuestionObject.name = name;
    this.activeQuestionName = name;
  }

  @action
  createQuestion = async () => {
    this.request = true;

    const name = this.activeQuestionName;
    const question_type = this.selectedAnswerType;
    const answersCopy = toJS(this.activeAnswers);
    let answers = [];

    if( this.activeQuestion >=0){
      const copyObj = toJS(this.questions.find(item => item.id ===this.activeQuestion));
      const selectedAnswerType = copyObj.question_type;

      if (selectedAnswerType === valueForSelectAnswers[2].value) {
        answersCopy.map(item => {
          if (!item.image) item.image = null;
          answers.push(item);
          return item;
        })
      } else {
        answers = answersCopy;
      }

      try{
        copyObj.answers = answers;
        // const id = this.activeQuestion, name = this.activeQuestionName, help_text=, question_type, order , answers
        const resp = await changeQuestionRequest(copyObj);
        const i = this.questions.findIndex(item => item.id === resp.results.id);
        runInAction(()=>{
          this.questions[i]=resp.result;
        });

      } catch (error) {
        if (error && error.errors && error.errors) {
          const errorText = Object.values(error.errors)[0][0];
          runInAction(() => {
            this.errorText = errorText;
            this.showErrorModal = true;
          })
        }
      }
      finally {
        runInAction(()=>{
          this.showSaveButton = false;
          this.request = false;
        })
        return ;
      }

    }

    if (this.selectedAnswerType === valueForSelectAnswers[2].value) {
      answersCopy.map(item => {
        if (!item.image) item.image = null;
        answers.push(item);
        return item;
      })
    } else {
      answers = answersCopy;
    }

    const objectTemp = {
      name,
      question_type,
      answers
    };

    try {

      const result = await createQuestionRequest(objectTemp);
      runInAction(() => {
        this.questions.push(result);
        this.activeAnswers = [];
        this.activeQuestion = undefined;
        this.createQuestionObject = {};
        this.activeQuestionName = '';
      });
    } catch (error) {
      if (error && error.errors && error.errors) {
        const errorText = Object.values(error.errors)[0][0];

        runInAction(() => {
          this.errorText = errorText;
          this.showErrorModal = true;
        })
      }
    } finally {
      runInAction(() => {
        this.request = false;
      });
    }


  }

  @action
  resetActiveQuestion = () => {
    this.activeQuestion = undefined;
    this.activeQuestionName = '';
    this.activeAnswers = [];
    this.visibleModalCreateAnswer = false;
    this.createQuestionObject = {};
    this.showSaveButton = false;
    this.selectedAnswerType = undefined;
  }

  @action
  changeSearch =async (searchText, page = 1) => {
    this.searchValue = searchText;
    this.page = page;
    this.request = true;

    setParams('page',page);
    setParams('s',searchText);

    try{
      const response = await filterQuestionsRequest({name: searchText, page});
      runInAction(()=>{
        this.totalPages = response.total_pages;
        this.questions = response.results;
        this.isSuperuser = response.is_superuser;
      })
    } catch (error) {
      if (error && error.errors && error.errors) {
        const errorText = Object.values(error.errors)[0][0];
        runInAction(() => {
          this.errorText = errorText;
          this.showErrorModal = true;
        })
      }
    } finally {
      runInAction(() => {
        this.request = false;
      });
    }
  }

  @action
  deleteQuestion = async (id) => {
    if (id === undefined) {
      this.createQuestionObject = {};
      this.activeQuestionName = '';
      this.activeQuestion =  undefined;
      this.activeAnswers = [];
      return;
    }

    this.request = true;

    try {
      await deleteQuestionRequest({id});

      const data = this.questions.filter(item => item.id !== id);
      removeCreateEditFromRoutePage(keys.RISK_QUESTIONS);
      runInAction(() => {
        this.activeQuestion = undefined;
        this.activeQuestionName = '';
        this.questions = data;
        this.activeAnswers = [];
      });

    } catch (error) {
      if (error && error.errors && error.errors) {
        const errorText = Object.values(error.errors)[0][0];
        runInAction(() => {
          this.errorText = errorText;
          this.showErrorModal = true;
        })
      }
    } finally {
      runInAction(() => {
        this.request = false;
      });
    }


  }

  @action
  getQuestion = async (page = 1) => {
    this.request = true;
    this.page = page;
    setParams('page',page);
    try {
      const response = await getQuestionsRequest({page});
      if (response && response.results) {
        runInAction(() => {
          this.totalPages = response.total_pages;
          this.questions = response.results;
          this.isSuperuser = response.is_superuser;
        });
      }
    } catch (error) {
      if (error && error.errors && error.errors) {
        const errorText = Object.values(error.errors)[0][0];
        runInAction(() => {
          this.errorText = errorText;
          this.showErrorModal = true;
        })
      }
    } finally {
      runInAction(() => {
        this.showSaveButton = false;
        this.request = false;
      });
    }
  }


  @action
  createAnswer = (text, image) => {

    const obj = {
      answer: text,
      image
    }

    this.activeAnswers.push(obj);
    this.showSaveButton = true;

  }

  @action
  setActiveQuestion = id => {
    this.activeQuestion = Number(id);
    this.showSaveButton = false;

    const activeIndex = this.questions.findIndex(item => item.id === Number(id));
    if (activeIndex > -1) {
      this.activeQuestionName = this.questions[activeIndex].name;
      this.activeAnswers = this.questions[activeIndex].answers;
      this.selectedAnswerType = this.questions[activeIndex].question_type;
    } else {
      this.activeQuestionName = undefined;
      this.activeAnswers = [];
      this.selectedAnswerType = undefined;
    }
  };

}

