import {action, configure, runInAction, observable} from 'mobx';
import {
  getAllFacilitiesRequest,
  createFacilitiesRequest,
  updateFacilitiesRequest,
  deleteFacilitieRequest,
  searchFacilitiesRequest,
} from '../requests/geopositionRequests';
import {isAdminGetFromLocal, setParams} from '../utils';

configure({enforceActions: 'observed'});


export default class GeopositionStore {

  @observable request = false;
  @observable isSuperuser = isAdminGetFromLocal();
  @observable facilities = []
  @observable activeFacilities = undefined;
  @observable creatingFacilities = undefined;
  @observable editingFacilities = undefined;
  @observable idEditFacilities = undefined;
  @observable nameEditFacilities = undefined;
  @observable countryEditFacilities = undefined;
  @observable regionEditFacilities = undefined;
  @observable regionLongFacilities = undefined;
  @observable regionLatFacilities = undefined;
  @observable searchText = '';
  @observable page = 1;
  @observable totalPages = 1;
  @observable errorText = '';
  @observable showErrorModal = false;


  @action
  setActiveFacilities = ({id}) => { //метод установки активного блока


    this.activeFacilities = id;
    this.editingFacilities = false;
    this.creatingFacilities = false;

  }

  @action
  reset = () => {
    this.activeFacilities = undefined;
    this.creatingFacilities = undefined;
    this.editingFacilities = undefined;
    this.idEditFacilities = undefined;
    this.nameEditFacilities = undefined;
    this.countryEditFacilities = undefined;
    this.regionEditFacilities = undefined;
    this.regionLongFacilities = undefined;
    this.regionLatFacilities = undefined;
  }

  @action
  setShowModalError = show => this.showErrorModal = show;

  @action
  setSearchField = async (text, page = 1) => {

    this.searchText = text;
    this.page = Number(page);
    this.request = true;

    setParams('s', text);
    setParams('page', page);
    try {
      const response = await searchFacilitiesRequest({name: text, page});
      runInAction(() => {
        this.totalPages = response.total_pages;
        this.facilities = response.results;
        this.isSuperuser = response.is_superuser;
      });
    } catch (error) {
      if (error && error.errors && error.errors) {
        const errorText = Object.values(error.errors)[0][0];
        runInAction(() => {
          this.errorText = errorText;
          this.showErrorModal = true;
        })
      }
    } finally {
      runInAction(() => {
        this.request = false;
      });
    }
  }

  @action
  getAllFacilities = async (page = 1) => { //получения всей геопозиций

    this.request = true;
    this.page = Number(page);
    setParams('page', page);

    try {
      const result = await getAllFacilitiesRequest({page});

      runInAction(() => {
        this.facilities = result.results;
        this.totalPages = result.total_pages;
        this.isSuperuser = result.is_superuser;
      });
    } catch (error) {
      if (error && error.errors && error.errors) {
        const errorText = Object.values(error.errors)[0][0];
        runInAction(() => {
          this.errorText = errorText;
          this.showErrorModal = true;
        })
      }
    } finally {
      runInAction(() => {
        this.request = false;
      });
    }

  }
  @action
  createFacilities = (value) => { //открытие формы создания новой геопозиции
    this.creatingFacilities = value;
    this.editingFacilities = false;
    this.activeFacilities = undefined;
  }

  @action
  addFacilities = async (name, country, region, long, lat) => { //создание новой геопозиции
    this.request = true;
    try {
      const result = await createFacilitiesRequest({name, country, region, long, lat});
      runInAction(() => {
        this.creatingFacilities = false;
        this.facilities.push(result);
      });
    } catch (error) {
      if (error && error.errors && error.errors) {
        const errorText = Object.values(error.errors)[0][0];
        runInAction(() => {
          this.errorText = errorText;
          this.showErrorModal = true;
        })
      }
    } finally {
      runInAction(() => {
        this.request = false;
      })
    }

  }

  @action
  updateFacilities = async (name, country, region, long, lat,) => { //именение геопозиции
    this.request = true;

    let id = this.idEditFacilities;
    try {
      const result = await updateFacilitiesRequest({id, name, country, region, long, lat,});
      const index = this.facilities.findIndex(item => item.id === id);

      runInAction(() => {
        this.editingFacilities = false;
        this.creatingFacilities = false;
        if (index > -1) {
          this.facilities[index] = result;
        }

      });

    } catch (error) {
      if (error && error.errors && error.errors) {
        const errorText = Object.values(error.errors)[0][0];
        runInAction(() => {
          this.errorText = errorText;
          this.showErrorModal = true;
        })
      }
    } finally {
      runInAction(() => {
        this.request = false;
      })
    }

  }


  @action
  editFacilities = ({id}) => { //открытие формы редактирования геопозиции

    const facilityTemp = this.facilities.find(item => item.id === id);

    this.editingFacilities = true;
    this.creatingFacilities = false;

    if (facilityTemp) {
      const {name, country, region, long, lat} = facilityTemp;
      this.idEditFacilities = id;
      //фиксируем id  и данные геопозии,которая оедактируется
      this.nameEditFacilities = name;
      this.countryEditFacilities = country;
      this.regionEditFacilities = region;
      this.activeFacilities = id;
      this.regionLongFacilities = long;
      this.regionLatFacilities = lat;
    }
  }

  @action
  deleteFacility = async (id) => {
    this.request = true;

    try {
      await deleteFacilitieRequest({id});
      const indexOfItem = this.facilities.findIndex(item => item.id === id);
      if (indexOfItem > -1) {
        runInAction(() => {
          this.facilities.splice(indexOfItem, 1);
        });
      }
    } catch (error) {
      if (error && error.errors && error.errors) {
        const errorText = Object.values(error.errors)[0][0];
        runInAction(() => {
          this.errorText = errorText;
          this.showErrorModal = true;
        })
      }
    } finally {
      runInAction(() => {
        this.request = false;
      })
    }
  }


}

