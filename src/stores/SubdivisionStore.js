import { action, configure, runInAction, observable} from 'mobx';
import {
  createSubdivisionRequest,
  getAllSubdivisionsRequest,
  updateSubdivisionsRequest,
  deleteSubdivisionRequest,
  searchTextRequest,
  uploadCSVSubdivisionsRequest,
} from '../requests/subdivisionRequests';
import {message} from "antd";
import * as keys from '../keys';
import {setParams, getParams, setEditPage, isAdminGetFromLocal,} from "../utils";


configure({ enforceActions: 'observed' });

export default class SubdivisionStore {

  @observable isSuperuser = isAdminGetFromLocal();
  @observable request = false;
  @observable subdivisions = [];
  @observable creatingSubdivision = undefined;
  @observable editingSubdivision = undefined;
  @observable activeIndex = undefined;
  @observable activeSubdivisionCode = undefined;
  @observable activeSubdivisionName = undefined;
  @observable activeSubdivision = undefined;
  @observable showErrorModal = false;
  @observable errorText = ''; 
  @observable searchText = '';

  @observable parametr = 0;
  @observable page = 1;
  @observable totalPages = 1;

  constructor() {
    let params = getParams();
    this.parametr = params.id;
  }

  @action
  uploadCSVSubdivisions = async (file) => {

    this.request = true;
    try{
      await uploadCSVSubdivisionsRequest(file);
      message.success(`Подразделения в процесе создания, это может занять некоторое время`);
    } catch (error) {
      if (error && error.errors && error.errors) {
        const errorText = Object.values(error.errors)[0][0];
        runInAction(() => {
          this.errorText = errorText;
          this.showErrorModal = true;
        })
      }
    } finally {
      runInAction(()=>{
        this.request = false;
      })
    }
  }



  @action
  setSearchField = async (text, page = 1) => {
    this.request = true;
    this.searchText = text;
    this.page = Number(page);
    setParams('s', text);
    setParams('page',page);

    try{
      const response = await searchTextRequest({name: text, page});
      runInAction(() => {
        this.subdivisions = response.results;
        this.totalPages = response.total_pages;
        this.isSuperuser = response.is_superuser;
      });
    } catch (error) {

    } finally {
      runInAction(()=>{
        this.request = false;
      });
    }



  }

  @action
  deleteSubdivision = async (key) => {

    this.activeSubdivision = key;
    this.request = true;
    this.activeIndex = this.subdivisions.findIndex(item => item.id === key);
    let id = this.subdivisions[this.activeIndex].id;

    try{
      await deleteSubdivisionRequest({id});


      runInAction(() => {
        this.subdivisions.splice(this.activeIndex, 1);
        this.creatingSubdivision = false;
        this.editingSubdivision = false;
        this.activeIndex = undefined;
        this.activeSubdivision = undefined;
        this.activeSubdivisionCode = undefined;
        this.activeSubdivisionName = undefined;

      });
    } catch (e) {

    } finally {
      runInAction(()=>{
        this.request = false;
      });
    }


  }

  @action
  editSubdivision = (id) => {
  
    this.activeSubdivision = id;
    this.creatingSubdivision = false;
    this.editingSubdivision = true;

    setEditPage(keys.SUBDIVISION, id);


    this.activeIndex = this.subdivisions.findIndex(item => item.id === id);
    if (this.activeIndex > -1) {
      this.activeSubdivisionCode = this.subdivisions[this.activeIndex].code;
      this.activeSubdivisionName = this.subdivisions[this.activeIndex].name;
     

    } else {
      this.activeSubdivisionCode = undefined;
      this.activeSubdivisionName = undefined;
    }

  }

  @action
  saveSubdivision = async (code, name) => {
    try {
      let id = this.subdivisions[this.activeIndex].id;
      const response = await updateSubdivisionsRequest({ id, code, name, });

      runInAction(() => {
        this.subdivisions[this.activeIndex] = response;
        this.editingSubdivision = false;
        //this.activeIndex = id;
        this.activeSubdivisionCode = code;
        this.activeSubdivisionName = name;
      });
    } catch (error) {
      //const errorText = error.errors.code;
      const errorText = 'Произошла ошибка! Возможно данные уже существуют. Попробуйте повторить попытку.';
      runInAction(() => {
        this.errorText = errorText;
        this.showErrorModal = true;
      })

    } finally {
      runInAction(()=>{
        this.request = false;
      });
    }

  }
  
  
  @action
  createSubdivision = (value) => {
    this.creatingSubdivision = true;
    this.activeSubdivision = undefined;
    this.editingSubdivision = false;
  }

  @action
  cancelCreateSubdivision = (value) => {
    this.creatingSubdivision = false;
    this.activeSubdivision = undefined;
    this.editingSubdivision = false;
  }


  @action
  setActiveSubdivision = id => {
    this.creatingSubdivision = false;
    this.editingSubdivision = false;
    this.activeSubdivision = id;

    this.activeIndex = this.subdivisions.findIndex(item => item.id === id);
    if (this.activeIndex > -1) {

      this.activeSubdivisionCode =this.subdivisions[this.activeIndex].code;
      this.activeSubdivisionName = this.subdivisions[this.activeIndex].name;

    } else {
      this.activeSubdivisionCode = undefined;
      this.activeSubdivisionName = undefined;
    }
  };

  @action
  setShowModalError = show => this.showErrorModal = show;

  @action
  addNewSubdivision = async (code, name) => {

    this.request = true;
    this.errorText = '';


    try {
      const response = await createSubdivisionRequest({ code, name });
      runInAction(() => {
        this.creatingSubdivision = false;
        this.activeSubdivisionCode = undefined;
        this.activeSubdivisionName = undefined;
        this.subdivisions.push(response);

      });
    } catch (error) {
      //const errorText = error.errors.code ;
      const errorText = 'Произошла ошибка! Возможно данные уже существуют. Попробуйте повторить попытку.';
      runInAction(() => {
        this.errorText = errorText;
        this.showErrorModal = true;
      })
    } finally {
      runInAction(()=>{
        this.request = false;
      });
    }
  }

  @action
  getAllSubdivisions = async (page = 1) => {

    this.request = true;
    this.page = Number(page);
    setParams('page',page);
    try{
      const response = await getAllSubdivisionsRequest({page: this.page});
      runInAction(() => {
        this.subdivisions = response.results;
        this.totalPages = response.total_pages;
        this.isSuperuser = response.is_superuser;
      });
    } catch (e) {

    } finally {
      runInAction(()=>{
        this.request = false;
      });
    }
  }
  

 

}

