import {action, configure, runInAction, observable} from 'mobx';
import {persist} from 'mobx-persist'
import {
    signInRequest,
    resetPasswordRequest,
    getRegistrationDataRequest,
    signUpRequest,
    getProfileRequest,
    getPositionNameRequest,
} from '../requests/loginRequests';
import * as keys from '../keys';
import {replace} from "../modules/history";

configure({enforceActions: 'observed'});


export default class LoginStore {
   @persist @observable fullName= '';
   @persist @observable password= '';
   @persist @observable number= null;
   @persist @observable position= '';
   @persist @observable request= false;
   @observable refresh= '';
   @observable access= '';
   @persist @observable userId= null;
   @persist @observable isSuperuser= false;
   @observable error= '';
   @observable message= '';
   @persist ('object') @observable position_level = {};
   @persist ('object') @observable business_unit = {};
   @persist ('object') @observable branch = {};
   @observable currentUserInfo = {};
   @observable isRegistered = false;

   @observable positionNameArr= [];
   @observable positionListReal = []; //for render
   @observable selectedPosition = {}; //real data

    constructor() {
        const access = window.localStorage.getItem('access');
        const refresh = window.localStorage.getItem('refresh');

        this.access = access;
        this.refresh = refresh;

        window.addEventListener('storage', this.changeLocalStorage);
    }

    changeLocalStorage = ( ) => {
        const accessStorage = window.localStorage.getItem('access');
        const refreshStorage = window.localStorage.getItem('refresh');
        if(this.access!==accessStorage) {
            this.setAccess(accessStorage);
        }
        if(this.refresh!==refreshStorage) {
            this.setRefresh(refreshStorage);
        }
    }

    @action
    setIsRegistered = (value) => {
      this.isRegistered = value;
    }

    @action setAccess = access => {
        this.access = access;
    }

    @action setRefresh = refresh => {
        this.refresh = refresh;
    }

    @action clearMessages = () => {
        this.message = '';
        this.error = '';
    }

    @action 
    resetPassword = async ({email}) => { 
        this.request = true;
        try {
            this.error = '';
            this.message = '';
            const result = await resetPasswordRequest({email});
          //  this.request = false;
            runInAction(() => {
                this.message = result.message;
            });
        
          
        }
        catch (err) {

            if (err && ((Array.isArray(err.errors) && err.errors.length > 0) || (Object.keys(err.errors).length > 0))) {
                runInAction(() => {
                    this.error = 'Пожалуйста, введите корректный email. Поле может быть чувствительно к регистру.';
                    this.message = '';
                })
            }
        } finally {
            runInAction(() => {
                this.request = false;
            })
        }

    }

    @action
    getProfile = async () => {
        try{
            const response = await getProfileRequest();
            runInAction(()=>{
                this.currentUserInfo =response;
            })
        } catch (error) {

        }
        finally {

        }
    }

    @action
    singIn = async ({email, password}) => {

        this.request = true;
        try {
            this.error = '';
            const result = await signInRequest({password, email});


            runInAction(() => {
                const {refresh, access, user_id, is_admin} = result;
                localStorage.setItem('refresh', refresh);
                localStorage.setItem('access', access);
                localStorage.setItem('isAdmin',is_admin);
                replace(keys.MESSAGES);
                this.refresh = refresh;
                this.access = access;
                this.userId = user_id;
                this.isSuperuser = is_admin;
            });
        } catch (error) {
            if (error && error.errors ) {
                runInAction(() => {
                    this.error = 'Пожалуйста, введите корректные имя пользователя и пароль учётной записи. Оба поля могут быть чувствительны к регистру.';
                    this.showErrorModal = true;
                })
            }
        } finally {
            runInAction(() => {
                this.request = false;
            })

        }
    }


    @action 
    getRegistrationData = async () => {

        this.request = true;
        try {
            this.error = '';
            const result = await getRegistrationDataRequest();
            runInAction(() => {
                this.position_level = result.position_level;
                this.business_unit = result.business_unit;
                this.branch = result.branch;
            });
        
          
        }
        catch (err) {

            if (err && err.errors && ((Array.isArray(err.errors) && err.errors.length > 0) || (Object.keys(err.errors).length > 0))) {
                runInAction(() => {
                  //  this.error = 'Пожалуйста, введите корректный email. Поле может быть чувствительно к регистру.';
                  //  this.message = '';
                })
            }
        } finally {
            runInAction(() => {
                this.request = false;
            })
        }

    }


    @action
    singUp = async ({ email, last_name, first_name, middle_name, phone, position_level,position_name, business_unit, branch }) => {
       
        this.request = true;
        this.isRegistered = false;
        try {
            this.error = '';
            await signUpRequest({email, last_name, first_name, middle_name, phone, position_level,position_name, business_unit, branch});


            runInAction(() => {
               this.isRegistered = true;
            });
        } catch (err) {
                runInAction(() => {
                    this.error = 'Пожалуйста, введите корректные данные. Поля могут быть чувствительны к регистру.';
                    this.isRegistered = false;
                })
        } finally {
            runInAction(() => {
                this.request = false;
            })

        }
    }

    @action
    getPositionName = async (position) => {

            this.request = true;
            try {
                this.error = '';
                const response = await getPositionNameRequest({position});

                runInAction(() => {
                    const tempArr = [];
                    response.positions.map(item => {
                     return  tempArr.push({key: item.id, label: item.name })
                    });
                    this.positionListReal = response;
                    this.positionNameArr = tempArr;
                });
            } catch (err) {
                    runInAction(() => {
                        this.error = 'Произошла ошибка';
                        this.isRegistered = false;
                    })
            } finally {
                runInAction(() => {
                    this.request = false;
                })

            }
    }


    @action
    setSelectedPosition = (key) => {
        const selecId = Number(key);
        this.selectedPosition = this.positionNameArr.find(item => item.id === selecId);
    }


    @action
    logOut = () => {
        localStorage.removeItem('access');
        localStorage.removeItem('refresh');
        localStorage.removeItem('isAdmin')
        window.location.search = '';
        window.location.pathname = keys.LOGIN;
        window.history.replaceState(null, null, keys.LOGIN);
    }


}

