import {action, configure, runInAction, observable,} from 'mobx';

configure({enforceActions: 'observed'});

export default class MenuStore {

    @observable activeTab = "1";
    @observable activeTabName = 'MESSAGES';


    @action
    changeActiveTab = (tab, tab_name) => {

        runInAction(() => {
            this.activeTab = tab;
            this.activeTabName = tab_name;
        });
    }

}

