import {action, configure, runInAction, observable, toJS} from 'mobx';
import {
  createTextRequest,
  getAllTextsRequest,
  updateTextRequest,
  deleteTextRequest,
  searchTextRequest
} from '../requests/textSectionsRequests';
import {getParams, isAdminGetFromLocal, setParams} from '../utils';

configure({enforceActions: 'observed'});

export default class TextSectionStore {

  @observable request = false;
  @observable textSections = {};
  @observable isSuperuser = isAdminGetFromLocal();
  @observable activeTextSection = undefined;
  @observable activeTextDescriptionTitle = undefined;
  @observable activeTextDescriptionDesc = undefined;
  @observable creatingText = undefined;
  @observable editingText = undefined;
  @observable activeIndex = undefined;
  @observable searchText = '';
  @observable showErrorModal = false;
  @observable errorText = '';
  @observable order = 0;
  @observable parametr = 0;
  @observable page = 1;
  @observable totalPages = 1;

  constructor() {
    let params = getParams();
    this.parametr = params.id;
  }


  @action
  setActiveTextSection = id => {
    this.creatingText = false;
    this.editingText = false;
    this.activeTextSection = id;

    this.activeIndex = toJS(this.textSections).results.findIndex(item => item.id === id);
    if (this.activeIndex > -1) {
      this.activeTextDescriptionTitle = toJS(this.textSections).results[this.activeIndex].title;
      this.activeTextDescriptionDesc = toJS(this.textSections).results[this.activeIndex].text;

    } else {
      this.activeTextDescriptionTitle = undefined;
      this.activeTextDescriptionDesc = undefined;
    }
  };

  @action
  setShowModalError = show => this.showErrorModal = show;

  @action
  setSearchField = async (text, page = 1) => {

    this.cancelCreateTextSection();
    this.searchText = text;
    this.page = Number(page);
    this.request = true;

    setParams('s', text);
    setParams('page',page);
    try {
      const response = await searchTextRequest({title: text, page});
      runInAction(() => {
        this.totalPages = response.total_pages;
        this.textSections = response;
        this.isSuperuser = response.is_superuser;
      });
    } catch (error) {
      if (error && error.errors && error.errors.title) {
        const errorText = error.errors.title[0];
        runInAction(() => {
          this.errorText = errorText;
          this.showErrorModal = true;
        })
      }
    } finally {
      runInAction(() => {
        this.request = false;
      });
    }
  }

  @action
  createTextSection = () => {
    this.creatingText = true;
    this.activeTextSection = undefined;
    this.editingText = false;
  }

  @action
  cancelCreateTextSection = () => {
    this.creatingText = false;
    this.activeTextSection = undefined;
    this.editingText = false;
  }

  @action
  editTextSection = (id) => {
    this.activeTextSection = id;
    this.creatingText = false;
    this.editingText = true;


    this.activeIndex = toJS(this.textSections).results.findIndex(item => item.id === id);
    if (this.activeIndex > -1) {
      this.activeTextDescriptionTitle = toJS(this.textSections).results[this.activeIndex].title;
      this.activeTextDescriptionDesc = toJS(this.textSections).results[this.activeIndex].text;

    } else {
      this.activeTextDescriptionTitle = undefined;
      this.activeTextDescriptionDesc = undefined;
    }

  }
  @action
  saveTextSection = async (title, text,) => {

    text = text.replace(/<(.|\n)*?>/g, '').trim()

    this.request = true;
    this.errorText = '';

    let id = toJS(this.textSections.results[this.activeIndex].id);
    let order = toJS(this.textSections.results[this.activeIndex].order);

    try {
      const response = await updateTextRequest({id, title, text, order});

      runInAction(() => {
        this.textSections.results[this.activeIndex] = response;
        this.editingText = false;
        this.activeIndex = id;
        this.activeTextDescriptionTitle = title;
        this.activeTextDescriptionDesc = text;
      });
    } catch (error) {
      const errorText = error.errors.title[0];
      runInAction(() => {
        this.errorText = errorText;
        this.showErrorModal = true;
      })

    } finally {
      runInAction(() => {
        this.request = false;
      })
    }

  }
  @action
  changeActiveTextDescriptionDesc = (value) => {
    this.activeTextDescriptionDesc = value;
  }

  @action
  addNewTextSection = async (title, text) => {
    this.request = true;
    this.errorText = '';

    try {
      const response = await createTextRequest({title, text, order: this.order});
      runInAction(() => {
        this.creatingText = false;
        this.activeTextDescriptionTitle = undefined;
        this.activeTextDescriptionDesc = undefined;
        this.textSections.results.push(response);
      });
    } catch (error) {
      if(error && error.errors && error.errors.title){
        const errorText = error.errors.title[0];
        runInAction(() => {
          this.errorText = errorText;
          this.showErrorModal = true;
        })
      }

    } finally {
      runInAction(() => {
        this.request = false;
      })
    }
  }

  @action
  deleteTextSection = async (key) => {

    this.activeTextSection = key;
    this.request = true;

    this.activeIndex = toJS(this.textSections).results.findIndex(item => item.id === key);
    let id = toJS(this.textSections.results[this.activeIndex].id);

    try{
      await deleteTextRequest({id});


      runInAction(() => {
        this.textSections.results.splice(this.activeIndex, 1);
        this.creatingText = false;
        this.editingText = false;
        this.activeIndex = undefined;
        this.activeTextSection = undefined;
        this.activeTextDescriptionTitle = undefined;
        this.activeTextDescriptionDesc = undefined;

      });
    } catch (e) {

    } finally {
      runInAction(() => {
        this.request = false;
      })
    }


  }


  @action
  getAllTexts = async (page = 1) => {
    this.request = true;
    this.page = Number(page);
    setParams('page',page);
    try{
      const response = await getAllTextsRequest({page: this.page});

      runInAction(() => {
        this.textSections = response;
        this.totalPages = response.total_pages;
        this.isSuperuser = response.is_superuser;
      });
    } catch (e) {

    } finally {
      runInAction(() => {
        this.request = false;
      })
    }
  }
}

