import { action, configure, runInAction, observable} from 'mobx';
import {
  createPositionsRequest,
  getAllPositionsRequest,
  updatePositionsRequest,
  deletePositionsRequest,
  searchTextRequest,
  uploadCSVPositionsRequest,
} from '../requests/positionRequests';
import {message} from "antd";
import * as keys from '../keys';
import {setParams, setEditPage, isAdminGetFromLocal,} from "../utils";


configure({ enforceActions: 'observed' });

export default class PositionsStore {

  @observable isSuperuser = isAdminGetFromLocal();
  @observable request = false;
  @observable positions = [];
  @observable creatingPosition = undefined;
  @observable editingPosition = undefined;
  @observable activeIndex = undefined;
  @observable activePositionCode = undefined;
  @observable activePositionName = undefined;
  @observable activePosition = undefined;
  @observable showErrorModal = false;
  @observable errorText = ''; 
  @observable searchText = '';

  @observable page = 1;
  @observable totalPages = 1;


  @action
  uploadCSVPosition = async (file) => {

    this.request = true;
    try{
      await uploadCSVPositionsRequest(file);
      message.success(`Подразделения в процесе создания, это может занять некоторое время`);
    } catch (error) {
      if (error && error.errors && error.errors) {
        const errorText = Object.values(error.errors)[0][0];
        runInAction(() => {
          this.errorText = errorText;
          this.showErrorModal = true;
        })
      }
    } finally {
      runInAction(()=>{
        this.request = false;
      })
    }
  }



  @action
  setSearchField = async (text, page = 1) => {
    this.request = true;
    this.searchText = text;
    this.page = Number(page);
    setParams('s', text);
    setParams('page',page);

    try{
      const response = await searchTextRequest({name: text, page});
      runInAction(() => {
        this.positions = response.results;
        this.totalPages = response.total_pages;
        this.isSuperuser = response.is_superuser;
      });
    } catch (error) {

    } finally {
      runInAction(()=>{
        this.request = false;
      });
    }



  }

  @action
  deletePosition = async (key) => {

    this.activePosition = key;
    this.request = true;
    this.activeIndex = this.positions.findIndex(item => item.id === key);
    let id = this.positions[this.activeIndex].id;

    try{
      await deletePositionsRequest({id});


      runInAction(() => {
        this.positions.splice(this.activeIndex, 1);
        this.creatingPosition = false;
        this.editingPosition = false;
        this.activeIndex = undefined;
        this.activePosition = undefined;
        this.activePositionCode = undefined;
        this.activePositionName = undefined;

      });
    } catch (e) {

    } finally {
      runInAction(()=>{
        this.request = false;
      });
    }


  }

  @action
  editPosition = (id) => {
  
    this.activePosition = id;
    this.creatingPosition = false;
    this.editingPosition = true;

    setEditPage(keys.POSITION, id);


    this.activeIndex = this.positions.findIndex(item => item.id === id);
    if (this.activeIndex > -1) {
      this.activePositionCode = this.positions[this.activeIndex].code;
      this.activePositionName = this.positions[this.activeIndex].name;
     

    } else {
      this.activePositionCode = undefined;
      this.activePositionName = undefined;
    }

  }

  @action
  savePosition = async (code, name) => {
    try {
      let id = this.positions[this.activeIndex].id;
      const response = await updatePositionsRequest({ id, code, name, });

      runInAction(() => {
        this.positions[this.activeIndex] = response;
        this.editingPosition = false;
        //this.activeIndex = id;
        this.activePositionCode = code;
        this.activePositionName = name;
      });
    } catch (error) {
      //const errorText = error.errors.code;
      const errorText = 'Произошла ошибка! Возможно данные уже существуют. Попробуйте повторить попытку.';
      runInAction(() => {
        this.errorText = errorText;
        this.showErrorModal = true;
      })

    } finally {
      runInAction(()=>{
        this.request = false;
      });
    }

  }
  
  
  @action
  createPosition = (value) => {
    this.creatingPosition = true;
    this.activePosition = undefined;
    this.editingPosition = false;
  }

  @action
  cancelCreatePosition = (value) => {
    this.creatingPosition = false;
    this.activePosition = undefined;
    this.editingPosition = false;
  }


  @action
  setActivePosition = id => {
    this.creatingPosition = false;
    this.editingPosition = false;
    this.activePosition = id;

    this.activeIndex = this.positions.findIndex(item => item.id === id);
    if (this.activeIndex > -1) {

      this.activePositionCode =this.positions[this.activeIndex].code;
      this.activePositionName = this.positions[this.activeIndex].name;

    } else {
      this.activePositionCode = undefined;
      this.activePositionName = undefined;
    }
  };

  @action
  setShowModalError = show => this.showErrorModal = show;

  @action
  addNewPosition = async (code, name) => {

    this.request = true;
    this.errorText = '';


    try {
      const response = await createPositionsRequest({ code, name });
      runInAction(() => {
        this.creatingPosition = false;
        this.activePositionCode = undefined;
        this.activePositionName = undefined;
        this.positions.push(response);

      });
    } catch (error) {
      //const errorText = error.errors.code ;
      const errorText = 'Произошла ошибка! Возможно данные уже существуют. Попробуйте повторить попытку.';
      runInAction(() => {
        this.errorText = errorText;
        this.showErrorModal = true;
      })
    } finally {
      runInAction(()=>{
        this.request = false;
      });
    }
  }

  @action
  getAllPosition = async (page = 1) => {

    this.request = true;
    this.page = Number(page);
    setParams('page',page);
    try{
      const response = await getAllPositionsRequest({page: this.page});
      runInAction(() => {
        this.positions = response.results;
        this.totalPages = response.total_pages;
        this.isSuperuser = response.is_superuser;
      });
    } catch (e) {

    } finally {
      runInAction(()=>{
        this.request = false;
      });
    }
  }
  

 

}

