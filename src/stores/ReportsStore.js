import {action, configure, observable} from 'mobx';
import {
  downloadFileReport
} from '../requests/reportsRequests';
import {isAdminGetFromLocal} from "../utils";

configure({enforceActions: 'observed'});


export default class ReportsStore {
  @observable isSuperuser = isAdminGetFromLocal();
  @observable request = false;
  @observable errorText = '';
  @observable showErrorModal = false;

  @action
  setShowModalError = (show) => this.showErrorModal = show;


  downloadFile = () => {
    downloadFileReport()
  }


}