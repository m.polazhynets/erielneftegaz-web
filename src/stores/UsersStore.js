import {action, configure, runInAction, observable} from 'mobx';
import {
  getAllUsersRequest,
  editRequest,
  searchUsersRequest,
  getPaginatedDataRequest
} from '../requests/userRequests';
import {isAdminGetFromLocal, setEditPage, setParams} from '../utils';
import * as keys from '../keys';
configure({enforceActions: 'observed'});

class UsersStore {
  @observable request = false;
  @observable isSuperuser = isAdminGetFromLocal();
  @observable fetching = false;
  @observable usersList = [];
  @observable wellList = [];
  @observable wellListActive = [];
  @observable selectedItemsFacility = [];
  @observable positionList = [];
  @observable positionListReal = []; //for render
  @observable selectedPosition = {}; //real data
  @observable subdivisionList = []; // for render
  @observable subdivisionListReal= []; //real data
  @observable selectedSubdivision = {};
  @observable isEditingMode = false;
  @observable selectedUserId = undefined;
  @observable selectedUserData = {};
  @observable fio = '';
  @observable position = '';
  @observable searchText = '';
  @observable page = 1;
  @observable totalPages = 1;
  @observable errorText = '';
  @observable showErrorModal = false;

  @action
  selectActiveUser = (params) => {
    const id = Number(params.id);
    setEditPage(keys.USERS_AD, id);
    this.selectedUserId = id;
    this.isEditingMode = true;
    const item = this.usersList.find(item => item.id === id);

    if(item) {
      const newArray = [];

      item.facility.map(item => newArray.push({key: String(item.id), label: item.name}));

      this.selectedUserData = item;
      this.selectedItemsFacility = newArray;
    }
  };

  @action
  clearEditMode = () => {
    this.isEditingMode = false;
    this.selectedUserId = undefined;
    this.selectedUserData = {};
    this.fio = '';
  }


  @action
  setSelectedItemsFacility = selectedItems => this.selectedItemsFacility = selectedItems;

  @action
  setSelectedPosition = (key) => {
    const selecId = Number(key);
    this.selectedPosition = this.positionListReal.find(item => item.id === selecId);
  }

  @action
  setSelectedSubdivision = (key) => {
    const selecId = Number(key);
    this.selectedSubdivision = this.subdivisionListReal.find(item => item.id === selecId);
  }

  @action
  setShowModalError = (show) => this.showErrorModal = show;

  @action
  cancelActive = () => {
    setParams('id','');
    this.selectedUserId = undefined;
    this.isEditingMode = false;
    this.selectedUserData = {};
  };

  @action
  getAllUsers = async (page = 1) => {
      this.request = true;
      this.page = Number(page);
      setParams('page',page);

      try{
        const response = await getAllUsersRequest({page});

        runInAction(()=>{
          this.usersList = response.results;
          this.totalPages = response.total_pages;
          this.isSuperuser = response.is_superuser;
        })
      } catch (error) {
            if (error && error.errors && error.errors) {
              const errorText = Object.values(error.errors)[0][0];
              runInAction(() => {
                this.errorText = errorText;
                this.showErrorModal = true;
              })
            }
      } finally {
        runInAction(()=>{
          this.request = false;
        })
      }
  }

  @action
  setSearchField = async (text, page = 1) => {
    this.searchText = text;
    this.page = Number(page);
    this.request = true;

    setParams('s', text);
    setParams('page',page);


    try{
      const response = await searchUsersRequest({page, name: text});

      runInAction(()=>{
        this.totalPages = response.total_pages;
        this.usersList = response.results;
        this.isSuperuser = response.is_superuser;
      });

    } catch (error) {
      if (error && error.errors && error.errors.title) {
        const errorText = error.errors.title[0];
        runInAction(() => {
          this.errorText = errorText;
          this.showErrorModal = true;
        })
      }
    } finally {
      runInAction(() => {
        this.request = false;
      });
    }
  }

  @action
  setActiveFacility = (facility) => {

    let tempArray = [];

    facility.map(item => {
      tempArray.push({id: item.key, name: item.label});
      return item;
    });

    this.wellListActive = tempArray;
  }

  @action
  editUser = async ({subdivision, position, ...data}) => {

    this.request = true;

    const facilityIdArr = [];

    this.wellListActive.map(item => {
      facilityIdArr.push(Number(item.id));
    });
    try{
      const id = this.selectedUserId;
      let response = await editRequest({
        id,
        facility: facilityIdArr,
        subdivision: this.selectedSubdivision,
        position: this.selectedPosition,
        ...data,
      });

      const indexItem = this.usersList.findIndex(item => item.id === id);

      const facility = response.facility.slice();

      facility.map((item, index) => {
        response.facility[index].key =  item.id;
        response.facility[index].label =  item.name;
      });

      runInAction(()=>{
        this.usersList[indexItem] = response;
      })

    } catch (error) {
      if (error && error.errors && error.errors) {
        const errorText = 'Произошла ошибка! Возможно данные уже существуют. Попробуйте повторить попытку.';
        runInAction(() => {
          this.errorText = errorText;
          this.showErrorModal = true;
        })
      }
    } finally {
      runInAction(()=>{
        this.request = false;
      })
    }
  }

  @action
  getPaginatedData = async (search_in, value) => {

    if(search_in === 'facility'){
      this.fetching = true;
    }

    try{
      const response = await getPaginatedDataRequest({search_in, value});
      runInAction(()=>{
        if(search_in === 'facility'){
          this.wellList = response;
        } else if(search_in === 'position'){

          const tempArr = [];

          response.map(item => tempArr.push({key: item.id, label: item.name }));
          this.positionListReal = response;
          this.positionList = tempArr;
        } else if(search_in === 'subdivision'){
          const tempArr = [];

          response.map(item => tempArr.push({key: item.id, label: item.name }));
          this.subdivisionListReal = response;
          this.subdivisionList = tempArr;
        }

      })
    } catch (error) {
      if (error && error.errors && error.errors) {
        const errorText = Object.values(error.errors)[0][0];
        runInAction(() => {
          this.errorText = errorText;
          this.showErrorModal = true;
        })
      }
    } finally {
      runInAction(()=>{
        if(search_in === 'facility'){
          this.fetching = false;
        }
      })
    }
  }

}

export default UsersStore;
