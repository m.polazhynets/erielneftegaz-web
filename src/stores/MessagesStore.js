import {action, configure, runInAction, observable} from 'mobx';
import {
  getListCreatedCardRequest,
  getCreatedCardRequest,
  createSpasCardTasksRequest,
  createStatusLogRequest,
  createTaskResultsRequest,
  getCreatedCardStatusLogsRequest,
  getCreatedCardTasksRequest,
  getCreatedCardUsersRequest,
  getSpasCardTaskRequest,
  setNewResponsibleUserRequest,
  updateSpasCardTaskRequest,
  updateTaskResultRequest, updateSpasCardTaskSetStatusRequest,
} from '../requests/messagesRequest';
import {setParams} from '../utils';
configure({enforceActions: 'observed'});


export default class MessagesStore {
  @observable request = false;
  @observable activeMessageId = null;
  @observable messagesList = [];
  @observable spasCardInfo = {};
  @observable spasCardsTasks = [];
  @observable spasCardsUsers = [];
  @observable selectedUserId = null;
  @observable activeTaskId = null;
  @observable activeTaskObject = null;
  @observable activeRiskObject = null;
  @observable statusMessage = undefined;
  @observable statusTask = undefined;
  @observable isEditingStatusTask = false;
  @observable isCreatingTask = false;
  @observable isEditingTask = false;
  @observable isEditingResultOfTask = false;
  @observable isCreatingResultOfTask = false;
  @observable spasCardsStatusLogs = [];
  @observable searchText = '';
  @observable page = 1;
  @observable totalPages = 1;
  @observable errorText = '';
  @observable showErrorModal = false;

  @action
  setMessageStatus = value => this.statusMessage = value;

  @action
  setActiveMessageId = id => this.activeMessageId = id;

  @action
  setShowModalError = (show) => this.showErrorModal = show;

  @action
  setSelectedUserId = id => this.selectedUserId = Number(id);

  @action
  setEditTask = () => {
    this.isCreatingTask = false;
    this.isEditingTask = true;

    const temp = this.spasCardsTasks.find(item => item.id === this.activeTaskId);
    this.activeTaskObject = temp;
  }

  @action
  cancelButtonClicked = () => {
    this.isEditingStatusTask = false;
    this.isEditingTask = false;
    this.isCreatingTask = false;
    this.isEditingResultOfTask = false;
    this.isCreatingResultOfTask = false;
  }

  @action
  setStatusTask = (status) => {
    this.statusTask = status;
  }

  @action
  setEditingTaskStatus = () => {
    this.isEditingStatusTask = true;
    this.isEditingTask = false;
    this.isCreatingTask = false;
    const temp = this.spasCardsTasks.find(item => item.id === this.activeTaskId);
    this.statusTask = temp.status;
    this.activeTaskObject = temp;
  }

  @action
  cancelSelectionTask = () => {
    this.activeTaskId = null;
    this.activeTaskObject = null;

  }

  @action
  setCreatingTask = () => {
    this.isEditingStatusTask = false;
    this.isCreatingTask = true;
    this.isEditingTask = false;
  }

  @action
  setCreatingResult = () => {
    this.isCreatingResultOfTask = true;
    this.isEditingResultOfTask = false;
  }

  @action
  setEditingResult = (idTask) => {
    this.isEditingStatusTask = false;
    this.isEditingResultOfTask = true;
    this.isCreatingResultOfTask = false;
  }

  @action
  setActiveTaskId = id => {
    this.activeTaskId = Number(id);
    const index = this.spasCardsTasks.findIndex(item => item.id === Number(id));
    if (index > -1) {
      if (this.spasCardsTasks[index].reports.length > 0) {
        this.activeRiskObject = this.spasCardsTasks[index].reports[0];
      } else {
        this.activeRiskObject = {};
      }
    }
  }

  @action
  createResultOfTask = async (dataForForm) => {
    this.request = false;
    try {
    const task = this.activeTaskId;
      await createTaskResultsRequest({task, ...dataForForm});
      const response =await getCreatedCardTasksRequest({id: this.activeMessageId});


      runInAction(()=>{
        this.spasCardsTasks = response;
        this.isCreatingResultOfTask = false;
        this.activeRiskObject = response.find(item => item.id === task).reports[0];
      })
    } catch (error) {
      if (error && error.errors && error.errors) {
        const errorText = Object.values(error.errors)[0][0];
        runInAction(() => {
          this.errorText = errorText;
          this.showErrorModal = true;
        })
      }
    } finally {
      runInAction(()=>{
        this.request = false;
      })
    }
  }

  @action
  editResultOfTask = async (dataForForm) => {
    this.request = true;
    const task = this.activeTaskId;
    const id = this.activeRiskObject.id;

    try{
      await updateTaskResultRequest(id,{task, ...dataForForm});
      const response = await getCreatedCardTasksRequest({id:this.activeMessageId});

      const riskObj = response.find(item => item.id === this.activeTaskId).reports[0];

      runInAction(()=>{
        this.spasCardsTasks = response;
        this.activeRiskObject = riskObj;
        this.isEditingResultOfTask= false;
      })

    } catch (error) {
      if (error && error.errors && error.errors) {
        const errorText = Object.values(error.errors)[0][0];
        runInAction(() => {
          this.errorText = errorText;
          this.showErrorModal = true;
        })
      }
    } finally {
      runInAction(()=>{
        this.request = false;
      })
    }
  }

  @action
  editCardSpasTaskStatus = async () => {
    this.request = true;

    try{
      const id = this.activeTaskId;
      const status = this.statusTask;
      await updateSpasCardTaskSetStatusRequest({id, status});

      const taskIndex = this.spasCardsTasks.findIndex(item => item.id === id);

      runInAction(()=>{
        this.spasCardsTasks[taskIndex].status = status;
        this.isEditingStatusTask = false;
      })
    } catch (error) {
      if (error && error.errors && error.errors) {
        const errorText = Object.values(error.errors)[0][0];
        runInAction(() => {
          this.errorText = errorText;
          this.showErrorModal = true;
        })
      }
    } finally {
      runInAction(()=>{
        this.request = false;
      })
    }


  }

  @action
  createSpasCardTask = async ({text, expiry_date}) => {
      this.request = true;
      const performer = this.selectedUserId;
      const spas_card = this.spasCardInfo.id;

      try{
       const responseCreateCardSpas = await createSpasCardTasksRequest({expiry_date, text, performer, spas_card});
       const idNewTask = responseCreateCardSpas.id;
        const responseCardTask = await getSpasCardTaskRequest({id: idNewTask});
        runInAction(()=>{
          this.isEditingTask = false;
          this.isCreatingTask = false;
          this.spasCardsTasks.push(responseCardTask);
        })
      } catch (error) {
        if (error && error.errors && error.errors) {
          const errorText = Object.values(error.errors)[0][0];
          runInAction(() => {
            this.errorText = errorText;
            this.showErrorModal = true;
          })
        }
      } finally {
        runInAction(()=>{
          this.request = false;
        })
      }
  }

  @action
  editSpasCardTask = async ({text, expiry_date}) => {
    this.request = true;
    let performer = this.selectedUserId;

    const spas_card = this.spasCardInfo.id;
    const id = this.activeTaskId;

    if(!performer && this.activeTaskObject.performer.id){
      performer = this.activeTaskObject.performer.id;

    }

    try{
      await updateSpasCardTaskRequest({id, expiry_date, text, performer, spas_card});
      const response = await getCreatedCardTasksRequest({id: spas_card});



      runInAction(()=>{
        this.isEditingTask = false;
        this.spasCardsTasks = response;
        this.activeTaskId = null;
        this.activeTaskObject = null;
      })
    } catch (error) {
      if (error && error.errors && error.errors) {
        const errorText = Object.values(error.errors)[0][0];
        runInAction(() => {
          this.errorText = errorText;
          this.showErrorModal = true;
        })
      }
    } finally {
      runInAction(()=>{
        this.request = false;
      })
    }
  }


  @action
  setStatusForSpasCard =async status => {

    this.request = true;
    const id = this.activeMessageId;

    try{
      await createStatusLogRequest({id, status});
      const response = await getCreatedCardStatusLogsRequest({id});

      runInAction(()=>{
        this.spasCardsStatusLogs = response;
      })
    } catch (error) {
      if (error && error.errors && error.errors) {
        const errorText = Object.values(error.errors)[0][0];
        runInAction(() => {
          this.errorText = errorText;
          this.showErrorModal = true;
        })
      }
    } finally {
      runInAction(()=>{
        this.request = false;
        this.statusMessage = undefined;
      })
    }
  }

  @action
  setUserForSpasCard =async () => {

    const user_id = this.selectedUserId;
    const id = this.activeMessageId;
    this.request = true;

    try{
      await setNewResponsibleUserRequest({id, user_id});
      const response = await getCreatedCardRequest({id});

      runInAction(()=>{
        this.selectedUserId = null;
        this.spasCardInfo = response;
      })

    } catch (error) {
      if (error && error.errors && error.errors) {
        const errorText = Object.values(error.errors)[0][0];
        runInAction(() => {
          this.errorText = errorText;
          this.showErrorModal = true;
        })
      }
    } finally {
      runInAction(()=>{
        this.request = false;
      })

    }

  }

  @action
  getListUsersSearch =async value => {
    const facility_id = Number(this.spasCardInfo.facility.id);
    const id =  this.activeMessageId;
    const name = value;

    try{
      const response = await getCreatedCardUsersRequest({id, facility_id, name: name});

      const tempArray = [];

      response.map(item => {
        tempArray.push({key: item.id, label: item.fio})
        return item;
      })



      runInAction(()=>{
        this.spasCardsUsers = tempArray;
      });

    } catch (error) {
      if (error && error.errors && error.errors) {
        const errorText = Object.values(error.errors)[0][0];
        runInAction(() => {
          this.errorText = errorText;
          this.showErrorModal = true;
        })
      }
    } finally {

    }
  }

  @action
  getListCreatedCard = async (page) => {
    this.request = true;
    this.page = Number(page);
    setParams('page',page);

    try{
      const response = await getListCreatedCardRequest({page});
      runInAction(()=>{
        this.messagesList = response.results;
        this.totalPages = response.total_pages;
      });
    } catch (error) {
      if (error && error.errors && error.errors) {
        const errorText = Object.values(error.errors)[0][0];
        runInAction(() => {
          this.errorText = errorText;
          this.showErrorModal = true;
        })
      }
    } finally {
      runInAction(()=>{
        this.request = false;
      })
    }
  }

  @action
  getInfoByCard = async (idParam) => {
    const id = Number(idParam);

    this.request = true;

    try{
      const promiseGetCreatedCard = getCreatedCardRequest({id});
      const promiseGetCreatedCardTasks = getCreatedCardTasksRequest({id});
      const promiseGetCreatedCardStatusLogs = getCreatedCardStatusLogsRequest({id});

      const [
        responseGetCreatedCard,
        responseGetCreatedCardTasks,
        responseGetCreatedCardStatusLogs,
      ] = await Promise.all([
        promiseGetCreatedCard,
        promiseGetCreatedCardTasks,
        promiseGetCreatedCardStatusLogs,
      ])

      runInAction(()=>{
        this.spasCardInfo = responseGetCreatedCard;
        this.spasCardsTasks = responseGetCreatedCardTasks;
        this.spasCardsStatusLogs = responseGetCreatedCardStatusLogs;
      });

    } catch (error) {
      if (error && error.errors && error.errors) {
        const errorText = Object.values(error.errors)[0][0];
        runInAction(() => {
          this.errorText = errorText;
          this.showErrorModal = true;
        })
      }
    } finally {
      runInAction(()=>{
        this.request = false;
      })
    }
  }

}