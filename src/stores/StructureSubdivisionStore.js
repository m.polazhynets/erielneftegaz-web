import { action, configure, runInAction, observable} from 'mobx';
import {
  uploadFileSubdivisionsStructureRequest,
  getSubdivisionsStructure
} from '../requests/structureSubdivisionRequest';
import {getParams, isAdminGetFromLocal,} from "../utils";


configure({ enforceActions: 'observed' });

export default class SubdivisionStore {

  @observable isSuperuser = isAdminGetFromLocal();
  @observable request = false;
  @observable errorText = '';
  @observable searchText = '';
  @observable subdivisionsStructureList = [];
  @observable parametr = 0;
  @observable page = 1;
  @observable totalPages = 1;

  constructor() {
    let params = getParams();
    this.parametr = params.id;
  }

  @action
  uploadJSSubdivisionsStructure = async (file) => {

    this.request = true;
    try {
      await uploadFileSubdivisionsStructureRequest(file);
    } catch (error) {
      if (error && error.errors && error.errors) {
        const errorText = Object.values(error.errors)[0][0];
        runInAction(() => {
          this.errorText = errorText;
          this.showErrorModal = true;
        })
      }
    } finally {
      runInAction(() => {
        this.request = false;
      })
    }
  }

  getSubdivisionsStructure = async() => {
    this.request = true;
    try{
    const response = await getSubdivisionsStructure();

    runInAction(()=>{
      this.subdivisionsStructureList = response;
      this.totalPages = response.total_pages;
    })

    } catch (error) {
      if (error && error.errors && error.errors) {
        const errorText = Object.values(error.errors)[0][0];
        runInAction(() => {
          this.errorText = errorText;
          this.showErrorModal = true;
        })
      }
    } finally {
      this.request = false;
    }

  }

}