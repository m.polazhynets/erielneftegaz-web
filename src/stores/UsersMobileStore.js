import {action, configure, runInAction, observable} from 'mobx';
import {
  editMobileUser,
  getAllMobileUsers,
  searchUsersMobileRequest,
} from '../requests/usersMobileRequests';
import {isAdminGetFromLocal, setEditPage, setParams} from '../utils';
import * as keys from '../keys';
configure({enforceActions: 'observed'});

class UsersMobileStore {
  @observable request = false;
  @observable isSuperuser = isAdminGetFromLocal();
  @observable usersMobileList = [];
  @observable isEditingUser = false;
  @observable activeUserId = null;
  @observable activeUserObject = {};
  @observable statusOfUser = false;
  @observable fetching = false;
  @observable searchText = '';
  @observable page = 1;
  @observable totalPages = 1;
  @observable errorText = '';
  @observable showErrorModal = false;

  @action
  selectActiveUser = (idParam) => {
    const id = Number(idParam);

    setEditPage(keys.USERS_MOBILE, id);

    this.activeUserObject = this.usersMobileList.find(item => item.id === id);
    this.statusOfUser = this.activeUserObject.is_active;
    this.activeUserId = id;
    this.isEditingUser = true;
  };

  @action
  setStatusOfUser = (status) => this.statusOfUser = status;

  @action
  setShowModalError = (show) => this.showErrorModal = show;

  @action
  cancelButtonClicked = () => {
    this.clearEditMode();
  }

  @action
  clearEditMode = () => {
    this.isEditingUser = false;
    this.activeUserObject = {};
    this.activeUserId = null;
  }

  @action
  getAllMobileUsers = async (page = 1) => {
    this.request = true;
    this.page = Number(page);
    setParams('page',page);

    try{
      const response = await getAllMobileUsers({page});

      runInAction(()=>{
        this.usersMobileList = response.results;
        this.totalPages = response.total_pages;
        this.isSuperuser = response.is_superuser;
      })
    } catch (error) {
      if (error && error.errors && error.errors) {
        const errorText = Object.values(error.errors)[0][0];
        runInAction(() => {
          this.errorText = errorText;
          this.showErrorModal = true;
        })
      }
    } finally {
      runInAction(()=>{
        this.request = false;
      })
    }
  }

  @action
  setSearchField = async (text, page = 1) => {

    this.searchText = text;
    this.page = Number(page);
    this.request = true;

    setParams('s', text);
    setParams('page',page);


    try{
      const response = await searchUsersMobileRequest({page, name: text});

      runInAction(()=>{
        this.totalPages = response.total_pages;
        this.usersMobileList = response.results;
        this.isSuperuser = response.is_superuser;
      });

    } catch (error) {
      if (error && error.errors && error.errors.title) {
        const errorText = error.errors.title[0];
        runInAction(() => {
          this.errorText = errorText;
          this.showErrorModal = true;
        })
      }
    } finally {
      runInAction(() => {
        this.request = false;
      });
    }
  }

  @action
  editMobileUser = async () => {
    this.request = true;

    try{
      const id = this.activeUserId;
      const is_active = this.statusOfUser;
      await editMobileUser({id, is_active});

      const indexOfUser = this.usersMobileList.findIndex(item => item.id === id);

      runInAction(()=>{
        this.usersMobileList[indexOfUser].is_active = is_active;
        this.isEditingUser = false;
      });

    } catch (error) {
      if (error && error.errors && error.errors) {
        const errorText = 'Произошла ошибка! Возможно данные уже существуют. Попробуйте повторить попытку.';
        runInAction(() => {
          this.errorText = errorText;
          this.showErrorModal = true;
        })
      }
    } finally {
      runInAction(()=>{
        this.request = false;
      })
    }

  }



}

export default UsersMobileStore;
