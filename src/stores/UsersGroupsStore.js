import {action, configure, runInAction, observable} from 'mobx';
import {
  createUsersGroupRequest,
  deleteUsersGroupRequest,
  editUsersGroupRequest,
  getAllUsersGroupsRequest,
  searchTextRequest,
} from '../requests/usersGroupsRequest';
import * as keys from '../keys';
import {setParams, setEditPage, isAdminGetFromLocal,} from "../utils";
import {searchUsersRequest} from "../requests/userRequests";

configure({ enforceActions: 'observed' });

export default class UsersGroupsStore {

  @observable isSuperuser = isAdminGetFromLocal();
  @observable request = false;
  @observable usersGroupsList = [];
  @observable usersList = [];
  @observable selectedItemsUsers = [];
  @observable fetching = false;
  @observable creatingUsersGroups = undefined;
  @observable editingUsersGroups = undefined;
  @observable activeIndex = undefined;
  @observable activeUsersGroupsName = undefined;
  @observable activeUsersGroups = undefined;
  @observable showErrorModal = false;
  @observable errorText = '';
  @observable searchText = '';

  @observable page = 1;
  @observable totalPages = 1;


  @action
  setSearchField = async (text, page = 1) => {

    this.request = true;
    this.searchText = text;
    this.page = Number(page);
    setParams('s', text);
    setParams('page',page);

    try{
      const response = await searchTextRequest({name: text, page});
      runInAction(() => {
        this.usersGroupsList = response.results;
        this.totalPages = response.total_pages;
        this.isSuperuser = response.is_superuser;
      });
    } catch (error) {

    } finally {
      runInAction(()=>{
        this.request = false;
      });
    }
  }

  @action
  setSelectedItemsUsers = selectedItems => this.selectedItemsUsers = selectedItems;

  @action
  searchUsers = async (name) => {

    this.fetching = true;

    try{
      const response = await searchUsersRequest({page: 1, name});

      const tempArray = [];

      response.results.map(item => {
       return tempArray.push({id: item.id.toString(), name: item.fio});
      });

      runInAction(()=>{
        this.usersList = tempArray;
      })
    } catch (error) {
      if (error && error.errors && error.errors) {
        const errorText = Object.values(error.errors)[0][0];
        runInAction(() => {
          this.errorText = errorText;
          this.showErrorModal = true;
        })
      }
    } finally {
      runInAction(()=>{
        this.fetching = false;
      })
    }
  }

  @action
  deleteUsersGroup = async (key) => {


    this.activeUsersGroups = key;
    this.request = true;
    this.activeIndex = this.usersGroupsList.findIndex(item => item.id === key);
    let id = this.usersGroupsList[this.activeIndex].id;

    try{
      await deleteUsersGroupRequest({id});


      runInAction(() => {
        this.usersGroupsList.splice(this.activeIndex, 1);
        this.creatingUsersGroups = false;
        this.editingUsersGroups = false;
        this.activeIndex = undefined;
        this.activeUsersGroups = undefined;
        this.activeUsersGroupsName = undefined;
        this.selectedItemsUsers = [];

      });
    } catch (e) {

    } finally {
      runInAction(()=>{
        this.request = false;
      });
    }


  }

  @action
  editUsersGroups = (id) => {

    this.activeUsersGroups = Number(id);
    this.creatingUsersGroups = false;
    this.editingUsersGroups = true;

    setEditPage(keys.USERS_GROUPS, id);


    this.activeIndex = this.usersGroupsList.findIndex(item => Number(item.id) === Number(id));
    if (this.activeIndex > -1) {
      this.activeUsersGroupsName = this.usersGroupsList[this.activeIndex].name;
      const tempArray = [];
      this.usersGroupsList[this.activeIndex].users.map(item => {
       return  tempArray.push({key: item.id.toString(), label: item.fio});
      });

      this.selectedItemsUsers = tempArray;


    } else {
      this.activeUsersGroupsName = undefined;
      this.selectedItemsUsers = [];
    }

  }

  @action
  saveUsersGroups = async (name) => {
    try {
      let id = this.usersGroupsList[this.activeIndex].id;

      const users = [];

      this.selectedItemsUsers.map(item => {
        const id = Number(item.key);
        users.push(id);
        return item;
      });

      const response = await editUsersGroupRequest({ id, name, users});

      response.users = users;

      runInAction(() => {
        this.usersGroupsList[this.activeIndex] = response;
        this.editingUsersGroups = false;
        this.activeUsersGroupsName = name;
        this.selectedItemsUsers = [];
      });
    } catch (error) {
      //const errorText = error.errors.code;
      const errorText = 'Произошла ошибка! Возможно данные уже существуют. Попробуйте повторить попытку.';
      runInAction(() => {
        this.errorText = errorText;
        this.showErrorModal = true;
      })

    } finally {
      runInAction(()=>{
        this.request = false;
      });
    }

  }


  @action
  createUsersGroups = () => {
    this.creatingUsersGroups = true;
    this.activeUsersGroups = undefined;
    this.editingUsersGroups = false;
    this.selectedItemsUsers = [];
  }

  @action
  cancelCreateUsersGroups = () => {
    this.creatingUsersGroups = false;
    this.activeUsersGroups = undefined;
    this.editingUsersGroups = false;
    this.selectedItemsUsers = [];
  }


  @action
  setActiveUsersGroups = idStr => {

    const id = Number(idStr);
    this.creatingUsersGroups = false;
    this.editingUsersGroups = false;
    this.activeUsersGroups = id;

    this.activeIndex = this.usersGroupsList.findIndex(item => Number(item.id) === id);
    if (this.activeIndex > -1) {

      this.activeUsersGroupsName = this.usersGroupsList[this.activeIndex].name;


      const tempArray = [];
      this.usersGroupsList[this.activeIndex].users.map(item => {
      return   tempArray.push({key: item.id.toString(), label: item.fio});
      });

      this.selectedItemsUsers = tempArray;

    } else {
      this.activeUsersGroupsName = undefined;
      this.selectedItemsUsers = [];
    }
  };

  @action
  setShowModalError = show => this.showErrorModal = show;

  @action
  addNewUsersGroups = async (name) => {


    this.request = true;
    this.errorText = '';

    const users = [];

    this.selectedItemsUsers.map(item => {
      const id = Number(item.key);
      users.push(id);
      return item;
    })

    try {
      const response = await createUsersGroupRequest({name, users });

      const tempArrayForUsersGroupList = [];

      this.selectedItemsUsers.map((item,i) => {
       return  tempArrayForUsersGroupList.push({id: item.key.toString(), name: item.label})
      })

      response.users = tempArrayForUsersGroupList;

      runInAction(() => {
        this.usersGroupsList.push(response);
        this.creatingUsersGroups = false;
        this.activeUsersGroupsName = undefined;
        this.selectedItemsUsers = [];
      });
    } catch (error) {
      //const errorText = error.errors.code ;
      const errorText = 'Произошла ошибка! Возможно данные уже существуют. Попробуйте повторить попытку.';
      runInAction(() => {
        this.errorText = errorText;
        this.showErrorModal = true;
      })
    } finally {
      runInAction(()=>{
        this.request = false;
      });
    }
  }

  @action
  getAllUsersGroups = async (page = 1) => {


    this.request = true;
    this.page = Number(page);
    setParams('page',page);
    try{
      const response = await getAllUsersGroupsRequest({page: this.page});
      runInAction(() => {
        this.usersGroupsList = response.results;
        this.totalPages = response.total_pages;
        this.isSuperuser = response.is_superuser;
      });
    } catch (e) {

    } finally {
      runInAction(()=>{
        this.request = false;
      });
    }
  }




}

