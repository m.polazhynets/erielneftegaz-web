import { action, configure, runInAction, observable } from 'mobx';
import {
  createRiskNotificationRequest,
  deleteRiskNotificationRequest,
  editRiskNotificationRequest,
  getRiskNotificationItemRequest,
  getRiskNotificationsRequest,
  getPaginatedDataRequest
} from '../requests/riskNotificationRequests';
import * as keys from '../keys';
import {
  setParams,
  setEditPage,
  isAdminGetFromLocal,
  removeCreateEditFromRoutePage,
} from "../utils";
import { searchTextRequest } from "../requests/usersGroupsRequest";

configure({ enforceActions: 'observed' });

export default class RiskNotificationStore {
  @observable isSuperuser = isAdminGetFromLocal();
  @observable riskNotificationsList = [];
  @observable positionList = [];
  @observable selectedPositions = [];
  @observable groupsUsersList = [];
  @observable selectedGroupsUsers = [];
  @observable isCreatingRiskNotification = false;
  @observable isEditingRiskNotification = false;
  @observable activeRiskNotificationItemId = null;
  @observable fetchingPosition = false;
  @observable fetchingGroupsUsers = false;
  @observable arrayOfResponsible = [];
  @observable riskItemsList = [];
  @observable activeRiskObj = {};
  @observable activeRiskNotificationItemObject = {};
  @observable activeX = 0;
  @observable activeY = 0;
  @observable request = false;
  @observable showErrorModal = false;
  @observable errorText = '';
  @observable searchText = '';

  @observable page = 1;
  @observable totalPages = 1;

  @action
  searchPosition = async (name) => {
    this.fetchingPosition = true;

    try {
      const response = await getPaginatedDataRequest({ value: name, search_in: 'position' });

      const tempArray = [];

      response.map(item => {
      return  tempArray.push({ id: item.id.toString(), name: item.name });
      });

      runInAction(() => {
        this.positionList = tempArray;
      })
    } catch (error) {
      if (error && error.errors && error.errors) {
        const errorText = Object.values(error.errors)[0][0];
        runInAction(() => {
          this.errorText = errorText;
          this.showErrorModal = true;
        })
      }
    } finally {
      runInAction(() => {
        this.fetchingPosition = false;
      })
    }
  }

  @action
  setSearchField = async (text) => {
    this.searchText = text;
    setParams('s', text);
    this.getAllRiskNotification(text);
  }

  @action
  cancelButtonClicked = () => {
    this.isCreatingRiskNotification = false;
    this.isEditingRiskNotification = false;
    removeCreateEditFromRoutePage(keys.RISK_NOTIFICATIONS);
  }

  @action
  setCreateRiskNotification = () => {
    this.isEditingRiskNotification = false;
    this.isCreatingRiskNotification = true;
    this.selectedGroupsUsers = [];
    this.selectedPositions = [];
  }

  @action
  setActiveRiskNotificationItemId = itemId => {
    this.activeRiskNotificationItemId = itemId;
    this.isEditingRiskNotification = true;
    this.activeRiskNotificationItemObject = this.riskNotificationsList.find(item => item.id === itemId);

    const selectedPositionsTemp = [];
    const selectedGroupsUsersTemp = [];

    setEditPage(keys.RISK_NOTIFICATIONS, itemId);

    this.activeRiskNotificationItemObject.position.map(item => selectedPositionsTemp.push({ key: item.id.toString(), label: item.name }));
    this.activeRiskNotificationItemObject.groups.map(item => selectedGroupsUsersTemp.push({ key: item.id.toString(), label: item.name }));

    this.selectedPositions = selectedPositionsTemp;
    this.selectedGroupsUsers = selectedGroupsUsersTemp;
    this.activeX = this.activeRiskNotificationItemObject.risk.x;
    this.activeY = this.activeRiskNotificationItemObject.risk.y;
    this.isEditingRiskNotification = true;
    this.isCreatingRiskNotification = false;
  }



  @action
  searchGroupsUsers = async (name) => {
    this.fetchingGroupsUsers = true;

    try {
      const response = await searchTextRequest({ page: 1, name });

      const tempArray = [];

      response.results.map(item => {
        return tempArray.push({ id: item.id.toString(), name: item.name });
      });

      runInAction(() => {
        this.groupsUsersList = tempArray;
      })
    } catch (error) {
      if (error && error.errors && error.errors) {
        const errorText = Object.values(error.errors)[0][0];
        runInAction(() => {
          this.errorText = errorText;
          this.showErrorModal = true;
        })
      }
    } finally {
      runInAction(() => {
        this.fetchingPosition = false;
      })
    }
  }

  @action
  createRiskNotification = async ({ name, send_by_email, send_by_sms }) => {

    this.request = true;

    try {
      const risk = this.activeRiskObj.id;
      const position = [];
      const groups = [];

      this.selectedPositions.map(item => {
        const id = Number(item.key);
        position.push(id);
        return item;
      });

      this.selectedGroupsUsers.map(item => {
        const id = Number(item.key);
        groups.push(id);
        return item;
      });

      const responseCreate = await createRiskNotificationRequest({ name, position, risk, groups, send_by_email, send_by_sms });
      const responseItem = await getRiskNotificationItemRequest({ id: responseCreate.id });

      const itemOfResponsible = {
        x: responseItem.risk.x,
        y: responseItem.risk.y,
        id: responseItem.id,
        riskCategoryId: responseItem.risk.id
      };

      runInAction(() => {
        this.riskNotificationsList.push(responseItem);
        this.arrayOfResponsible.push(itemOfResponsible);
        this.isCreatingRiskNotification = false;
        this.isEditingRiskNotification = false;
      });

    } catch (error) {
      if (error && error.errors && error.errors) {
        const errorText = Object.values(error.errors)[0][0];
        runInAction(() => {
          this.errorText = errorText;
          this.showErrorModal = true;
        })
      }
    } finally {
      runInAction(() => {
        this.request = false;
      })
    }
  }


  @action
  editRiskNotification = async ({ name, send_by_email, send_by_sms }) => {



    this.request = true;

    try {
      const risk = this.activeRiskObj.id;
      const position = [];
      const groups = [];

      this.selectedPositions.map(item => {
        const id = Number(item.key);
        position.push(id);
        return item;
      });

      this.selectedGroupsUsers.map(item => {
        const id = Number(item.key);
        groups.push(id);
        return item;
      });

      const responseEdit = await editRiskNotificationRequest({ id: this.activeRiskNotificationItemObject.id, name, position, risk, groups, send_by_email, send_by_sms });
      const responseItem = await getRiskNotificationItemRequest({ id: responseEdit.id });

      const itemOfResponsible = {
        x: responseItem.risk.x,
        y: responseItem.risk.y,
        id: responseItem.id,
        riskCategoryId: responseItem.risk.id
      };

      const indexRiskNotificationsItem = this.riskNotificationsList.findIndex(item => item.id === this.activeRiskNotificationItemObject.id);
      const indexArrayOfResp = this.arrayOfResponsible.findIndex(item => (item.x === this.activeX && item.y === this.activeY));

      runInAction(() => {
        this.riskNotificationsList[indexRiskNotificationsItem] = responseItem;
        this.arrayOfResponsible.splice(indexArrayOfResp, 1);
        this.arrayOfResponsible.push(itemOfResponsible);

        this.arrayOfResponsible.push(itemOfResponsible);
        this.isCreatingRiskNotification = false;
        this.isEditingRiskNotification = false;
      });

    } catch (error) {
      if (error && error.errors && error.errors) {
        const errorText = Object.values(error.errors)[0][0];
        runInAction(() => {
          this.errorText = errorText;
          this.showErrorModal = true;
        })
      }
    } finally {
      runInAction(() => {
        this.request = false;
      })
    }
  }

  @action
  setActiveItem = (x, y) => {
    this.activeX = x;
    this.activeY = y;
    const index = this.arrayOfResponsible.findIndex(item => (item.x === x && item.y === y));

    if (index > -1) {
      //if is created object of risk notification
      this.activeRiskObj = this.arrayOfResponsible[index].risk;
      this.isEditingRiskNotification = true;
      this.isCreatingRiskNotification = false;

      // const indexNotifRisk = this.riskNotificationsList.findIndex(item => (item.risk.x === x && item.risk.y === y));
      //
      // if(indexNotifRisk >-1) this.activeRiskNotificationItemObject = this.riskNotificationsList[indexNotifRisk];
    } else {
      const item = this.riskItemsList.find(item => (item.x === x && item.y === y));
      this.activeRiskObj = item;
      // this.isEditingRiskNotification = true;
    }

  }

  @action
  setSelectedPositions = (list) => {
    this.selectedPositions = list;
  }

  @action
  setSelectedGroupUsers = (list) => {
    this.selectedGroupsUsers = list;
  }

  @action
  setShowModalError = show => this.showErrorModal = show;

  @action
  deleteRiskNotification = async () => {
    this.request = true;

    try {
      const id = this.activeRiskNotificationItemId;
      await deleteRiskNotificationRequest({ id });

      removeCreateEditFromRoutePage(keys.RISK_NOTIFICATIONS)

      const index = this.riskNotificationsList.findIndex(item => item.id === id);


      runInAction(() => {
        const indexArrayOfResp = this.arrayOfResponsible.findIndex(item => item.id === id);

        if (indexArrayOfResp > -1) this.arrayOfResponsible.splice(indexArrayOfResp, 1);

        this.riskNotificationsList.splice(index, 1);
        this.activeRiskNotificationItemObject = {};
        this.isEditingRiskNotification = null;
        this.isCreatingRiskNotification = null;
        this.selectedPositions = [];
        this.selectedGroupsUsers = [];
      })

    } catch (error) {
      if (error && error.errors && error.errors) {
        const errorText = Object.values(error.errors)[0][0];
        runInAction(() => {
          this.errorText = errorText;
          this.showErrorModal = true;
        })
      }
    }
    finally {
      runInAction(() => {
        this.request = false;
      })
    }
  }

  @action
  getAllRiskNotification = async (name = "") => {
    this.request = true;

    try {
      const response = await getRiskNotificationsRequest({ name });
      const arrayOfItems = [];

      response.results.map(item => {
        arrayOfItems.push({
          x: item.risk.x,
          y: item.risk.y,
          id: item.id,
          riskCategoryId: item.risk.id
        });
        return item;
      });
      runInAction(() => {
        this.arrayOfResponsible = arrayOfItems;
        this.riskNotificationsList = response.results;
        this.isSuperuser = response.is_superuser;
      });
    } catch (error) {
      if (error && error.errors && error.errors) {
        const errorText = Object.values(error.errors)[0][0];
        runInAction(() => {
          this.errorText = errorText;
          this.showErrorModal = true;
        })
      }
    } finally {
      runInAction(() => {
        this.request = false;
      })
    }
  }

  @action
  getAllRisks = async () => {
    try {
      const response = await getPaginatedDataRequest({ search_in: 'risk_category', value: '' });

      runInAction(() => {
        this.riskItemsList = response;
      })

    } catch (error) {
      if (error && error.errors && error.errors) {
        const errorText = Object.values(error.errors)[0][0];
        runInAction(() => {
          this.errorText = errorText;
          this.showErrorModal = true;
        })
      }
    } finally {
    }
  }

}

