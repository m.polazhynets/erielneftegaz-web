import LoginStore from "./LoginStore";
import MenuStore from "./MenuStore";
import RiskMapIssuesStore from "./RiskMapIssuesStore";
import StructureSubdivisionStore from './StructureSubdivisionStore';
import TextSectionStore from "./TextSectionStore";
import PositionStore from "./PositionStore";
import GeopositionStore from "./GeopositionStore";
import SubdivisionStore from "./SubdivisionStore";
import UsersStore from "./UsersStore";
import CreateCategoryRiskStore from "./CreateCategoryRiskStore";
import MessagesStore from "./MessagesStore";
import UsersGroupsStore from "./UsersGroupsStore";
import RiskNotificationStore from "./RiskNotificationStore";

import { create } from "mobx-persist";
import ReportsStore from "./ReportsStore";
import UsersMobileStore from "./UsersMobileStore";





const hydrate = create();

class Store {

    loginStore = new LoginStore();
    menuStore= new MenuStore();
    riskMapIssuesStore= new RiskMapIssuesStore();
    textSectionStore= new TextSectionStore();
    geopositionStore= new GeopositionStore();
    positionStore= new PositionStore();
    messagesStore = new MessagesStore();
    usersStore = new UsersStore();
    createCategoryRiskStore = new CreateCategoryRiskStore();
    structureSubdivisionStore = new StructureSubdivisionStore();
    subdivisionStore= new SubdivisionStore();
    usersGroupsStore= new UsersGroupsStore();
    riskNotificationStore= new RiskNotificationStore();
    reportsStore = new ReportsStore();
    usersMobileStore = new UsersMobileStore();

    constructor() {
        hydrate("loginPersist", this.loginStore);
    }


}


const store = new Store();

export default store;


