import {action, configure, runInAction, observable} from 'mobx';
import {
  createRiskCategoryRequest,
  deleteCategoryRequest,
  getRiskCategoryItemRequest,
  editRiskCategoryRequest,
  getAllRiskCategories
} from "../requests/createCategoryRiskRequests";
import * as keys from '../keys';
import {getPaginatedDataRequest} from "../requests/userRequests";
import {removeCreateEditFromRoutePage, setCreatePage, setEditPage, setParams} from '../utils';


configure({enforceActions: 'observed'});


export default class CreateCategoryRiskStore {

  @observable request = false;
  @observable arrayOfResponsible = [];
  @observable riskCategoryList = [];
  @observable positionListReal = [];
  @observable selectedPosition = {};
  @observable selectedRiskItem = {};
  @observable positionList = [];
  @observable showErrorModal = false;
  @observable errorText = '';
  @observable activeX = -1;
  @observable activeY = -1;
  @observable isEditMode = false;
  @observable isCreateMode = false;

  @action
  setActiveItem = (xParam, yParam) => {

    const x = Number(xParam);
    const y = Number(yParam);

    this.activeX = x;
    this.activeY = y;
    const index = this.arrayOfResponsible.findIndex(item => (item.x === x && item.y === y));

    if (index > -1) {
      this.isEditMode = true;
      this.isCreateMode = false;


      const id = this.arrayOfResponsible[index].riskCategoryId;
      if(id >= 0) {
        this.getRiskItemCategory({id});
        setEditPage(keys.CREATE_CATEGORY_RISKS, id);
        setParams('x',x, true);
        setParams('y',y, true);
      }

    } else {
      this.isEditMode = false;
      this.isCreateMode = true;
      setCreatePage(keys.CREATE_CATEGORY_RISKS);
      setParams('x',x, true);
      setParams('y',y, true);
    }

  }

  @action
  cancelButtonClicked = () => {
    removeCreateEditFromRoutePage(keys.CREATE_CATEGORY_RISKS);
    setParams('x');
    setParams('y');
    this.isEditMode = false;
    this.isCreateMode = false;
    this.selectedPosition = {};
    this.selectedRiskItem = {};
  }

  @action
  setSelectedPosition = (key) => {
    const selecId = Number(key);
    this.selectedPosition = this.positionListReal.find(item => item.id === selecId);
  }

  @action
  setShowModalError = show => this.showErrorModal = show;

  @action
  getPaginatedDataRiskCategory = async () => {
    try {
      this.riskCategoryList = await getPaginatedDataRequest({search_in: 'risk_category', value: ''});
    } catch (error) {
      if (error && error.errors && error.errors) {
        const errorText = Object.values(error.errors)[0][0];
        runInAction(() => {
          this.errorText = errorText;
          this.showErrorModal = true;
        })
      }
    } finally {

    }
  }

  @action
  createCategoryRisk = async ({name}) => {
    this.request = true;
    const risk = this.riskCategoryList.find(item => {
      return (item.x === this.activeX && item.y === this.activeY);
    });

    const positionId = this.selectedPosition.id;
    try {
      const response = await createRiskCategoryRequest({
        name,
        risk: risk.id,
        position: positionId
      });

      const {x, y} = risk;


      const index = this.arrayOfResponsible.findIndex(item => (item.x === x && item.y === y));

      const objectToTable = {x, y, id: positionId, riskCategoryId: response.id};

      if (index > -1) {
        runInAction(() => {
          this.arrayOfResponsible = objectToTable;
        })
      } else {
        runInAction(() => {
          this.arrayOfResponsible.push(objectToTable);
        })
      }

      this.cancelButtonClicked();

    } catch (error) {
      if (error && error.errors && error.errors) {
        const errorText = Object.values(error.errors)[0][0];
        runInAction(() => {
          this.errorText = errorText;
          this.showErrorModal = true;
        })
      }
    } finally {
      runInAction(() => {
        this.request = false;
      })
    }
  }

  @action
  getRiskItemCategory = async ({id}) => {
    this.request = true;

    try {
      const response = await getRiskCategoryItemRequest({id});

      const {
        id: categoryId,
        risk,
        position,
        name: categoryName,
      } = response;

      const {id: positionId, name: positionName} = position;


      runInAction(() => {
        this.selectedPosition = position;
        this.selectedRiskItem = {categoryName, categoryId, positionName, positionId, riskId: risk.id};
      });


    } catch (error) {
      if (error && error.errors && error.errors) {
        const errorText = Object.values(error.errors)[0][0];
        runInAction(() => {
          this.errorText = errorText;
          this.showErrorModal = true;
        })
      }
    } finally {
      runInAction(() => {
        this.request = false;
      })
    }
  }


  @action
  editRiskCategory = async ({name}) => {

    this.request = true;
    try {

      const {riskId, categoryId, positionId} = this.selectedRiskItem;

      let position = positionId;

      if(this.selectedPosition.id) position = this.selectedPosition.id;


      const response = await editRiskCategoryRequest({id: categoryId, name, risk: riskId, position});
      const index = this.riskCategoryList.findIndex(item => (item.x === this.activeX && item.y === this.activeY));

      const objectToTable = {x: this.activeX, y: this.activeY, id: positionId, riskCategoryId: response.id};

      this.riskCategoryList[index] = objectToTable;
      this.cancelButtonClicked();

    } catch (error) {
      if (error && error.errors && error.errors) {
        const errorText = Object.values(error.errors)[0][0];
        runInAction(() => {
          this.errorText = errorText;
          this.showErrorModal = true;
        })
      }
    } finally {
      runInAction(() => {
        this.request = false;
      })
    }
  }

  @action
  getAllRisks = async () => {
    this.request = true;
    try {
      const response = await getAllRiskCategories();
      const arrayOfItems = [];

      response.results.map(item => {
        arrayOfItems.push({
          x: item.risk.x,
          y: item.risk.y,
          id: (item.position && item.position.id) ? item.position.id : null,
          riskCategoryId: item.id
        });
        return item;
      });
      runInAction(() => {
        this.arrayOfResponsible = arrayOfItems;
      })

    } catch (error) {
      if (error && error.errors && error.errors) {
        const errorText = Object.values(error.errors)[0][0];
        runInAction(() => {
          this.errorText = errorText;
          this.showErrorModal = true;
        })
      }
    } finally {
      runInAction(() => {
        this.request = false;
      })
    }
  }

  @action
  getListOfPositionSearch = async (value) => {

    try {
      const response = await getPaginatedDataRequest({search_in: 'position', value});
      const tempArr = [];

      response.map(item => tempArr.push({key: item.id, label: item.name}));
      runInAction(() => {
        this.positionListReal = response;
        this.positionList = tempArr;
      })

    } catch (error) {
      if (error && error.errors && error.errors) {
        const errorText = Object.values(error.errors)[0][0];
        runInAction(() => {
          this.errorText = errorText;
          this.showErrorModal = true;
        })
      }
    } finally {

    }
  }

  @action
  deleteCategoryItem = async () => {
    this.request = true;
    try {
      await deleteCategoryRequest({id: this.selectedRiskItem.categoryId});

      runInAction(() => {
        this.arrayOfResponsible = this.arrayOfResponsible.filter(item => item.riskCategoryId !== this.selectedRiskItem.categoryId);
      });
      this.cancelButtonClicked()

    } catch (error) {
      if (error && error.errors && error.errors) {
        const errorText = Object.values(error.errors)[0][0];
        runInAction(() => {
          this.errorText = errorText;
          this.showErrorModal = true;
        })
      }
    } finally {
      runInAction(() => {
        this.request = false;
      })
    }
  }


}

