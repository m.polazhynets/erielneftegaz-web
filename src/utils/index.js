import history from "../modules/history";
import qs from "qs";
import jsonFormData from "json-form-data";

const options = {
  showLeafArrayIndexes: true,
  includeNullValues: true,
  mapping: function(value) {
    if (typeof value === "boolean") {
      return +value ? "1" : "0";
    }
    return value;
  }
};

export function hasErrors(fieldsError) {
  return Object.keys(fieldsError).some(field => fieldsError[field]);
}

export const getParams = () => {
  return window.location.search.substring(1);
};

export const setParams = (nameParam, valueParam, setNull = false) => {
  let query = history.location.query;
	let pathname = history.location.pathname;
	let pageParams = qs.stringify(query);

  if (!setNull && !valueParam && query) {
    delete query[nameParam];
  } else {
    if (query) {
      query[nameParam] = valueParam;
    }
  }

  if (pageParams) pageParams = `?${pageParams}`;

  window.history.replaceState("", "", `${pathname}${pageParams}`);
};

export const isCreatePage = key =>
	history.location.pathname === `${key}/create`;
	
export const isEditPage = key => {
  const pathname = history.location.pathname;

  const isTheSameKey = pathname.includes(key);
  const isEdit = pathname.includes("/edit");

  return isTheSameKey && isEdit;
};

export const setCreatePage = key => {
  const queryString = window.location.search;
  history.push(`${key}/create${queryString}`);
};

export const setEditPage = (key, id) => {
  const queryString = window.location.search;
  history.push(`${key}/${id}/edit${queryString}`);
};

export const removeCreateEditFromRoutePage = key => {
  const queryString = window.location.search;
  history.push(`${key}${queryString}`);
};

export const getIdFromCreateRoute = key => {
  const idString = history.location.pathname.replace(`${key}/`, "");
  const indexSlash = idString.indexOf(`/`);
  const id = Number(idString.substr(0, indexSlash));

  return id;
};

export const checkPermission = (key, isAdmin) => {
  const editPage = isEditPage(key);
  const createPage = isCreatePage(key);

  if (!isAdmin && (editPage || createPage)) {
    return false; //forbidden
	}
	
  return true;
};

export const isAdminGetFromLocal = () => {
  const isAdmin = localStorage.getItem("isAdmin");
  if (isAdmin && isAdmin === "true") {
    return true;
  } else {
    return false;
  }
};

export const convertQuestionToFormData = data => {
  const { answers, ...dataCopy } = data;
  const ansObj = convertAnswerArrayToObject(answers);
  const dataForForm = { ...dataCopy, ...ansObj };
  const formData = jsonFormData(dataForForm, {
    ...options,
    initialFormData: new FormData()
  });

  return formData;
};

export const convertAnswerArrayToObject = answers => {
  let obj = {};

  if (!answers || (answers && answers.length === 0)) return;

  answers.map((item, i) => {
    const { image, id, answer } = item;

    obj[`answer_${i}_text`] = answer;
    obj[`answer_${i}_id`] = id;
    obj[`answer_${i}_image`] = image;

    if (!obj[`answer_${i}_text`]) obj[`answer_${i}_text`] = null;
    if (!obj[`answer_${i}_id`]) obj[`answer_${i}_id`] = null;
    if (!obj[`answer_${i}_image`]) obj[`answer_${i}_image`] = null;
    return item;
  });

  return obj;
};

// eslint-disable-next-line
export const PHONE_NUMBER = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;

// eslint-disable-next-line
export const EMAIL_VALIDATION_REGEX = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
