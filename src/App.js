import React, { Component } from 'react';
import { Router, Route, Switch, Redirect } from 'react-router-dom';
import { Layout, } from 'antd';
import { Provider, inject, observer } from 'mobx-react';
import history from "./modules/history";
import store from './stores';
import * as keys from './keys';
import 'antd/dist/antd.css';
import 'react-quill/dist/quill.snow.css'; // ES6
import './styles/styles.scss';

import {
    Login,
    Registration,
    Messages,
    TextSections,
    Users,
    Geoposition,
    RiskQuestions,
    RiskNotifications,
    // Statistics,
    Subdivision,
    StructureSubdivision,
    CreateCategoryRisks,
    Reports,
    // AdminSettings,
    ResetPassword,
    Positions,
    UsersGroups,
    UsersMobile,
    AccessPage,
    Page404,
} from './pages';

import SiderContainer from "./containers/SiderContainer";

const PrivateRoute = inject("loginStore")(observer(({component, loginStore, ...rest}) => {
    if(!loginStore || (!!loginStore && !loginStore.refresh)) return <Redirect to={keys.LOGIN} />
    return (
      <>
      <SiderContainer />
      <Route component={component} {...rest} />
    </>
    )
}));

const PublicRoute = ({ component, ...rest }) => {
    return <Route component={component} {...rest} />
};

class App extends Component {

    render() {
        return (
            <Provider {...store}>
                <Router history={history}>
                    
                    <Layout>

                        <Switch>
                            <PublicRoute exact path={keys.LOGIN} component={Login} />
                            <PublicRoute exact path={keys.RESET_PASSWORD} component={ResetPassword} />
                            <PublicRoute path={keys.REGISTRATION} component={Registration} />
                            <PrivateRoute path={keys.MESSAGES} component={Messages} />
                            <PrivateRoute path={keys.TEXT_SECTIONS} component={TextSections} />
                            <PrivateRoute path={keys.USERS_MOBILE} component={UsersMobile} />
                            <PrivateRoute path={keys.USERS_AD} component={Users} />
                            <PrivateRoute path={keys.USERS_GROUPS} component={UsersGroups} />
                            <PrivateRoute path={keys.GEOPOSITION} component={Geoposition} />
                            <PrivateRoute path={keys.RISK_QUESTIONS} component={RiskQuestions} />
                            <PrivateRoute path={keys.RISK_NOTIFICATIONS} component={RiskNotifications} />
                            {/*<PrivateRoute path={keys.STATISTICS} component={Statistics} />*/}
                            <PrivateRoute path={keys.SUBDIVISION} component={Subdivision} />
                            <PrivateRoute path={keys.POSITION} component={Positions} />
                            <PrivateRoute path={keys.STRUCTURE_SUBDIVISION} component={StructureSubdivision} />
                            <PrivateRoute path={keys.CREATE_CATEGORY_RISKS} component={CreateCategoryRisks} />
                            <PrivateRoute path={keys.REPORTS} component={Reports} />
                            {/*<PrivateRoute path={keys.ADMIN_SETTINGS} component={AdminSettings} />*/}
                            <PublicRoute path={keys.ACCESS_PAGE} component={AccessPage} />
                            <Route path="*">
                                <Page404/>
                            </Route>
                        </Switch>
                    </Layout>
                </Router>

            </Provider>
        )
    };
}

export default App;
