import api from './api';

export const getAllUsersGroupsRequest = ({page}) => {
  return api({
    url: 'users_groups/',
    method: 'GET',
    params: {
      page,
    }
  });
};

export const searchTextRequest = ({page, name}) => {
  return api({
    url: `users_groups/`,
    method: 'GET',
    params: {
      name,
      page,
    },
  })
}

export const createUsersGroupRequest = ({name, users}) => {
  return api({
    url: 'users_groups/',
    method: 'POST',
    data: {
      name,
      users,
    }
  });
};

export const editUsersGroupRequest = ({id, name, users}) => {

  return api({
    url: `users_groups/${id}/`,
    method: 'PUT',
    data: {
      name,
      users,
    }
  });
};

export const deleteUsersGroupRequest = ({id}) => {
  return api({
    url: `users_groups/${id}/`,
    method: 'DELETE',
  });
};


