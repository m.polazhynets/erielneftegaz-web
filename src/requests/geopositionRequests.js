import api from './api';

export const getAllFacilitiesRequest = ({page}) => {
    return api({
        url: 'facilities/',
        method: 'GET',
        params: {
            page,
        }
    });
};

export const searchFacilitiesRequest = ({page, name}) => {
    return api({
        url: 'facilities/',
        method: 'GET',
        params: {
            page,
            name,
        }
    });
};

export const createFacilitiesRequest = ({name, country, region, long, lat}) => {

    return api({
        url: 'facilities/',
        method: 'POST',
        data: {
            name,
            country,
            region,
            long, 
            lat,
        }
    });
};

export const updateFacilitiesRequest = ({id,name, country, region, long, lat}) => {

    return api({
        url: `facilities/${id}/`,
        method: 'PUT',
        data: {
            name,
            country,
            region,
            long, 
            lat,
        }
    });
};

export const deleteFacilitieRequest = ({id}) => {
    return api({
        url: `facilities/${id}/`,
        method: 'DELETE',
    });
}