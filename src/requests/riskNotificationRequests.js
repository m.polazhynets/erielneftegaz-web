import api from './api';

export const getRiskNotificationsRequest = ({name}) => {
  return api({
    url: 'risk_notifications/',
    method: 'get',
    params: {
      name,
    }
  });
};


export const createRiskNotificationRequest = ({name, send_by_sms, send_by_email, risk, position, groups}) => {
  return api({
    url: 'risk_notifications/',
    method: 'post',
    data: {
      name,
      send_by_sms,
      send_by_email,
      risk,
      position,
      groups,
    }
  });
};

export const getRiskNotificationItemRequest = ({id}) => {
  return api({
    url: `risk_notifications/${id}/`,
    method: 'get',
  });
};

export const editRiskNotificationRequest = ({id, send_by_sms, send_by_email, risk, position, groups}) => {
  return api({
    url: `risk_notifications/${id}/`,
    method: 'put',
    data: {
      send_by_sms,
      send_by_email,
      risk,
      position,
      groups,
    }
  });
};

export const deleteRiskNotificationRequest = ({id}) => {
  return api({
    url: `risk_notifications/${id}/`,
    method: 'delete',
  });
};

export const getPaginatedDataRequest = ({search_in, value}) => {
  return api({
    url: 'get_paginated_data',
    params: {
      search_in,
      value,
    }
  })
}
