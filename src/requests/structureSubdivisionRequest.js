import api from './api';

export const uploadFileSubdivisionsStructureRequest = (file) => {
  const bodyFormData = new FormData();
  bodyFormData.append('file', file);
  return api({
    url: 'subdivisions_structure/',
    data: bodyFormData,
    method: 'post',
    headers: {'Content-Type': 'multipart/form-data' }
  });
};

export const getSubdivisionsStructure = () => {
  return api({
    url: 'subdivisions_structure/',
    method: 'get',
  });
}

