import api from './api';

export const signInRequest = ({email, password}) => {
    return api({
        url: 'login/',
        method: 'POST',
        data: {
            email,
            password
        }
    });
};

export const signUpRequest = ({email, last_name, first_name, middle_name, phone, position_level,position_name, business_unit, branch}) => {
  
    return api({
        url: 'registration/',
        method: 'post',
        params: {},
        data: {email, last_name, first_name, middle_name, phone, position_level,position_name, business_unit, branch}
    });
};

export const refreshToken = ({refresh}) => {
    return api({
        url: 'refresh/',
        method: 'post',
        data: {refresh}
    })
}

export const resetPasswordRequest = ({email}) => {
    return api({
        url: 'reset_password/',
        method: 'post',
        data: {email}
    })
}

export const getRegistrationDataRequest = () => {
    return api({
        url: 'registration/',
        method: 'get',
    })
}

export const getProfileRequest = () => {
    return api({
        url: 'profile/',
        method: 'get',
    })
}


export const getPositionNameRequest = ({position}) => {
    return api({
        url: 'get_extra_register_data/',
        method: 'get',
        params: {position}
    })
}

