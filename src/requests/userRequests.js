import api from './api';

export const getAllUsersRequest = ({page}) => {
  return api({
    url: 'users/',
    params: {
      page
    }
  });
};

export const searchUsersRequest = ({page, name}) => {
  return api({
    url: 'users/',
    params: {
      page,
      name
    }
  });
}

export const editRequest = ({id, business_unit, phone, branch, position_level, subdivision, position, facility }) => {
  const idSubdivision = subdivision.id;
  const idPosition = position.id;

  return api({
    method: 'PUT',
    url: `users/${id}/`,
    data: {
      business_unit,
      phone,
      facility,
      branch,
      position_level,
      subdivision: idSubdivision,
      position: idPosition,
    }
  });
};

export const getPaginatedDataRequest = ({search_in, value}) => {
  return api({
    method: 'get',
    url: `get_paginated_data/`,
    params: {
      search_in,
      value
    }
  });
};

