import api from './api';

export const getAllRiskCategories = () => {
  return api({
    url: 'risk_categories/',
  });
}

export const createRiskCategoryRequest = ({name, risk, position}) => {
  return api({
    url: 'risk_categories/',
    method: 'POST',
    data: {
      name,
      risk,
      position
    }
  });
}

export const getRiskCategoryItemRequest = ({id}) => {
  return api({
    url: `risk_categories/${id}/`,
    method: 'GET',
  });
}

export const editRiskCategoryRequest = ({id,name, risk, position}) => {
  return api({
    url: `risk_categories/${id}/`,
    method: 'PUT',
    data: {
      name,
      risk,
      position,
    }
  });
}

export const deleteCategoryRequest = ({id}) => {
  return api({
    url: `risk_categories/${id}/`,
    method: 'DELETE',
  });
}

