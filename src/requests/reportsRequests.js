import {downloadFileRequest} from './api';

export const downloadFileReport = () => {
  downloadFileRequest('operational_reports/');
}