import api from './api';

export const createPositionsRequest = ({code,name,}) => {

    return api({
        url: 'positions/',
        method: 'POST',
        data: {
            code,
            name,
        }
    });
};


export const getAllPositionsRequest = ({page = 1}) => {
    return api({
        url: 'positions/',
        method: 'GET',
        params : {
            page
        }
    });
};

export const updatePositionsRequest = ({code,name,id}) => {

    return api({
        url: `positions/${id}/`,
        method: 'PUT',
        data: {
            code,
            name,
        }
    });
};

export const getPositionItemRequest = ({id}) => {
    return api({
        url: `positions/${id}/`,
        method: 'GET',
    });
};


export const deletePositionsRequest = ({id}) => {

    return api({
        url: `positions/${id}/`,
        method: 'DELETE',
       
    });
};

export const searchTextRequest = ({name, page =1}) => {
    return api({
        url: `positions/`,
        method: 'GET',
        params: {name, page},
    })
}

export const uploadCSVPositionsRequest = (file) => {
    const bodyFormData = new FormData();
    bodyFormData.append('file', file);
    return api({
        url: 'upload_positions/',
        data: bodyFormData,
        method: 'post',
        headers: {'Content-Type': 'multipart/form-data' }
    });
};
