import jsonFormData from "json-form-data";
import api from './api';

const options = {
  showLeafArrayIndexes: true,
  includeNullValues: true,
  mapping: function(value) {
    if (typeof value === 'boolean') {
      return +value ? '1': '0';
    }
    return value;
  }
};


export const getListCreatedCardRequest = ({page}) => {
  return api({
    url: 'spas_cards/',
    method: 'get',
    params: {
      page
    }
  });
};

export const getCreatedCardRequest = ({id}) => {
  return api({
    url: `spas_cards/${id}/`,
    method: 'get',
  })
}

export const getCreatedCardTasksRequest = ({id}) => {
  return api({
    url: `spas_cards/${id}/tasks/`,
    method: 'get',
  })
}

export const getCreatedCardUsersRequest = ({id, facility_id, name}) => {
  return api({
    url: `spas_cards/${id}/users/`,
    method: 'get',
    params: {
      facility_id,
      name
    }
  })
}

export const getCreatedCardStatusLogsRequest = ({id}) => {
  return api({
    url: `spas_cards/${id}/status_logs/`,
    method: 'get',
  })
}

export const createStatusLogRequest = ({id, status}) => {
  return api({
    url: `spas_cards/${id}/set_status/`,
    method: 'post',
    data: {
      status,
    }
  })
}

export const setNewResponsibleUserRequest = ({id, user_id}) => {
  return api({
    url: `spas_cards/${id}/set_user/`,
    method: 'post',
    data: {
      user_id
    }
  })
}

export const createSpasCardTasksRequest = ({expiry_date, performer, text, spas_card}) => {
  return api({
    url: `spas_card_tasks/`,
    method: 'post',
    data: {
      expiry_date,
      performer,
      text,
      spas_card
    }
  })
}

export const getSpasCardTaskRequest = ({id}) => {
  return api({
    url: `spas_card_tasks/${id}/`,
    method: 'get',
  })
}

export const updateSpasCardTaskRequest = ({id,expiry_date, performer, text, spas_card}) => {
  return api({
    url: `spas_card_tasks/${id}/`,
    method: 'put',
    data: {
      id,
      expiry_date,
      performer,
      text,
      spas_card
    }
  })
}

export const updateSpasCardTaskSetStatusRequest = ({id,status}) => {
  return api({
    url: `spas_card_tasks/${id}/set_status/`,
    method: 'post',
    data: {
      id,
      status,
    }
  })
}

export const getSpasCardTaskResultsRequest = ({id}) => {
  return api({
    url: `spas_cards/${id}/reports/`,
    method: 'get',
  })
}

export const createTaskResultsRequest = (dataForForm) => {
  const formData = jsonFormData(dataForForm, {...options, initialFormData: new FormData(),});
  return api({
    url: `task_result/`,
    method: 'post',
    data: formData,
    headers: {'Content-Type': 'multipart/form-data' }
  })
}

export const getTaskResultRequest = ({id}) => {
  return api({
    url: `task_result/${id}/`,
    method: 'get',
  })
}

export const updateTaskResultRequest = (id, dataForForm) => {
  const formData = jsonFormData(dataForForm, {...options, initialFormData: new FormData(),});
  return api({
    url: `task_result/${id}/`,
    method: 'put',
    data: formData,
    headers: {'Content-Type': 'multipart/form-data' }
  })
}







