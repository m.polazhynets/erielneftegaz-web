import api from './api';

export const createTextRequest = ({title, text, order}) => {
    return api({
        url: 'texts/',
        method: 'POST',
        data: {
            title,
            text,
            order,
        },
    });
};


export const getAllTextsRequest = ({page = 1}) => {
    return api({
        url: 'texts/',
        method: 'GET',
        params : {
            page
        }
    });
};

export const updateTextRequest = ({id,title, text, order}) => {

    return api({
        url: `texts/${id}/`,
        method: 'PUT',
        data: {
            title,
            text,
            order,
        },
    });
};

export const deleteTextRequest = ({id}) => {
    return api({
        url: `texts/${id}/`,
        method: 'DELETE',
    });
};

export const searchTextRequest = ({title, page =1}) => {
    return api({
        url: `texts/`,
        method: 'GET',
        params: {title, page},
    })
}
