import axios from "axios";

import * as keys from "../../keys";

axios.defaults.timeout = 10000; // ms;
axios.defaults.xsrfHeaderName = "X-CSRFTOKEN";
axios.defaults.xsrfCookieName = "csrftoken";

// for multiple requests
let isRefreshing = false;
let failedQueue = [];

const processQueue = (error, token = null) => {
  failedQueue.forEach((prom) => {
    if (error) {
      prom.reject(error);
    } else {
      prom.resolve(token);
    }
  });

  failedQueue = [];
};

axios.interceptors.response.use(
  function (response) {
    return response;
  },
  function (error) {
    const originalRequest = error.config;

    if (
      error &&
      error.response &&
      error.response.status === 401 &&
      error.config.url.includes("refresh")
    ) {
      localStorage.removeItem("access");
      localStorage.removeItem("refresh");
      localStorage.removeItem("isAdmin");
      window.location.pathname = keys.LOGIN;
      window.location.search = "";
      window.history.replaceState(null, null, keys.LOGIN);
      return Promise.reject(error);
    }

    if (error.response.status === 401 && !originalRequest._retry) {
      if (isRefreshing) {
        return new Promise(function (resolve, reject) {
          failedQueue.push({ resolve, reject });
        })
          .then((token) => {
            originalRequest.headers["Authorization"] = "Bearer " + token;
            return axios(originalRequest);
          })
          .catch((err) => {
            return Promise.reject(err);
          });
      }

      originalRequest._retry = true;
      isRefreshing = true;

      const refresh = localStorage.getItem("refresh");
      return new Promise(function (resolve, reject) {
        try {
          axios
            .post(`${process.env.REACT_APP_AUTH_API}refresh/`, { refresh })
            .then((resp) => {
              const { data } = resp;
              localStorage.setItem("access", data.access);
              axios.defaults.headers.common["Authorization"] =
                "Bearer " + data.access;
              originalRequest.headers["Authorization"] =
                "Bearer " + data.access;
              processQueue(null, data.access);
              resolve(axios(originalRequest));
            })
            .catch((err) => {
              processQueue(err, null);
              reject(err);
            })
            .finally(() => {
              isRefreshing = false;
            });
        } catch (e) {}
      });
    }

    return Promise.reject(error);
  }
);

const AUTH_REQUESTS = [
  "login/",
  "refresh/",
  "registration/",
  "reset_password/",
  "get_extra_register_data/",
];

const request = (options, resolve, reject) => {
  const { url, ...optionsData } = options;

  const access = localStorage.getItem("access");
  const isAuthRequest = AUTH_REQUESTS.findIndex((item) => item.includes(url));

  optionsData.headers = {};
  optionsData.headers["Offset-Minutes"] = new Date().getTimezoneOffset();

  if (isAuthRequest > -1) {
    optionsData.url = `${process.env.REACT_APP_AUTH_API}${options.url}`;
  } else {
    optionsData.url = `${process.env.REACT_APP_MAIN_API}${options.url}`;
  }

  if (access && isAuthRequest === -1) {
    optionsData.headers.Authorization = `Bearer ${access}`;
  }

  axios({ ...optionsData })
    .then((resp) => {
      const data = resp.data || {};

      resolve(data);

      if (data.code >= 400) {
        reject(resp.data);
      }
    })
    .catch((err) => {
      if (err && !err.response) {
        reject({});
      }

      if (err && err.response && err.response.data) {
        reject(err.response.data);
      }
    });
};

export const downloadFileRequest = (url) => {
  let optionsData = {
    url: url,
    method: "GET",
    responseType: "blob",
  };

  optionsData.url = `${process.env.REACT_APP_MAIN_API}${url}`;

  const access = localStorage.getItem("access");

  optionsData.headers = {};
  optionsData.headers.Authorization = `Bearer ${access}`;

  axios(optionsData).then((response) => {
    const url = window.URL.createObjectURL(new Blob([response.data]));
    const link = document.createElement("a");
    link.href = url;
    link.setAttribute("download", "Оперативная отчетность.csv");
    document.body.appendChild(link);
    link.click();
  });
};

const api = (options) =>
  new Promise((resolve, reject) => request(options, resolve, reject));

export default api;
