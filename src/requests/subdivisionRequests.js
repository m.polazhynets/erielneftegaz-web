import api from './api';

export const createSubdivisionRequest = ({code,name,}) => {

    return api({
        url: 'subdivisions/',
        method: 'POST',
        data: {
            code,
            name,
        }
    });
};


export const getAllSubdivisionsRequest = ({page = 1}) => {
    return api({
        url: 'subdivisions/',
        method: 'GET',
        params : {
            page
        }
    });
};

export const updateSubdivisionsRequest = ({code,name,id}) => {

    return api({
        url: `subdivisions/${id}/`,
        method: 'PUT',
        data: {
            code,
            name,
        }
    });
};

export const deleteSubdivisionRequest = ({id}) => {

    return api({
        url: `subdivisions/${id}/`,
        method: 'DELETE',
       
    });
};

export const searchTextRequest = ({name, page =1}) => {
    return api({
        url: `subdivisions/`,
        method: 'GET',
        params: {name, page},
    })
}

export const uploadCSVSubdivisionsRequest = (file) => {
    const bodyFormData = new FormData();
    bodyFormData.append('file', file);
    return api({
        url: 'upload_subdivisions/',
        data: bodyFormData,
        method: 'post',
        headers: {'Content-Type': 'multipart/form-data' }
    });
};
