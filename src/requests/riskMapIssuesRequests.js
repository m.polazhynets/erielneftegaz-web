import api from './api';
import {convertQuestionToFormData} from '../utils';

export const getQuestionsRequest = ({page}) => {
    return api({
        url: 'questions/',
        params: {
            page
        }
    });
};



export const createQuestionRequest = (data) => {
    const formData = convertQuestionToFormData(data)

    return api({
        url: 'questions/',
        data: formData,
        method: 'post',
        headers: {'Content-Type': 'multipart/form-data' }
    });
};

export const changeQuestionRequest = ({id,...data}) => {
    const formData = convertQuestionToFormData(data);

    return api({
        url: `questions/${id}/`,
        data: formData,
        method: 'PUT',
        headers: {'Content-Type': 'multipart/form-data' }
    });
};

export const deleteQuestionRequest = ({id}) => {
    return api({
        url: `questions/${id}`,
        method: 'DELETE',
    })
}

export const filterQuestionsRequest = ({name, page}) => {
    return api({
        url: `questions/`,
        method: 'GET',
        params: {name, page},
    });
}


