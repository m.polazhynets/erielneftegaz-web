import api from './api';

export const getAllMobileUsers = ({page}) => {
  return api({
    url: 'mobile_users/',
    method: 'GET',
    params: {
      page
    }
  });
};

export const getMobileUser = ({id}) => {
  return api({
    url: `mobile_users/${id}/`,
    method: 'GET',
  });
}

export const editMobileUser = ({id, is_active}) => {
  return api({
    url: `mobile_users/${id}/`,
    method: 'PUT',
    data: {
      is_active
    }
  });
}

export const searchUsersMobileRequest = ({page, name}) => {
  return api({
    url: 'mobile_users/',
    method: 'GET',
    params: {
      page,
      name,
    }
  });
};