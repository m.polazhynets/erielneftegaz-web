import React, {Component} from 'react';
import { Layout, Row, Col,  } from 'antd';
import { inject, observer } from "mobx-react";
import MainApplicationLayout from '../../layouts/MainApplicationLayout';
import SubdivisionList from '../../containers/SubdivisionList';
import CreateSubdivision from '../../components/CreateSubdivision';
import EditSubdivision from '../../components/EditSubdivision';
import styles from './styles/index.module.scss';
import Preloader from "../../components/Preloader";

const { Content } = Layout;


@inject('subdivisionStore')
@observer
class Subdivision extends Component {
  render() {
        const {creatingSubdivision, editingSubdivision, request, isSuperuser} = this.props.subdivisionStore;

        return (
          <>
                <MainApplicationLayout>
                     <Content className={[styles['main-application-layout'], 'main-application'].join(" ")}>
                            
                            <Row className={styles['main-application-layout__row']}>
                            <Col xs={24} sm={24} md={12} className={styles['main-application-layout__row-col']}>
                               <SubdivisionList/>
                            </Col>
                            <Col xs={24} sm={24}  md={12} className={styles['main-application-layout__row-col']}>
                                {creatingSubdivision && isSuperuser && <CreateSubdivision/> }
                                {editingSubdivision && isSuperuser && <EditSubdivision/>}
                            </Col>
                            </Row>
                        </Content>
                </MainApplicationLayout>
            <Preloader loading={request} />
          </>
                
             
              
        );
    }
}

export default Subdivision;