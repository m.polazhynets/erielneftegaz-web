import React, {Component} from 'react';
import {Form, Icon} from 'antd';
import {inject, observer} from 'mobx-react';
import { Redirect, Link } from 'react-router-dom';
import * as keys from '../../../keys';

import RegistationFormLayout from '../../../layouts/RegistationFormLayout';
import Input from "../../../components/Input";
import Button from "../../../components/Button";
import styles from './styles/index.module.scss';
import {hasErrors, EMAIL_VALIDATION_REGEX} from '../../../utils';
import ErrorBlock from "../../../components/ErrorBlock";


@inject('loginStore')
@observer


class ResetPassword extends Component {

    onSubmit = (e) => {
        e.preventDefault();


        this.props.form.validateFields((err, values) => {
            if (!err) {
                this.props.loginStore.resetPassword(values);
            }
        })
    }

    render() {
        const {
            getFieldDecorator,
            getFieldsError,
        } = this.props.form;
        const {error, access, message,} = this.props.loginStore;


      if(access) return <Redirect to={keys.MESSAGES} />

        return (
            <RegistationFormLayout>
                <Form onSubmit={this.onSubmit}>

                    <div className={styles['title']}>
                        <p className={styles['title__text']}>Восстановление пароля</p>
                    </div>
                    <div className={styles['reset-password']}>
                        <Link to={keys.LOGIN} className={styles['reset-password__text']}><Icon type="arrow-left" /> Назад</Link>
                    </div>
                     <Form.Item
                        key={"email"}
                    >
                        {getFieldDecorator('email', {
                            rules: [{
                                type: "email",
                                required: true,
                                message: 'Email обязательный'
                            }, {
                                pattern: EMAIL_VALIDATION_REGEX,
                                message: "Неправильный формат email!"
                            }],

                        })(<Input placeholder={"Email"}/>)}

                    </Form.Item>
                    <ErrorBlock errorText={error} />
                    {message && <div className={styles['message']}>{message}</div>}
                    <Button
                        htmlType="submit"
                        class_name_of_styles={styles['enter-button']}
                        disabled={hasErrors(getFieldsError())}
                    >
                        Отправить
                    </Button>
                </Form>
               
            </RegistationFormLayout>
        );
    }
}

export default Form.create()(ResetPassword);
