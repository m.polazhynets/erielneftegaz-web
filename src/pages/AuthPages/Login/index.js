import React, {Component} from 'react';
import {Form} from 'antd';
import {inject, observer} from 'mobx-react';
import { Redirect, Link, } from 'react-router-dom';
import * as keys from '../../../keys';

import RegistationFormLayout from '../../../layouts/RegistationFormLayout';
import Input from "../../../components/Input";
import Button from "../../../components/Button";
import styles from './styles/index.module.scss';
import {hasErrors, EMAIL_VALIDATION_REGEX} from '../../../utils';
import ErrorBlock from "../../../components/ErrorBlock";


@inject('loginStore')
@observer


class Login extends Component {
    componentDidMount(){
        this.props.loginStore.clearMessages();
    }

    onSubmit = (e) => {
        e.preventDefault();


        this.props.form.validateFields((err, values) => {
            if (!err) {
                this.props.loginStore.singIn(values);
            }
        })
    }

    render() {
        const {
            getFieldDecorator,
            getFieldsError,
        } = this.props.form;
        const {error, access} = this.props.loginStore;


      //  if(access) return <Redirect to={keys[`${activeTabName}`]} />
      if(access) return <Redirect to={keys.MESSAGES} />

        return (
            <RegistationFormLayout>
                <Form onSubmit={this.onSubmit}>

                    <div className={styles['title']}>
                        <p className={styles['title__text']}>Вход</p>
                    </div>
                     <Form.Item
                        key={"email"}
                    >
                        {getFieldDecorator('email', {
                            rules: [{
                                type: "email",
                                required: true,
                                message: 'Email обязательный'
                            }, {
                                pattern: EMAIL_VALIDATION_REGEX,
                                message: "Неправильный формат email!"
                            }],

                        })(<Input placeholder={"Email"}/>)}

                    </Form.Item>
                    <Form.Item key={"password"}>
                        {getFieldDecorator('password', {
                            rules: [{
                                required: true,
                                message: 'Пароль обязательный'
                            }]
                        })(<Input type={"password"} placeholder={"Пароль"}/>)}

                    </Form.Item>
                    <ErrorBlock errorText={error} />
                    <Button
                        htmlType="submit"
                        class_name_of_styles={styles['enter-button']}
                        disabled={hasErrors(getFieldsError())}
                    >
                        Войти
                    </Button>
                </Form>
                <div className={styles['reset-password']}>
                    <Link to={keys.REGISTRATION} className={styles['reset-password__text']}>Регистрация </Link> /
                    <Link to={keys.RESET_PASSWORD} className={styles['reset-password__text']}> Забыли пароль?</Link>
                </div>
            </RegistationFormLayout>
        );
    }
}

export default Form.create()(Login);
