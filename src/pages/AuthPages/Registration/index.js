import React, {Component} from 'react';
import {Form, Select, message, } from "antd";
import {inject, observer, } from 'mobx-react';
import { Redirect, Link } from 'react-router-dom';
import { toJS } from 'mobx';
import Cleave from "cleave.js/react";
import ErrorBlock from "../../../components/ErrorBlock";
import * as keys from '../../../keys';
import RegistationFormLayout from '../../../layouts/RegistationFormLayout';
import styles from './styles/index.module.scss';
import Input from "../../../components/Input";
import Button from "../../../components/Button";
import AutoComplete from "../../../components/AutoComplete";

const { Option } = Select;


@inject('loginStore')
@observer


class Registration extends Component {

    state = {
       
        position: false,
    };
    componentDidMount() {
        this.props.loginStore.getRegistrationData();
        this.props.loginStore.clearMessages();
    }

    onSubmit = (e) => {
        e.preventDefault();


        this.props.form.validateFields((err, values) => {
            if (!err) {
               if(this.state.position) {
                  this.props.loginStore.singUp(values);
               }
               else {
                    message.error('Пожалуйста, выберите название должности из списка');
               }
            }
        })
    }
    searchPosition = value => this.props.loginStore.getPositionName(value);

    render() {
        const { getFieldDecorator, } = this.props.form;
        const { position_level, business_unit, branch, isRegistered, error, positionNameArr, setSelectedPosition, setIsRegistered } = this.props.loginStore;

        const children_position_level = [];
        Object.entries(toJS(position_level)).forEach(([key, value]) => 
        children_position_level.push(<Option key={key}>{value}</Option>))

        const children_business_unit = [];
        Object.entries(toJS(business_unit)).forEach(([key, value]) => 
        children_business_unit.push(<Option key={key}>{value}</Option>))

        const children_branch = [];
        Object.entries(toJS(branch)).forEach(([key, value]) => 
        children_branch.push(<Option key={key}>{value}</Option>))

        if(isRegistered) {
            alert("Ваше сообщение отправлено на рассмотрение");
            setIsRegistered(false);
            return <Redirect to={keys.LOGIN} />
        }
        
        return (
            <RegistationFormLayout>
                <div className={styles['title']}>
                    <p className={styles['title__text']}>Регистрация</p>
                </div>
                <Form onSubmit={this.onSubmit}>
                <Form.Item key={"email"}>
                    {getFieldDecorator('email', {
                        rules: [{
                            required: true,
                            message: 'Обязательное поле'
                        }]
                    })(<Input placeholder={"Email"} />)}
                </Form.Item>
                <Form.Item key={"last_name"}>
                    {getFieldDecorator('last_name', {
                        rules: [{
                            required: true,
                            message: 'Обязательное поле'
                        }]
                    })(<Input placeholder={"Фамилия"} />)}

                </Form.Item>

                <Form.Item key={"first_name"}>
                    {getFieldDecorator('first_name', {
                        rules: [{
                            required: true,
                            message: 'Обязательное поле'
                        }]
                    })(<Input placeholder={"Имя"} />)}

                </Form.Item>

                <Form.Item key={"middle_name"}>
                    {getFieldDecorator('middle_name', {
                        rules: [{
                            required: true,
                            message: 'Обязательное поле'
                        }]
                    })(<Input placeholder={"Отчество"} />)}

                </Form.Item>
                    <Form.Item key={"phone"}>
                        {getFieldDecorator('phone', {
                            rules: [{
                                required: true,
                                message: 'Обязательное поле'
                            }]
                        })(
                        // <Input placeholder={"Ваш телефон"} />
                        <Cleave
                            className="cleave-input"
                            placeholder="+7 (000) 0000000"
                            key={{
                            blocks: [3, 3, 7],
                            delimiters: ["(", ")-"],
                            delimiterLazyShow: true,
                            prefix: "+7 ",
                            noImmediatePrefix: true,
                            numericOnly: true
                            }}
                            options={{
                            blocks: [3, 3, 7],
                            delimiters: ["(", ")-"],
                            delimiterLazyShow: true,
                            prefix: "+7 ",
                            noImmediatePrefix: true,
                            numericOnly: true
                            }}
                        />
                        )}

                    </Form.Item>
                    {/* <Form.Item key={"password"}>
                        {getFieldDecorator('password', {
                            rules: [{
                                required: true,
                                message: 'Пароль обязательный'
                            }]
                        })(<Input type={"password"} placeholder={"Создать пароль"} />)}

                    </Form.Item> */}
                      <div className={styles['position']}>
                        <Form.Item key={"position_level"}
                        // className='info-description__detail-block'
                        >
                            {getFieldDecorator('position_level', {
                                rules: [{
                                    required: true,
                                    message: 'Обязательное поле'
                                }]
                            })(<Select 
                                // option_array={[
                                // {label: 'Должность 1',text: 'значение 1' },
                                // {label: 'Должность 2',text: 'значение 2' },
                                // {label: 'Должность 3',text: 'значение 3' },
                                // ]} 
                                //option_array={position_level}
                                placeholder={"Уровень должности"} 
                                //value={undefined}  
                                className={styles['select']} 
                            
                                >
                                {children_position_level}
                                </Select>
                                )}
                        </Form.Item>
                    </div>
                
                   <div className={styles['position']}>
                         <Form.Item key={"position_name"}
                          className={['info-description__detail-block','autocomlete-input', 'autocomlete-input-register '].join(" ")}
                       // className={styles['select']} 
                        >
                            {getFieldDecorator('position_name', {
                                rules: [{
                                    required: true,
                                    message: 'Обязательное поле'
                                }]
                            })(
                                //  <Select 
                              
                                // placeholder={"Должность"} 
                               
                                // className={styles['select']} 
                            
                                // >
                                //     {children_position_level}
                                // </Select>

                                    <AutoComplete
                                        dataSource={toJS(positionNameArr)}
                                        onSearch={(value)=>{
                                            this.searchPosition(value);
                                            this.setState({position: false})
                                        }}
                                        onSelect={(position, {key})=>{
                                            setSelectedPosition(key);
                                            this.setState({position: true})
                                          }}
                                        placeholder={"Должность"} 
                                    />
                                )}
                        </Form.Item>
                    </div>
                   <div className={styles['business']}>
                       <div className={styles['business__item']}>
                    <Form.Item key={"business_unit"}>
                        {getFieldDecorator('business_unit', {
                            rules: [{
                                required: true,
                                message: 'Обязательное поле'
                            }]
                        })(<Select
                            // stylesSelectClassName={styles['select-component']}
                            // option_array={[
                            // {label: 'Бизнес единица 1',text: 'значение 1' },
                            // {label: 'Бизнес единица 2',text: 'значение 2' },
                            // {label: 'Бизнес единица 3',text: 'значение 3' },
                            // ]} 
                        placeholder={"Бизнес единица"} 
                        className={styles['select']} 
                        >
                            {children_business_unit}
                        </Select>
                        )
                        }

                    </Form.Item>
                       </div>
                       <div  className={styles['business__branch']}>
                    <Form.Item key={"branch"}>
                        {getFieldDecorator('branch', {
                            rules: [{
                                required: true,
                                message: 'Обязательное поле'
                            }]
                        })(<Select
                            // stylesSelectClassName={styles['select-component']}
                            // option_array={[
                            // {label: 'Должность 1',text: 'значение 1' },
                            // {label: 'Должность 2',text: 'значение 2' },
                            // {label: 'Должность 3',text: 'значение 3' },
                            // ]} 
                            
                            placeholder={"Филиал"} 
                            className={styles['select']} 
                            >
                                {children_branch}
                            </Select>)
                        }

                    </Form.Item>
                       </div>
                   </div>
                    <div className={styles['business']}>
                        {/* <div className={styles['business__item']}>
                            <Form.Item key={"occurrence"}>
                                {getFieldDecorator('occurrence', {
                                    rules: [{
                                        required: true,
                                        message: 'Месторождение обязательно'
                                    }]
                                })(<Select stylesSelectClassName={styles['select-component']} option_array={[
                                    {label: 'Месторождение 1',text: 'значение 1' },
                                    {label: 'Месторождение 2',text: 'значение 2' },
                                    {label: 'Месторождение 3',text: 'значение 3' },
                                ]} placeholder={"Месторождение"} />)
                                }

                            </Form.Item>
                        </div>
                        <div  className={styles['business__branch']}>
                            <Form.Item key={"well"}>
                                {getFieldDecorator('well', {
                                    rules: [{
                                        required: true,
                                        message: 'Куст/Скважина'
                                    }]
                                })(<Select
                                    stylesSelectClassName={styles['select-component']}
                                    option_array={[
                                    {label: 'Куст 1',text: 'значение 1' },
                                    {label: 'Куст 2',text: 'значение 2' },
                                    {label: 'Куст 3',text: 'значение 3' },
                                ]} placeholder={"Куст/Скважина"} />)
                                }

                            </Form.Item>
                        </div> */}
                    </div>
                <ErrorBlock errorText={error} />
                <Button htmlType="submit" class_name_of_styles={styles['save-button']}>
                    Отправить
                </Button>
                </Form>

                <div className={styles['reset-password']}>
                    <Link to={keys.LOGIN} className={styles['reset-password__text']}>Вход</Link>
                </div>
            </RegistationFormLayout>
        );
    }
}

export default Form.create()(Registration);
