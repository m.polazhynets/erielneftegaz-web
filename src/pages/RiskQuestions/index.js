import React, {Component} from 'react';
import { Layout, Row, Col,  } from 'antd';
import { inject, observer } from 'mobx-react';
import MainApplicationLayout from '../../layouts/MainApplicationLayout';
import RiskQuestionsList from '../../containers/RiskQuestionsList';
import RiskQuestionDescription from '../../containers/RiskQuestionDescription';
import styles from './styles/index.module.scss';
import Preloader from "../../components/Preloader";
import ErrorModal from "../../modals/ErrorModal";

const { Content} = Layout;

@inject('riskMapIssuesStore')
@observer

class RiskQuestions extends Component {

  componentDidMount() {

  }


  render() {
      const { request, showErrorModal, errorText, setShowModalError } = this.props.riskMapIssuesStore;
        return (
          <>
                <MainApplicationLayout>
                     <Content className={[styles['main-application-layout'], 'main-application'].join(" ")}>
                            
                            <Row className={styles['main-application-layout__row']}>
                            <Col xs={24} sm={24} md={12} className={styles['main-application-layout__row-col']}>
                               <RiskQuestionsList/>
                            </Col>
                            <Col xs={24} sm={24}  md={12} className={styles['main-application-layout__row-col']}>
                               <RiskQuestionDescription/> 

                            </Col>
                            </Row>
                        </Content>
                </MainApplicationLayout>
            <Preloader loading={request} />
            <ErrorModal toggleModalError={setShowModalError} errorText={errorText} visibleModalError={showErrorModal} />
            </>
                
             
              
        );
    }
}

export default RiskQuestions;