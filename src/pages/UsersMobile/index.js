import React, {Component} from 'react';
import { Layout, Row, Col,  } from 'antd';
import MainApplicationLayout from '../../layouts/MainApplicationLayout';
import ErrorModal from "../../modals/ErrorModal";
import styles from './styles/index.module.scss';
import {inject, observer} from "mobx-react";
import Preloader from "../../components/Preloader";
import UsersMobileList from "../../containers/UsersMobileList";
import EditUserMobile from "../../containers/EditUserMobile";


const {Content} = Layout;

@inject('usersMobileStore')
@observer

class UsersMobile extends Component {

  render() {

      const {request, showErrorModal, errorText, setShowModalError, isEditingUser, isSuperuser} = this.props.usersMobileStore;


        return (
          <>
                <MainApplicationLayout>
                     <Content className={styles['main-application-layout']}>
                            <Row className={styles['main-application-layout__row']}>
                                <Col xs={24} sm={24} md={12} className={styles['main-application-layout__row-col']}>            
                                <UsersMobileList/>

                                </Col>
                                <Col xs={24} sm={24}  md={12} className={styles['main-application-layout__row-col']}>
                                  {isSuperuser && isEditingUser &&  <EditUserMobile />}
                                </Col>
                            </Row>
                        </Content>
                  <ErrorModal visibleModalError={showErrorModal} toggleModalError={()=>setShowModalError(false)} errorText={errorText}/>
                </MainApplicationLayout>
            <Preloader loading={request}/>
          </>
        );
    }
}

export default UsersMobile;