import React, {Component} from 'react';
import { Layout, Row, Col,  } from 'antd';
import MainApplicationLayout from '../../layouts/MainApplicationLayout';
import RiskNotificationsList from '../../containers/RiskNotificationsList';
import CreateRiskNotification from "../../containers/CreateRiskNotification";
import EditRiskNotification from "../../containers/EditRiskNotification";
import styles from './styles/index.module.scss';
import {inject, observer} from "mobx-react";
import Preloader from "../../components/Preloader";
import ShowRiskNotificationView from "../../containers/ShowRiskNotificationView";


const { Content } = Layout;

@inject('riskNotificationStore')
@observer

class RiskNotifications extends Component {
    render() {
      const {isCreatingRiskNotification, isEditingRiskNotification, request, isSuperuser, activeRiskNotificationItemObject} = this.props.riskNotificationStore;
        return (
          <>
                <MainApplicationLayout>
                     <Content className={[styles['main-application-layout'], 'main-application'].join(" ")}>
                            
                            <Row className={styles['main-application-layout__row']}>
                            <Col xs={24} sm={24} md={12}>
                                <RiskNotificationsList/>
                            </Col>
                            <Col xs={24} sm={24}  md={12} className={styles['main-application-layout__row-col']}>
                              {isSuperuser && isCreatingRiskNotification && <CreateRiskNotification/>}
                              {isSuperuser && isEditingRiskNotification && <EditRiskNotification/>}
                              {!isSuperuser && isEditingRiskNotification &&
                              <ShowRiskNotificationView notificationItem={activeRiskNotificationItemObject} />
                              }
                            </Col>
                            </Row>
                        </Content>
                </MainApplicationLayout>
            <Preloader loading={request} />
</>
                
             
              
        );
    }
}

export default RiskNotifications;