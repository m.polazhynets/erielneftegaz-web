import React, {Component} from 'react';
import { Layout, Row, Col,  } from 'antd';
import MainApplicationLayout from '../../layouts/MainApplicationLayout';
import StatisticsList from '../../containers/StatisticsList';
import StatisticsDescription from '../../containers/StatisticsDescription';
import styles from './styles/index.module.scss';

const { Content } = Layout;
class Statistics extends Component {
    render() {
        return (
                <MainApplicationLayout>
                     <Content className={[styles['main-application-layout'], 'main-application'].join(" ")}>
                            
                            <Row className={styles['main-application-layout__row']}>
                            <Col xs={24} sm={24} md={12} className={styles['main-application-layout__row-col']}>
                                <StatisticsList/>
                            </Col>
                            <Col xs={24} sm={24}  md={12} className={styles['main-application-layout__row-col']}>
                                 <StatisticsDescription/> 
                            </Col>
                            </Row>
                        </Content>
                </MainApplicationLayout>

                
             
              
        );
    }
}

export default Statistics;