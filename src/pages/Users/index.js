import React, {Component} from 'react';
import { Layout, Row, Col,  } from 'antd';
import MainApplicationLayout from '../../layouts/MainApplicationLayout';
import UsersList from '../../containers/UsersList';
import UsersDescription from '../../components/UsersDescription';
import ErrorModal from "../../modals/ErrorModal";
import styles from './styles/index.module.scss';
import {inject, observer} from "mobx-react";
import Preloader from "../../components/Preloader";
import UserView from "../../containers/UserView";


const { Content } = Layout;

@inject('usersStore')
@observer

class Users extends Component {

    render() {

      const {isEditingMode, request, showErrorModal, errorText, setShowModalError, isSuperuser, selectedUserData} = this.props.usersStore;


      // if(!checkPermission(keys.USERS_AD, isSuperuser)) return <Redirect to={keys.ACCESS_PAGE} />



        return (
          <>
                <MainApplicationLayout>
                     <Content className={styles['main-application-layout']}>
                            {/* <div style={{ padding: 24, background: '#fff', minHeight: 360 }}>content</div> */}
                            <Row className={styles['main-application-layout__row']}>
                                <Col xs={24} sm={24} md={12} className={styles['main-application-layout__row-col']}>            
                                <UsersList/>

                                </Col>
                                <Col xs={24} sm={24}  md={12} className={styles['main-application-layout__row-col']}>
                                  {!isSuperuser && isEditingMode && <UserView  item={selectedUserData} />}
                                  {isSuperuser  && isEditingMode && <UsersDescription /> }
                                </Col>
                            </Row>
                        </Content>
                  <ErrorModal visibleModalError={showErrorModal} toggleModalError={()=>setShowModalError(false)} errorText={errorText}/>
                </MainApplicationLayout>
            <Preloader loading={request}/>
          </>
        );
    }
}

export default Users;