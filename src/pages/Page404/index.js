import React from 'react';
import styles from './styles/index.module.scss';

const Page404 = () => {
  return (
    <div className={styles['container']}>
      <h3 className={styles['container__title']}>404</h3>
      <p className={styles['container__info']}>К сожалению, страница не найдена</p>

    </div>
  )
}

export default Page404