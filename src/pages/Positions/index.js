import React, {Component} from 'react';
import { Layout, Row, Col,  } from 'antd';
import { inject, observer } from "mobx-react";
import MainApplicationLayout from '../../layouts/MainApplicationLayout';
import PositionList from '../../containers/PositionList';
import CreatePosition from '../../components/CreatePosition';
import EditPosition from '../../components/EditPosition';
import styles from './styles/index.module.scss';
import Preloader from "../../components/Preloader";

const { Content } = Layout;


@inject('positionStore')
@observer
class Position extends Component {
  render() {
        const {creatingPosition,  editingPosition, request, isSuperuser} = this.props.positionStore;

        return (
          <>
                <MainApplicationLayout>
                     <Content className={[styles['main-application-layout'], 'main-application'].join(" ")}>
                            
                            <Row className={styles['main-application-layout__row']}>
                            <Col xs={24} sm={24} md={12} className={styles['main-application-layout__row-col']}>
                               <PositionList/>
                            </Col>
                            <Col xs={24} sm={24}  md={12} className={styles['main-application-layout__row-col']}>
                                {creatingPosition && isSuperuser && <CreatePosition/> }
                                {editingPosition && isSuperuser && <EditPosition/>}
                            </Col>
                            </Row>
                        </Content>
                </MainApplicationLayout>
            <Preloader loading={request} />
          </>
                
             
              
        );
    }
}

export default Position;