import React, {Component} from 'react';
import { Layout, Row, Col,  } from 'antd';
import MainApplicationLayout from '../../layouts/MainApplicationLayout';
import ProbabilityTable from "../../containers/ProbabilityTable";
import styles from './styles/index.module.scss';
import {inject, observer} from "mobx-react";
import CreateCategoryRiskForm from "../../containers/CreateCategoryRiskForm";
import Preloader from "../../components/Preloader";
import ErrorModal from "../../modals/ErrorModal";
import EditCategoryRiskForm from "../../containers/EditCategoryRiskForm";
import history from "../../modules/history";

const { Content } = Layout;

@inject('createCategoryRiskStore')
@observer

class CreateCategoryRisks extends Component {

  componentDidMount() {
    const {getPaginatedDataRiskCategory, getAllRisks, setActiveItem} = this.props.createCategoryRiskStore;

    const {x = 0,y = 0} = history.location.query;


    const allRiskPromise = getAllRisks();
    const getPaginatedData = getPaginatedDataRiskCategory();

    Promise.all([allRiskPromise, getPaginatedData])
      .then(()=>{
        setActiveItem(x, y);
      });
  }

  render() {
      const {
        activeY,
        activeX,
        setActiveItem,
        arrayOfResponsible,
        request,
        showErrorModal,
        setShowModalError,
        errorText,
        isCreateMode,
        isEditMode,
      } = this.props.createCategoryRiskStore;

        return (
          <>
                <MainApplicationLayout>
                     <Content className={[styles['main-application-layout'], 'main-application'].join(" ")}>
                            
                            <Row className={styles['main-application-layout__row']}>
                            <Col xs={24} sm={24} md={12} className={styles['main-application-layout__row-col']}>
                                <div className={'page-block'}>
                                  <div className={styles['page-block__table-container']}>
                                  <ProbabilityTable
                                    isShowMode={false}
                                    probability={activeX}
                                    consequences={activeY}
                                    selectedItems={arrayOfResponsible.slice()}
                                    onClickItem={(x,y)=>setActiveItem(x,y)}
                                  />
                                  </div>
                                </div>
                            </Col>
                            <Col xs={24} sm={24}  md={12} className={styles['main-application-layout__row-col']}>
                              {isCreateMode&& <CreateCategoryRiskForm />}
                              {isEditMode&& <EditCategoryRiskForm />}
                            </Col>
                            </Row>
                        </Content>
                  <ErrorModal visibleModalError={showErrorModal} toggleModalError={()=>setShowModalError(false)} errorText={errorText}/>
                </MainApplicationLayout>
          <Preloader loading={request}/>
    </>

                
             
              
        );
    }
}

export default CreateCategoryRisks;