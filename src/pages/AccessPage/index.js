import React from 'react';
import styles from './styles/index.module.scss';

const AccessPage = () => {
  return (
    <div className={styles['container']}>
      <p className={styles['container__info']}>У вас недостаточно прав для просмотра этой страницы</p>

    </div>
  )
}

export default AccessPage