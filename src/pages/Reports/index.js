import React, {Component} from 'react';
import { Layout, Row, Col,  } from 'antd';
import MainApplicationLayout from '../../layouts/MainApplicationLayout';
import ReportView from "../../containers/ReportView";
import styles from './styles/index.module.scss';

const { Content } = Layout;
class Reports extends Component {
    render() {
        return (
                <MainApplicationLayout>
                     <Content className={[styles['main-application-layout'], 'main-application'].join(" ")}>
                            
                            <Row className={styles['main-application-layout__row']}>
                            <Col xs={24} sm={24} md={12}>
                               <ReportView />
                            </Col>
                            <Col xs={24} sm={24}  md={12} className={styles['main-application-layout__row-col']}>
                               
                            </Col>
                            </Row>
                        </Content>
                </MainApplicationLayout>

                
             
              
        );
    }
}

export default Reports;