import React, {Component} from 'react';
import {Layout, Form, Row, Col,} from 'antd';
import {inject, observer} from "mobx-react";
import {Redirect} from 'react-router-dom';
import MainApplicationLayout from '../../layouts/MainApplicationLayout';
import TextSectionBlock from '../../containers/TextSectionBlock';
import TextDescription from '../../components/TextDescription';
import CreateTextSection from '../../components/CreateTextSection';
import EditTextSection from '../../components/EditTextSection';
import Preloader from "../../components/Preloader";
import styles from './styles/index.module.scss';
import * as keys from '../../keys';
import {checkPermission} from "../../utils";

const {Content} = Layout;


@inject('textSectionStore')
@observer

class TextSections extends Component {


  renderCreatingEditBlock = () => {
    const {creatingText, editingText, isSuperuser} = this.props.textSectionStore;

    const check = checkPermission(keys.TEXT_SECTIONS,isSuperuser);
    if(!check) {
      return <Redirect to={keys.ACCESS_PAGE} />;
    }
    if (creatingText) {
      return <CreateTextSection/>
    } else if (editingText) {
      return <EditTextSection/>
    } else {
      return <TextDescription/>
    }
  }


  render() {
    const {request} = this.props.textSectionStore;



    return (
      <>
        <MainApplicationLayout>
          <Content className={styles['main-application-layout']}>
            {/* <div style={{ padding: 24, background: '#fff', minHeight: 360 }}>content</div> */}
            <Row className={styles['main-application-layout__row']}>
              <Col xs={24} sm={24} md={12} className={styles['main-application-layout__row-col']}>
                <TextSectionBlock/>

              </Col>
              <Col xs={24} sm={24} md={12} className={styles['main-application-layout__row-col']}>
                {this.renderCreatingEditBlock()}
              </Col>
            </Row>
          </Content>
        </MainApplicationLayout>
        <Preloader loading={request}/>
      </>

    );
  }
}


export default Form.create()(TextSections);