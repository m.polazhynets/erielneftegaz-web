import React, {Component} from 'react';
import { Layout,Row, Col,  } from 'antd';
import { inject, observer } from "mobx-react";
import MainApplicationLayout from '../../layouts/MainApplicationLayout';
import UsersGroupsView from '../../containers/UsersGroupsView';
import UsersGroupsList from '../../containers/UsersGroupsList';
import CreateUsersGroups from '../../components/CreateUsersGroups';
import EditUsersGroup from '../../components/EditUsersGroup';
import styles from './styles/index.module.scss';
import Preloader from "../../components/Preloader";
import history from "../../modules/history";

const { Content } = Layout;


@inject('usersGroupsStore')
@observer
class UsersGroups extends Component {
  render() {
        const {creatingUsersGroups, editingUsersGroups, request, isSuperuser, selectedItemsUsers, activeUsersGroupsName} = this.props.usersGroupsStore;
        const {id} = history.location.query;

        return (
          <>
                <MainApplicationLayout>
                     <Content className={[styles['main-application-layout'], 'main-application'].join(" ")}>
                            
                            <Row className={styles['main-application-layout__row']}>
                            <Col xs={24} sm={24} md={12} className={styles['main-application-layout__row-col']}>
                               <UsersGroupsList/>
                            </Col>
                            <Col xs={24} sm={24}  md={12} className={styles['main-application-layout__row-col']}>
                                {!isSuperuser && Number(id) >=0 && <UsersGroupsView usersGroupName={activeUsersGroupsName} usersGroupArray={selectedItemsUsers} /> }
                                {creatingUsersGroups && isSuperuser && <CreateUsersGroups/> }
                                {editingUsersGroups && isSuperuser && <EditUsersGroup/>}
                            </Col>
                            </Row>
                        </Content>
                </MainApplicationLayout>
            <Preloader loading={request} />
          </>
                
             
              
        );
    }
}

export default UsersGroups;