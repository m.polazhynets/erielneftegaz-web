import React, { Component } from "react";
import { Layout, Row, Col } from "antd";
import MainApplicationLayout from "../../layouts/MainApplicationLayout";
import IncomingApplication from "../../containers/IncomingApplication";
import CardSPAS from "../../containers/CardSPAS";
import styles from "./styles/index.module.scss";
import Preloader from "../../components/Preloader";
import { inject, observer } from "mobx-react";
import CreateCardSpasTask from "../../containers/CreateCardSpasTask";
import EditCardSpasTask from "../../containers/EditCardSpasTask";
import CreateResultForCardSpasTask from "../../containers/CreateResultForCardSpasTask";
import EditResultForCardSpasTask from "../../containers/EditResultForCardSpasTask";
import EditCardSpasTaskStatus from "../../containers/EditCardSpasTaskStatus";

const { Content } = Layout;

@inject("messagesStore")
@observer
class Messages extends Component {
  render() {
    const {
      request,
      isEditingTask,
      isCreatingTask,
      isCreatingResultOfTask,
      isEditingResultOfTask,
      isEditingStatusTask,
    } = this.props.messagesStore;

    return (
      <>
        <MainApplicationLayout>
          <Content className={["main-application"].join(" ")}>
            <Row className={styles["main-application-layout__row"]}>
              <Col
                xs={24}
                sm={24}
                md={12}
                className={styles["main-application-layout__left-item"]}
              >
                <IncomingApplication />
              </Col>
              <Col
                xs={24}
                sm={24}
                md={12}
                className={[
                  styles["main-application-layout__row-col"],
                  styles["main-application-layout-right"],
                ].join(" ")}
              >
                {!isEditingTask &&
                  !isCreatingTask &&
                  !isCreatingResultOfTask &&
                  !isEditingStatusTask &&
                  !isEditingResultOfTask && <CardSPAS />}
                {isEditingStatusTask && <EditCardSpasTaskStatus />}
                {isCreatingTask && <CreateCardSpasTask />}
                {isEditingTask && <EditCardSpasTask />}
                {isCreatingResultOfTask && <CreateResultForCardSpasTask />}
                {isEditingResultOfTask && <EditResultForCardSpasTask />}
              </Col>
            </Row>
          </Content>
        </MainApplicationLayout>
        <Preloader loading={request} />
      </>
    );
  }
}

export default Messages;
