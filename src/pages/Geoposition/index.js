import React, {Component} from 'react';
import { Layout, Row, Col,  } from 'antd';
import {inject, observer} from "mobx-react";
import MainApplicationLayout from '../../layouts/MainApplicationLayout';
import GeopositionList from '../../containers/GeopositionList';
import GeopositionDescription from '../../components/GeopositionDescription';

import styles from './styles/index.module.scss';
import EditGeoposition from '../../components/EditGeoposition';
import ErrorModal from '../../modals/ErrorModal';
import Preloader from "../../components/Preloader";

const { Content } = Layout;

@inject('geopositionStore')
@observer
class Geoposition extends Component {

    render() {
        const { creatingFacilities, editingFacilities, request, showErrorModal, errorText, setShowModalError} = this.props.geopositionStore;
        return (
          <>
                <MainApplicationLayout>
                     <Content className={styles['main-application-layout']}>
            
                            <Row className={styles['main-application-layout__row']}>
                                <Col xs={24} sm={24} md={12} className={styles['main-application-layout__row-col']}>            
                                    <GeopositionList/>
                                </Col>
                                <Col xs={24} sm={24}  md={12} className={styles['main-application-layout__row-col']}>
                                  {creatingFacilities && <GeopositionDescription/> }
                                  { editingFacilities && <EditGeoposition/>}

                                </Col>
                            </Row>
                        </Content>
                        <ErrorModal visibleModalError={showErrorModal} errorText={errorText} toggleModalError={setShowModalError}/>
                </MainApplicationLayout>
            <Preloader loading={request} />
          </>
                
             
              
        );
    }
}

export default Geoposition;