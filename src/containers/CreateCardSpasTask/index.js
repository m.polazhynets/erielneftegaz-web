import React, { Component } from 'react';
import { Element, default as Scroll } from "react-scroll";
import { Form, DatePicker, TimePicker } from "antd";
import moment from 'moment';
import InputComponent from "../../components/Input";
import ButtonComponent from "../../components/Button";
import AutoComplete from "../../components/AutoComplete";
import styles from './styles/index.module.scss';
import { inject, observer } from "mobx-react";
import locale from 'antd/es/date-picker/locale/ru_RU';

const scroll = Scroll.animateScroll;

const format = 'HH:mm';

@inject('messagesStore')
@observer

class CreateCardSpasTask extends Component {

  state = {
    width: window.innerWidth
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields({ force: true }, (err, values) => {
      const { selectedUserId } = this.props.messagesStore;

      if (!err) {
        const { text, date, time } = values;
        const expiry_date = `${moment(date).format('DD.MM.YYYY')} ${moment(time).format('HH:mm')}`;
        if (selectedUserId) {
          this.props.messagesStore.createSpasCardTask({ text, expiry_date });
        } else {
          this.props.form.setFields({
            performer: {
              value: values.user,
              errors: [new Error('Это поле обязательное')],
            },
          });
        }

        if (this.state.width <= 767) {
          scroll.scrollToTop()
        }
      }
    });
  };

  disabledDate = (current) => {
    // Can not select days before today and today
    return current && current < moment().endOf('day');
  }

  onSelectUser = (value, { key }) => {
    const { setSelectedUserId } = this.props.messagesStore;
    setSelectedUserId(key);
  }

  searchUser = value => {
    const { setSelectedUserId, getListUsersSearch } = this.props.messagesStore;
    setSelectedUserId(null);
    getListUsersSearch(value);
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    const { spasCardsUsers, cancelButtonClicked } = this.props.messagesStore;

    return (
      <div className={['info-description'].join(' ')}>
        <div className='info-description__title'>
          <Element name="scroll-to-subdivision-create" >
            Создание задачи
          </Element>
        </div>
        <Form onSubmit={this.handleSubmit}>
          <div className='info-description__detail'>
            <Form.Item
              className='info-description__detail-block'
              label='Коментарий:'
            >
              {getFieldDecorator('text', {
                rules: [
                  {
                    required: true,
                    message: 'Это поле обязательное'
                  }
                ]
              })(
                <InputComponent
                  class_name_of_styles={styles['users__input']}
                // disabled={true}
                />
              )}
            </Form.Item>
            <Form.Item
              className='info-description__detail-block'
              label='Новий исполнитель:'
            >
              {getFieldDecorator('performer', {
                initialValue: '',
                rules: [
                  {
                    required: true,
                    message: 'Это поле обязательное'
                  },
                ]
              })(
                <AutoComplete
                  customClassName='page-block__select'
                  onFocus={() => this.searchUser('')}
                  onSearch={this.searchUser}
                  dataSource={spasCardsUsers.slice()}
                  onSelect={this.onSelectUser}
                />
              )}
            </Form.Item>
            <div className={styles['date-time-block']}>
              <Form.Item
                className='info-description__detail-block'
                label='Дата завершения:'
              >
                {getFieldDecorator('date', {
                  rules: [
                    {
                      required: true,
                      message: 'Это поле обязательное'
                    }
                  ]
                })(
                  <DatePicker disabledDate={this.disabledDate} locale={locale} className={styles['date-picker']} />
                )}
              </Form.Item>
              <Form.Item
                className='info-description__detail-block'
                label='Время завершения:'
              >
                {getFieldDecorator('time', {
                  rules: [
                    {
                      required: true,
                      message: 'Это поле обязательное'
                    }
                  ]
                })(
                  <TimePicker format={format} locale={locale} placeholder={'Время'} className={styles['time-picker']} />
                )}
              </Form.Item>
            </div>

          </div>

          <div className={['page-block__action-block', styles['page-block__action-block-custom']].join(" ")}>
            <div className={['page-block__item', 'page-block__item-search'].join(' ')}>
              <ButtonComponent
                class_name_of_styles='page-block__button-custom-yellow'
                htmlType="submit"
              //disabled={hasErrors(getFieldsError()) || this.state.editorHtml.replace(/<(.|\n)*?>/g, '').trim().length === 0}
              >
                Создать
              </ButtonComponent>
            </div>
            <div className={['page-block__item', 'page-block__item-button'].join(' ')}>
              <ButtonComponent
                class_name_of_styles={'page-block__button-custom-white'}
                onClick={
                  () => {
                    cancelButtonClicked()
                    if (this.state.width <= 767) {
                      scroll.scrollToTop();
                    }
                  }
                }
              >
                Отменить
              </ButtonComponent>
            </div>
          </div>
        </Form>


      </div>
    );
  }
}

export default Form.create()(CreateCardSpasTask);