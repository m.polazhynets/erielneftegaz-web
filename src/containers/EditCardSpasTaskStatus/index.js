import React, {Component} from 'react';
import {Element, default as Scroll} from "react-scroll";
import {Form} from "antd";
import SelectComponent from "../../components/Select";
import ButtonComponent from "../../components/Button";
import styles from './styles/index.module.scss';
import {inject, observer} from "mobx-react";


const scroll = Scroll.animateScroll;


@inject('messagesStore')
@observer

class EditCardSpasTaskStatus extends Component {

  state = {
    width: window.innerWidth
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields({force: true},(err, values) => {

      if (!err) {
        this.props.messagesStore.editCardSpasTaskStatus();

        if(this.state.width<=767) {
          scroll.scrollToTop()
        }

      }
    });

  };


  render() {
    const {cancelButtonClicked, activeTaskObject, statusTask, setStatusTask} = this.props.messagesStore;



    return (
      <div className={['info-description'].join(' ')}>
        <div className='info-description__title'>
          <Element name="scroll-to-subdivision-create" >
            {`Изменение задачи №${activeTaskObject.id}`}
          </Element>
        </div>
        <Form onSubmit={this.handleSubmit}>
          {<div>
            <div className='label'>Изменить статус задачи</div>
            <div className={styles['status__select-block']}>
              <SelectComponent
                value={statusTask}
                onChange={setStatusTask}
                placeholder="Cтатус задачи"
                styles_select_classname='page-block__select'
                option_array={[
                  {label: 'В работе', value: 'open'},
                  {label: 'Выполнено', value: 'complete'},
                  {label: 'Не выполнено', value: 'fail'},
                ]}
              />
            </div>
          </div>}

          <div className={['page-block__action-block', styles['page-block__action-block-custom']].join(" ")}>
            <div className={['page-block__item', 'page-block__item-search'].join(' ')}>
              <ButtonComponent
                class_name_of_styles='page-block__button-custom-yellow'
                htmlType="submit"
              >
                Изменить
              </ButtonComponent>
            </div>
            <div className={['page-block__item', 'page-block__item-button'].join(' ')}>
              <ButtonComponent
                class_name_of_styles={'page-block__button-custom-white'}
                onClick={
                  () => {
                    cancelButtonClicked()
                    if (this.state.width <= 767) {
                      scroll.scrollToTop();
                    }
                  }
                }
              >
                Отменить
              </ButtonComponent>
            </div>
          </div>
        </Form>


      </div>
     );
  }
}

export default Form.create()(EditCardSpasTaskStatus);