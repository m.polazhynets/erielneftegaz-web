import React, {Component} from 'react';
import { Pagination, } from 'antd';
import {  animateScroll as scroll,  scroller } from 'react-scroll'
import { toJS } from 'mobx';
import {inject, observer} from "mobx-react";
import SearchInput from '../../components/SearchInput';
import ButtonComponent from '../../components/Button';
import TextBlock from '../../components/TextBlock';
import * as keys from '../../keys';
import history from "../../modules/history";
import ErrorModal from "../../modals/ErrorModal";
import {
  getIdFromCreateRoute,
  isCreatePage,
  isEditPage,
  removeCreateEditFromRoutePage,
  setCreatePage
} from "../../utils";


@inject('textSectionStore')
@observer

class TextSectionBlock extends Component {

  constructor() {
    super();
    this.state = {
      active: undefined,
      width: window.innerWidth,
      searchValue: '',
    };
  }

  componentDidMount() {
    const {getAllTexts, setSearchField, createTextSection, editTextSection} = this.props.textSectionStore;
    const {s, page} = history.location.query;
    const createPage = isCreatePage(keys.TEXT_SECTIONS);
    const editPage = isEditPage(keys.TEXT_SECTIONS);

    if(createPage) {
      createTextSection();
    }



    if(s){
      let pageNumber = 1;
      if(page) pageNumber = page;
      setSearchField(s,pageNumber)
        .then(()=>{
          if(editPage) {
            const id = getIdFromCreateRoute(keys.TEXT_SECTIONS);
            editTextSection(id);
          }
        });
      this.setState({searchValue: s});
    } else {
      let pageNumber = 1;
      if(page) pageNumber = page;
      getAllTexts(pageNumber)
        .then(()=>{
          if(editPage) {
            const id = getIdFromCreateRoute(keys.TEXT_SECTIONS);
            editTextSection(id);
          }
        });
    }

    window.addEventListener("resize", this.updateDimensions);
  }

  onClick = (key, id) => {

    this.setState({ active: key, });

    removeCreateEditFromRoutePage(keys.TEXT_SECTIONS);
    this.props.textSectionStore.setActiveTextSection(key);
  }
  updateDimensions = () => {
    this.setState({
      width: window.innerWidth
    });
  }
  handleChange = value => {
    const {s} = history.location.query;
    if(s) {
      this.props.textSectionStore.setSearchField(s, value);
    } else {
      this.props.textSectionStore.getAllTexts(value);
    }
  };

  onCreateButtonClick = () => {


    const {
      createTextSection,
      creatingText,
    } = this.props.textSectionStore;

    setCreatePage(keys.TEXT_SECTIONS);

    createTextSection();
    if(this.state.width<=767 && !creatingText ) {
      scroll.scrollToBottom()
    }
    else if(this.state.width<=767 && creatingText) {
      scroller.scrollTo('scroll-to-text-section-create', {
        duration: 800,
        delay: 0,
        smooth: 'easeInOutQuart'
      })
    }
  }

  render() {
    const {
      activeTextSection,
      setSearchField,
      showErrorModal,
      errorText,
      setShowModalError,
      textSections,
      page,
      totalPages,
      isSuperuser,
    } = this.props.textSectionStore;

    
    let resultArr = textSections && textSections.results && toJS(textSections.results);

    resultArr && resultArr.sort(function(a, b){
      return a.order-b.order
    });
   
    const textsectionlist  = resultArr && resultArr.map((value, index) => {
        return (
                <TextBlock
                  title={value.title} 
                  text={value.text}
                  keys={value.id} 
                  id={value.id} 
                  onClick={this.onClick} 
                  active={activeTextSection}
                  showEditBlock={isSuperuser}
                  key={value.title+index}
          /> 
        )
      });

    const { searchValue } = this.state;

      return (
            <div className='page-block'>
                 <div className='page-block__action-block'>
                     <div className={['page-block__item','page-block__item-search'].join(' ')}>
                        <SearchInput 
                          onSearch={value =>{
                            setSearchField(value, 1);
                            removeCreateEditFromRoutePage(keys.TEXT_SECTIONS);
                          }}
                          value={searchValue} 
                          onChange={({target})=>{this.setState({searchValue: target.value})}} 
                          classNameOfSeach = 'page-block__search'
                          />
                     </div>
                   {isSuperuser && <div className={['page-block__item','page-block__item-button'].join(' ')}>
                        <ButtonComponent 
                          class_name_of_styles ='page-block__button' 
                          onClick = {this.onCreateButtonClick}>
                            Создать
                        </ButtonComponent>
                     </div>}
                 </div>
               
                <div className='page-block__list'>
                  {textsectionlist}
                  {textSections && textSections.results && textSections.results.slice().length > 0 && (totalPages > 1) &&
                  <Pagination
                    current={page}
                    defaultPageSize={1}
                    onChange={this.handleChange}
                    total={totalPages}
                  />
                  }
                  
                </div>

              <ErrorModal visibleModalError={showErrorModal} toggleModalError={()=>setShowModalError(false)} errorText={errorText}/>
            </div>
      );
    }
    componentWillUnmount() {
      window.removeEventListener("resize", this.updateDimensions);
    }
}


export default TextSectionBlock;