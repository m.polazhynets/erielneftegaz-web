import React, { Component } from "react";
import { Pagination } from "antd";
import SearchInput from "../../components/SearchInput";
import Application from "../../components/Application";
import styles from "./styles/index.module.scss";
import { inject, observer } from "mobx-react";
import history from "../../modules/history";
import ErrorModal from "../../modals/ErrorModal";

@inject("messagesStore")
@observer
class IncomingApplication extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    const { page } = history.location.query;

    let pageValue = 1;
    if (page) pageValue = page;
    this.props.messagesStore.getListCreatedCard(pageValue);
  }

  onClick = (key) => {
    const { setActiveMessageId, getInfoByCard } = this.props.messagesStore;
    setActiveMessageId(key);
    getInfoByCard(key);
  };
  handleChange = (value) => {
    const { s } = history.location.query;
    if (s) {
      // this.props.messagesStore.setSearchField(s, value);
    } else {
      this.props.messagesStore.getListCreatedCard(value);
    }
  };

  renderMessagesList = () => {
    // date: "09.02.2020"
    // time: "10:40"
    // facility_name: "Куст АВ1452"
    // facility_id: 1
    // status: "new"
    // files: []

    const { messagesList, activeMessageId } = this.props.messagesStore;
    return messagesList.map((item, i) => {
      return (
        <React.Fragment key={i}>
          <Application
            title={item.id}
            type={item.status}
            facility={item.facility}
            time={`${item.time} ${item.date}`}
            keys={item.id}
            onClick={this.onClick}
            activeApplication={activeMessageId}
            owner={item.owner
            }
          />
        </React.Fragment>
      );
    });
  };

  render() {
    const {
      totalPages,
      page,
      messagesList,
      showErrorModal,
      setShowModalError,
      errorText,
    } = this.props.messagesStore;

    return (
      <div className={styles["incoming-application"]}>
        <SearchInput />
        <div className={styles["list-applications"]}>
          {this.renderMessagesList()}
          {messagesList && messagesList.length > 0 && totalPages > 1 && (
            <Pagination
              defaultCurrent={page}
              defaultPageSize={1}
              onChange={this.handleChange}
              total={totalPages}
            />
          )}
        </div>
        <ErrorModal
          visibleModalError={showErrorModal}
          toggleModalError={() => setShowModalError(false)}
          errorText={errorText}
        />
      </div>
    );
  }
}

export default IncomingApplication;
