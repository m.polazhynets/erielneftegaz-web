import React, {Component} from 'react';
import {Pagination} from 'antd';
import {inject, observer} from "mobx-react";
import SearchInput from '../../components/SearchInput';
import UserMobileBlock from "../../components/UserMobileBlock";
import classNames from 'classnames';
import styles from './styles/index.module.scss';
import history from "../../modules/history";
import {getIdFromCreateRoute, isEditPage, removeCreateEditFromRoutePage} from "../../utils";
import * as keys from "../../keys";

@inject('usersMobileStore')
@observer

class UsersMobileList extends Component {
    state = {
        activeUserBlock: undefined,
        width: window.innerWidth,
        searchValue: '',
      };


  componentDidMount() {

    const {getAllMobileUsers, setSearchField, selectActiveUser} = this.props.usersMobileStore;
    const {s, page: pageParam} = history.location.query;
    const page = Number(pageParam);
    const editPage = isEditPage(keys.USERS_MOBILE);

    if(s){
      let pageNumber = 1;
      if(page) pageNumber = page;

      setSearchField(s,pageNumber)
        .then(()=>{
          if(editPage) {
            const id = getIdFromCreateRoute(keys.USERS_MOBILE);
            selectActiveUser(id);
          }
        });


      this.setState({
        searchValue: s,
      });
    } else {
      let pageNumber = 1;
      if(page) pageNumber = page;
      getAllMobileUsers(pageNumber)
        .then(()=>{
          if(editPage) {
            const id = getIdFromCreateRoute(keys.USERS_MOBILE);
            selectActiveUser(id);
          }
        });
    }



    window.addEventListener("resize", this.updateDimensions);
  }
 
  onClick = (id) => {
    const {selectActiveUser} = this.props.usersMobileStore;
    selectActiveUser(id);
  }
  updateDimensions = () => {
    this.setState({
      width: window.innerWidth
    });
  }

  handleChange = (value) => {
    const {s} = history.location.query;
    if (s) {
      this.props.usersMobileStore.setSearchField(s, value);
    } else {
      this.props.usersMobileStore.getAllMobileUsers(value);
    }
  }

  renderUsersList = () => {
    const {usersMobileList, activeUserId} = this.props.usersMobileStore;

   return  usersMobileList.map(item => {
      const {fio, id, is_active} = item;
      return (
        <UserMobileBlock
          title={fio}
          onClick={this.onClick}
          isActive={is_active}
          isActiveUserBlock={id === activeUserId}
          key={id}
          id={id}
        />
      )
    })

  }

  searchFunc = value =>{
    const { setSearchField, clearEditMode } = this.props.usersMobileStore;

    setSearchField(value, 1);
    clearEditMode();
    removeCreateEditFromRoutePage(keys.USERS_MOBILE);
  }

  render() {
    const { page, totalPages, usersMobileList } = this.props.usersMobileStore;
    const {searchValue} = this.state;

      return (
            <div className='page-block'>
              <SearchInput
                value={searchValue}
                onSearch={this.searchFunc}
                onChange={({target})=>{this.setState({searchValue: target.value})}}
                classNameOfSeach = 'page-block__search'
              />
                <div className={classNames('page-block__list', styles['page-block__list'])}>
                  {this.renderUsersList()}

                  {usersMobileList.length > 0 && (totalPages > 1) && <Pagination
                   current={page}
                   defaultPageSize={1}
                   onChange={this.handleChange}
                   total={totalPages}
                 />}
                    
                  
                </div>
            </div>
      );
    }
    componentWillUnmount() {
      window.removeEventListener("resize", this.updateDimensions);
    }
}


export default UsersMobileList;