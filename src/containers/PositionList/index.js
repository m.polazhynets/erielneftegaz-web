import React, {Component} from 'react';
import { Pagination, } from 'antd';
import {Redirect} from 'react-router-dom';
import {  animateScroll as scroll, scroller } from 'react-scroll';
import {inject, observer} from "mobx-react";
import SearchInput from '../../components/SearchInput';
import ButtonComponent from '../../components/Button';
import PositionBlock from '../../components/PositionBlock';
import ErrorModal from "../../modals/ErrorModal";
import UploadFile from "../../components/UploadFile";
import history from "../../modules/history";
import styles from './styles/index.module.scss';
import * as routeKeys from '../../keys';
import {
  checkPermission,
  getIdFromCreateRoute,
  isEditPage,
  removeCreateEditFromRoutePage
} from "../../utils";

@inject('positionStore')
@observer

class PositionList extends Component {
  
    constructor() {
      super();
      this.state = {
        width: window.innerWidth,
        searchValue: '',
        file: [],
      };
    }

    componentDidMount() {
        const {getAllPosition,  setSearchField} = this.props.positionStore;

        const editPage = isEditPage(routeKeys.POSITION);
        // const createPage = isCreatePage(routeKeys.POSITION);

        const {s, page} = history.location.query;

        if(s){
          let pageNumber = 1;
          if(page) pageNumber = page;
          setSearchField(s,pageNumber)
            .then(()=>{
              this.setEditPageFunc(editPage);
            })
          this.setState({searchValue: s});
        } else {
          let pageNumber = 1;
          if(page) pageNumber = page;
          getAllPosition(pageNumber)
            .then(()=>{
              this.setEditPageFunc(editPage);
            })
        }
        window.addEventListener("resize", this.updateDimensions);
    }

    setEditPageFunc = (editPage) => {
      if(editPage){
        const id = getIdFromCreateRoute(routeKeys.POSITION)
        if(id){
          this.props.positionStore.editPosition(id);
        }
      }
    }
  
  updateDimensions = () => {
    this.setState({
      width: window.innerWidth
    });
  }
  onClick = (key, id) => {
    removeCreateEditFromRoutePage(routeKeys.POSITION);
    this.props.positionStore.setActivePosition(key);
  }
  
  clearSearchText = () => {
    const {s} = history.location.query;
    if(s){
      this.props.positionStore.setSearchField('');
      this.setState({searchValue: ''});
    }
  }
  handleChange = value => {
    const {s} = history.location.query;
    if(s) {
      this.props.positionStore.setSearchField(s, value);
    } else {
      this.props.positionStore.getAllPosition(value);
    }


  };

  setFile = (file) => {
    this.setState({file:[file]});
  }

  onRemove = () => this.setState({file: []});

  saveFile = () => {
    const [file] = this.state.file;
    this.props.positionStore.uploadCSVPosition(file.originFileObj);
    this.setState({file: []})
  }


  render() {
   
    const {
          positions,
          activePosition,
          showErrorModal,
      errorText,
      setShowModalError,
      createPosition,
      setSearchField,
      page,
      totalPages,
      creatingPosition,
          isSuperuser,
         } = this.props.positionStore;

    const {file} = this.state;


     
    const renderSubBlock = positions.map((value, index) => {
      return (
              <PositionBlock
                name={value.name} 
                code={value.code} 
                keys={value.id}
                showEditBlock={isSuperuser}
                id={value.id} 
                onClick={this.onClick}
                activePosition={activePosition}
                key={index}
        /> 
      )
    });

    const check = checkPermission(routeKeys.POSITION, isSuperuser);

    if(!check) return <Redirect to={routeKeys.ACCESS_PAGE} />
    

      return (
            <div className='page-block'>
                 <div className='page-block__action-block'>
                     <div className={['page-block__item','page-block__item-search'].join(' ')}>
                        <SearchInput 
                          onSearch={(value)=>{setSearchField(value)}}
                          value={this.state.searchValue} 
                          onChange={({target})=>{this.setState({searchValue: target.value})}} 
                          classNameOfSeach = 'page-block__search'
                        />
                     </div>
                   {isSuperuser && <div className={['page-block__item','page-block__item-button'].join(' ')}>
                        <ButtonComponent 
                            class_name_of_styles = 'page-block__button'
                            onClick = {()=>{ 
                              this.clearSearchText();
                              createPosition(true);
                              if(this.state.width<=767 && !creatingPosition  ) { //
                                scroll.scrollToBottom()
                              }
                              else if(this.state.width<=767 && creatingPosition) { //
                                 scroller.scrollTo('scroll-to-subdivision-create', {
                                  duration: 800,
                                  delay: 0,
                                  smooth: 'easeInOutQuart'
                                })
                             }
                            }}
                        >
                            Создать
                        </ButtonComponent>
                     </div>}
                    
                 </div>
              {isSuperuser && <div className={styles['upload-file-container']}>
                <div className={styles['upload-file-container__title']}>Выберите csv файл</div>
                  <UploadFile
                    accept=".csv"
                    setFile={this.setFile}
                    onRemove={this.onRemove}
                    fileList={file}
                  />
                {file && file.length > 0 && <div className={['page-block__item','page-block__item-button'].join(' ')}>
                  <ButtonComponent
                    class_name_of_styles = 'page-block__button'
                    onClick = {this.saveFile}
                  >
                    Сохранить файл
                  </ButtonComponent>
                </div>}
              </div>}

               
                <div className='page-block__list'>
                  
                    {renderSubBlock}
                  {positions && positions.length > 0 && (totalPages > 1) &&
                    <Pagination
                      current={page}
                      defaultPageSize={1}
                      onChange={this.handleChange}
                      total={totalPages}
                    />}
                   
              
                 
                  
                </div>
                <ErrorModal visibleModalError={showErrorModal} toggleModalError={()=>setShowModalError(false)} errorText={errorText}/>
            </div>
      );
    }
    componentWillUnmount() {
      window.removeEventListener("resize", this.updateDimensions);
    }
}


export default PositionList;