import React, {Component} from 'react';
import styles from './styles/index.module.scss';
import {Element} from "react-scroll";
import {Form} from "antd";
import InputComponent from "../../components/Input";
import ButtonComponent from "../../components/Button";
import {inject, observer} from "mobx-react";
import SelectComponent from "../../components/Select";
import {removeCreateEditFromRoutePage} from "../../utils";
import * as keys from "../../keys";

@inject('usersMobileStore')
@observer

class EditUserMobile extends Component {

  handleSubmit = e => {
    e.preventDefault();

    this.props.form.validateFields((err, values) => {
          this.props.usersMobileStore.editMobileUser(values);
    });
  };

  render() {

    const {statusOfUser, setStatusOfUser, cancelButtonClicked, activeUserObject} = this.props.usersMobileStore;



    const {
      getFieldDecorator,
    } = this.props.form;


    return (


      <div className='info-description'>
        <div className='info-description__title'>
          <Element name="scroll-to-subdivision-create">
            Изменение статуса пользователя МП
          </Element>
        </div>
        <Form onSubmit={this.handleSubmit}>
          <div className='info-description__detail-block'>
            <Form.Item
              className='info-description__detail-block'
              label='ФИО:'
            >
              {getFieldDecorator('name', {
                initialValue: activeUserObject.fio,
                rules: []
              })(
                <InputComponent
                  class_name_of_styles={styles['users__input']}
                  disabled
                />
              )}
            </Form.Item>

          </div>

          <div>
            <div className='label'>Изменить статус</div>
            <div className={styles['status__select-block']}>
              <SelectComponent
                value={statusOfUser}
                onChange={setStatusOfUser}
                placeholder="Cтатус сообщения"
                styles_select_classname='page-block__select'
                option_array={[
                  {label: 'Активен', value: true},
                  {label: 'Заблокирован', value: false},
                ]}
              />
            </div>
          </div>

          <div className={['page-block__action-block', styles['page-block__action-block-custom']].join(" ")}>

            <div className={['page-block__item', 'page-block__item-search'].join(' ')}>
              <ButtonComponent
                class_name_of_styles='page-block__button-custom-yellow'
                htmlType="submit"
              >
                Изменить статус
              </ButtonComponent>
            </div>
            <div className={['page-block__item', 'page-block__item-button'].join(' ')}>
              <ButtonComponent class_name_of_styles='page-block__button-custom-white' onClick={() => {
                removeCreateEditFromRoutePage(keys.USERS_MOBILE);
                cancelButtonClicked();
              }}>
                Отменить
              </ButtonComponent>
            </div>
          </div>
        </Form>

      </div>
    );
  }
}

export default Form.create()(EditUserMobile);