import React, {Component} from 'react';
import { inject, observer, } from 'mobx-react';
import InputComponent from '../../components/Input';
import ButtonComponent from '../../components/Button';
import SelectComponent from '../../components/Select';
import CreateQuestion from '../../containers/CreateQuestion';
import CreateAnswerModal from '../../modals/CreateAnswerModal';
import {valueForSelectAnswers} from "../../constants/selectItems";
import styles from './styles/index.module.scss';
import * as keys from '../../keys';
import {removeCreateEditFromRoutePage, setParams} from "../../utils";

const ARRAY_SKIP_BUTTON = [valueForSelectAnswers[3].value, valueForSelectAnswers[4].value];

// ((selectedAnswerType === valueForSelectAnswers[3].value && activeAnswers.length === 0) ||
//   (selectedAnswerType !==valueForSelectAnswers[3].value && activeAnswers.length > 0))


@inject('riskMapIssuesStore')
@observer

class RiskQuestionDescription extends Component {

    render() {
        const {
          activeAnswers,
          activeQuestionName,
          createQuestion,
          deleteQuestion,
          activeQuestion,
          deleteAnswer,
          toggleModalCreateAnswer,
          createQuestionObject,
          selectedAnswerType,
          setSelectedAnswerType,
          showSaveButton,
          isSuperuser,
        } = this.props.riskMapIssuesStore;



        if (!activeQuestionName && !createQuestionObject.name) return (
          isSuperuser && <CreateQuestion />
        );


        return (
            <div className='info-description'>
                <div className={['info-description__title', styles['title-custom']].join(" ")}>
                    Вопросы карты рисков
                </div>

                <div className='info-description__detail'>
                    <div className='info-description__detail-block'>
                        <InputComponent
                            class_name_of_styles={[styles['risk__input'], styles['risk__input-custom']].join(" ")}
                            value={activeQuestionName}
                            disabled={true}
                            delete_icon={isSuperuser}
                            delete_func={() => {
                                deleteQuestion(activeQuestion)
                            }}
                        />
                    </div>
                  {!activeQuestion && <div className='info-description__detail-block'>
                    <SelectComponent
                      styles_select_classname='page-block__select'
                      option_array={valueForSelectAnswers}
                      placeholder={'Выберите тип ответа'}
                      value={selectedAnswerType}
                      onChange={setSelectedAnswerType}
                    />
                  </div>}
                    {
                        activeAnswers.map((ans, i) => (
                            <div key={i} className='info-description__detail-block'>
                                <InputComponent
                                    class_name_of_styles={styles['risk__input']}
                                    value={ans.answer}
                                    disabled
                                    delete_icon={isSuperuser}
                                    delete_func={ ()=> deleteAnswer(ans.id, i)}
                                />
                            </div>
                        ))
                    }
                </div>

              <div className={styles['button-block-save']}>
                {/*если вибрано собтвенний ответ или выбрано остальние пункти и есть  или редактируется список*/}
                {
                  (showSaveButton || ((selectedAnswerType && !activeQuestion &&
                  ( selectedAnswerType && (ARRAY_SKIP_BUTTON.indexOf(selectedAnswerType) > -1) && activeAnswers.length === 0)) ||
                    (selectedAnswerType && (ARRAY_SKIP_BUTTON.indexOf(selectedAnswerType) === -1) && activeAnswers.length > 0))
                  ) && isSuperuser &&
                <ButtonComponent
                    class_name_of_styles={['page-block__button-custom-yellow', styles['save-button']].join(' ')}
                    onClick={()=>{
                      setParams('title','');
                      removeCreateEditFromRoutePage(keys.RISK_QUESTIONS);
                      createQuestion();
                    }}
                  >
                    Сохранить
                  </ButtonComponent>}
                {selectedAnswerType && (ARRAY_SKIP_BUTTON.indexOf(selectedAnswerType) === -1) && isSuperuser &&
                <div className={[styles['button-block'],styles['button-block__margin-left']].join(' ')}>
                  <ButtonComponent class_name_of_styles='page-block__button-custom' onClick={() => {
                    toggleModalCreateAnswer(true)
                  }}>
                    Создать
                  </ButtonComponent>
                </div>
                }
              </div>

              <CreateAnswerModal/>
            </div>

        );
    }
}

export default RiskQuestionDescription;


// (selectedAnswerType && !activeQuestion &&
//   ((selectedAnswerType === valueForSelectAnswers[3].value && activeAnswers.length === 0) ||
//     (selectedAnswerType !==valueForSelectAnswers[3].value && activeAnswers.length > 0))
// )