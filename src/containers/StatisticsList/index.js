import React, {Component} from 'react';
import {Checkbox, } from 'antd';
import SearchInput from '../../components/SearchInput';
import ButtonComponent from '../../components/Button';
import StatisticsBlock from '../../components/StatisticsBlock';
import styles from './styles/index.module.scss';


const obj = [
  {keys: 0,  title: "Настроить вручную",},
];

class StatisticsList extends Component {
    state = {
        activeStatisticsBlock: undefined,
      };
  componentDidMount() {

  }
 
  onClick = (key) => {
    this.setState({ activeStatisticsBlock: key, });
   
  }
  render() {
    const statisticslist  = obj.map((value, index) => {
      return (
              <StatisticsBlock
                title={value.title} 
                keys={value.keys} 
                onClick={this.onClick} 
                activeStatisticsBlock={this.state.activeStatisticsBlock}
                key={value.title+index}
        /> 
      )
    });
    const onChange = (e) => {

    }
    
      return (
            <div className='page-block'>
                 <div className='page-block__action-block'>
                     <div className={['page-block__item','page-block__item-search'].join(' ')}>
                        <SearchInput classNameOfSeach = 'page-block__search'/>
                     </div>
                     <div className={['page-block__item','page-block__item-button'].join(' ')}>
                        <ButtonComponent class_name_of_styles = 'page-block__button-custom '>
                            Создать
                        </ButtonComponent>
                     </div>
                 </div>

                
                <div className='page-block__list'>
                    <div className={styles['checkbox-block']}>
                        <Checkbox onChange={onChange}><span className={styles['checkbox-label']}>Автоматическое получение аналитики</span></Checkbox>
                    </div>
                  { Object.keys(obj).length > 0 &&  statisticslist} 
                    
                  
                </div>
            </div>
      );
    }
}


export default StatisticsList;