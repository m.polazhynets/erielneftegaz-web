import React, {Component} from 'react';
import {Pagination} from 'antd';
import {inject, observer} from "mobx-react";
import SearchInput from '../../components/SearchInput';
import UserBlock from '../../components/UserBlock';
import classNames from 'classnames';
import styles from './styles/index.module.scss';
import history from "../../modules/history";
import {getIdFromCreateRoute, isEditPage, removeCreateEditFromRoutePage, } from "../../utils";
import * as keys from "../../keys";

@inject('usersStore')
@observer

class UsersList extends Component {
    state = {
        activeUserBlock: undefined,
        width: window.innerWidth,
        searchValue: '',
      };
  componentDidMount() {
    const {getAllUsers, setSearchField, selectActiveUser} = this.props.usersStore;
    const {s, page} = history.location.query;
    const editPage = isEditPage(keys.USERS_AD);

    if(s){
      let pageNumber = 1;
      if(page) pageNumber = page;

      setSearchField(s,pageNumber)
        .then(()=>{
          if(editPage) {
            const id = getIdFromCreateRoute(keys.USERS_AD);
            selectActiveUser({id});
          }
        });


      this.setState({
        searchValue: s,
      });
    } else {
      let pageNumber = 1;
      if(page) pageNumber = page;
      getAllUsers(pageNumber)
        .then(()=>{
          if(editPage) {
            const id = getIdFromCreateRoute(keys.USERS_AD);
            selectActiveUser({id});
          }
        });
    }



    window.addEventListener("resize", this.updateDimensions);
  }
 
  onClick = (id) => {
    const {selectActiveUser} = this.props.usersStore;
    selectActiveUser({id});
  }
  updateDimensions = () => {
    this.setState({
      width: window.innerWidth
    });
  }

  handleChange = (value) => {
    const {s} = history.location.query;
    if (s) {
      this.props.usersStore.setSearchField(s, value);
    } else {
      this.props.usersStore.getAllUsers(value);
    }
  }

  renderUsersList = () => {
    const {usersList, selectedUserId} = this.props.usersStore;
    return usersList.map( userItem =>{
      const {id, fio, position} = userItem;

      let positionTitle = (position && position.name) ? position.name : '';
      const title  = `${fio} ${positionTitle}`;
      return (<UserBlock
          title={title}
          onClick={this.onClick}
          isActiveUserBlock={id === selectedUserId}
          key={id}
          id={id}
        />
      );
    })
  }

  searchFunc = value =>{
    const { setSearchField, clearEditMode } = this.props.usersStore;


    setSearchField(value, 1);
    clearEditMode();
    removeCreateEditFromRoutePage(keys.USERS_AD);
  }

  render() {
    const { page, totalPages, usersList } = this.props.usersStore;
    const {searchValue} = this.state;

      return (
            <div className='page-block'>
              <SearchInput
                onSearch={this.searchFunc}
                value={searchValue}
                onChange={({target})=>{this.setState({searchValue: target.value})}}
                classNameOfSeach = 'page-block__search'
              />
                <div className={classNames('page-block__list', styles['page-block__list'])}>
                  {this.renderUsersList()}

                  {usersList.length > 0 && (totalPages > 1) &&<Pagination
                   current={page}
                   defaultPageSize={1}
                   onChange={this.handleChange}
                   total={totalPages}
                 />}
                    
                  
                </div>
            </div>
      );
    }
    componentWillUnmount() {
      window.removeEventListener("resize", this.updateDimensions);
    }
}


export default UsersList;