import React, {Component} from 'react';
import {Form, Checkbox,} from 'antd';
import {inject, observer} from "mobx-react";
import {checkPermission} from "../../utils";
import InputComponent from '../../components/Input/';
import ButtonComponent from '../../components/Button';
import styles from './styles/index.module.scss';
import {Element} from "react-scroll";
import MultiSelect from "../../components/MultiSelect";
import ProbabilityTable from "../ProbabilityTable";
import * as keys from "../../keys";
import {Redirect} from "react-router-dom";


@inject('riskNotificationStore')
@observer

class CreateRiskNotification extends Component {

  defineRuleForMultiSelectPositions = (rule, value, callback) => {
    const {selectedPositions} = this.props.riskNotificationStore;

    if (selectedPositions.length > 0) {
      callback();
      return;
    }
    callback('Это поле обязательное');
  };

  defineRuleForMultiSelectGroupsUsers = (rule, value, callback) => {
    const {selectedGroupsUsers} = this.props.riskNotificationStore;

    if (selectedGroupsUsers.length > 0) {
      callback();
      return;
    }
    callback('Это поле обязательное');
  };

  searchPosition = (value = '') => this.props.riskNotificationStore.searchPosition(value);

  searchGroupsUsers = (value = '') => this.props.riskNotificationStore.searchGroupsUsers(value);

  handleChangeSelect = (list) => this.props.riskNotificationStore.setSelectedPositions(list);

  handleChangeSelectGroupsUsers = (list) => this.props.riskNotificationStore.setSelectedGroupUsers(list);


  handleSubmit = e => {
    e.preventDefault();

    this.props.form.validateFields(({force: true}),(err, values) => {

      const {send_by_sms, send_by_email} = values;

      if(!(send_by_sms || send_by_email)) {
        this.props.form.setFields({
          send_by_sms: {
            value: send_by_sms,
            errors: [new Error('Как минимум должен быть выбран один способ уведомлений')],
          },
          send_by_email: {
            value: send_by_email,
            errors: [new Error('Как минимум должен быть выбран один способ уведомлений')],
          },
        });
      } else {
        this.props.form.setFields({
          send_by_sms: {
            value: send_by_sms,
          },
          send_by_email: {
            value: send_by_email,
          },
        });
        if (!err) {
          this.props.riskNotificationStore.createRiskNotification(values);
        }
      }
    });
  };

  render() {
    const {
      positionList,
      fetchingPosition,
      selectedPositions,
      groupsUsersList,
      selectedGroupsUsers,
      fetchingGroupsUsers,
      activeX,
      activeY,
      setActiveItem,
      arrayOfResponsible,
      cancelButtonClicked,
      isSuperuser,
    } = this.props.riskNotificationStore;
    const {
      getFieldDecorator,
    } = this.props.form;


    const check = checkPermission(keys.RISK_NOTIFICATIONS, isSuperuser);

    if(!check) return <Redirect to={keys.ACCESS_PAGE} />

    return (


      <div className='info-description'>
        <div className='info-description__title'>
          <Element name="scroll-to-subdivision-create">
            Создание оповещений по рискам
          </Element>
        </div>
        <Form onSubmit={this.handleSubmit}>
          <div className='info-description__detail-block'>
            <Form.Item
              className='info-description__detail-block'
              label='Название'
            >
              {getFieldDecorator('name', {
                rules: [{
                  required: true,
                  message: 'Это поле обязательное'
                }]
              })(
                <InputComponent
                />
              )}
            </Form.Item>

          </div>

          <div className='info-description__detail-block'>
            <Form.Item
              className='info-description__detail-block'
              label='Должности'
            >
              {getFieldDecorator('position', {
                rules: [
                  {
                    validator: this.defineRuleForMultiSelectPositions,
                  }

                ]
              })(
                <MultiSelect
                  requestFunc={this.searchPosition}
                  options={positionList.slice()}
                  fetching={fetchingPosition}
                  handleChange={this.handleChangeSelect}
                  valueArray={selectedPositions.slice()}
                />
              )}
            </Form.Item>
          </div>
          <div className='info-description__detail-block'>
            <Form.Item
              className='info-description__detail-block'
              label='Группы пользователей'
            >
              {getFieldDecorator('groups', {
                rules: [
                  {
                    validator: this.defineRuleForMultiSelectGroupsUsers,
                  }

                ]
              })(
                <MultiSelect
                  requestFunc={this.searchGroupsUsers}
                  options={groupsUsersList.slice()}
                  fetching={fetchingGroupsUsers}
                  handleChange={this.handleChangeSelectGroupsUsers}
                  valueArray={selectedGroupsUsers.slice()}
                />
              )}
            </Form.Item>
          </div>

          <div className='info-description__detail-block'>
            <Form.Item
              className='info-description__detail-block'
              label='Послать оповещения на телефон'
            >
              {getFieldDecorator('send_by_sms', {
                rules: []
              })(
                <Checkbox className={styles['checkbox']}/>
              )}
            </Form.Item>
          </div>

          <div className='info-description__detail-block'>
            <Form.Item
              className='info-description__detail-block'
              label='Отправлять оповещения на почту'
            >
              {getFieldDecorator('send_by_email', {
                rules: []
              })(
                <Checkbox/>
              )}
            </Form.Item>
          </div>
          <ProbabilityTable
            isShowMode={false}
            onClickItem={(x,y)=>setActiveItem(x,y)}
            probability={activeX}
            consequences={activeY}
            selectedItems={arrayOfResponsible.slice()}
          />
          <div className={['page-block__action-block', styles['page-block__action-block-custom']].join(" ")}>
            <div className={['page-block__item', 'page-block__item-search'].join(' ')}>
              <ButtonComponent
                class_name_of_styles='page-block__button-custom-yellow'
                htmlType="submit"
              >
                Создать оповещение
              </ButtonComponent>
            </div>
            <div className={['page-block__item', 'page-block__item-button'].join(' ')}>
              <ButtonComponent class_name_of_styles='page-block__button-custom-white' onClick={() => {
                cancelButtonClicked();
              }}>
                Отменить
              </ButtonComponent>
            </div>
          </div>
        </Form>

      </div>


    );
  }
}


export default Form.create()(CreateRiskNotification);