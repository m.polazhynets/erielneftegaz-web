import React, { Component } from 'react';
import { Input } from 'antd';
import classNames from 'classnames';
import { } from '../../styles/images.js';
import styles from './styles/index.module.scss';
import InputComponent from '../../components/Input';
import RiskLevelBlock from '../../components/RiskLevelBlock';
import {inject, observer} from "mobx-react";

const obj_start = [
     {keys: 0,  title: "Низкая", activeStyle: 'active-start'},
     {keys: 1,  title: "Средняя", activeStyle: 'active-start'},
     {keys: 2,  title: "Высокая", activeStyle: 'active-start'},
     {keys: 3,  title: "Очень высокая", activeStyle: 'active-start'},
]

const obj_end = [
    {keys: 0,  title: "Низкая", activeStyle: 'active-end'},
    {keys: 1,  title: "Средняя", activeStyle: 'active-end'},
    {keys: 2,  title: "Высокая", activeStyle: 'active-end'},
    {keys: 3,  title: "Очень высокая", activeStyle: 'active-end'},
]

@inject('riskNotificationStore')
@observer

class RiskNotificationsDescription extends Component {
    state = {
        activeStartRiskBlock: undefined,
        activeEndRiskBlock: undefined,
    };

    onClickStart = (key) => {
        this.setState({ activeStartRiskBlock: key, });
       
    }
    onClickEnd = (key) => {
        this.setState({ activeEndRiskBlock: key, });
       
    }
    render() {
        const risklevelsstart  = obj_start.map((value, index) => {
            return (
                    <RiskLevelBlock
                      title={value.title} 
                      keys={value.keys} 
                      onClick={this.onClickStart} 
                      activeRiskBlock={this.state.activeStartRiskBlock}
                      active_style={value.activeStyle}
                      key={value.title+index}
              /> 
            )
          });

          const risklevelsend  = obj_end.map((value, index) => {
            return (
                    <RiskLevelBlock
                      title={value.title} 
                      keys={value.keys} 
                      onClick={this.onClickEnd} 
                      activeRiskBlock={this.state.activeEndRiskBlock}
                      active_style={value.activeStyle}
                      key={value.title+index}
              /> 
            )
          });

        return (

            <div className={['info-description', styles['risk']].join(' ')}>
                <div className={['info-description__title', styles['risk__title']].join(' ')}>
                        Настройка оповещений
                     </div>

                <div className='info-description__detail'>
                    <div className='info-description__detail-block'>
                        <InputComponent
                            class_name_of_styles={styles['risk__input']}
                            value="Группа уведомлений региональных директоров"
                            disabled={true} 
                           
                        />
                    </div>
                </div>

                <div className={styles['risk__settings']}>
                    <div className={styles['risk__start']}>
                        <div className={styles['risk__note']}>От</div>
                        { Object.keys(obj_start).length > 0 && risklevelsstart}
                      
                    </div>
                    <div className={styles['risk__end']}>
                        <div className={styles['risk__note']}>До</div>
                        { Object.keys(obj_end).length > 0 &&risklevelsend}
                        
                        
                    </div>

                </div>




            </div>

        );
    }
}

export default RiskNotificationsDescription;