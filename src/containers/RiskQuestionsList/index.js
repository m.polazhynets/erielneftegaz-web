import React, {Component} from 'react';
import SearchInput from '../../components/SearchInput';
import RiskBlock from '../../components/RiskBlock';
import {inject, observer} from "mobx-react";
import * as keys from '../../keys';
import history from "../../modules/history";
import {
  getIdFromCreateRoute,
  isCreatePage,
  isEditPage,
  removeCreateEditFromRoutePage,
  setEditPage,
  setParams
} from "../../utils";
import {Pagination} from "antd";
@inject('riskMapIssuesStore')
@observer

class RiskQuestionsList extends Component {

  constructor() {
    super();
    this.state = {
      value: '',
    }
  }

  componentDidMount(): void {
    const {getQuestion, changeSearch, setActiveQuestion} = this.props.riskMapIssuesStore;
    const {s, title} = history.location.query;

    const {createQuestionObjectLocal} = this.props.riskMapIssuesStore;

    const editPage = isEditPage(keys.RISK_QUESTIONS);
    const createPage = isCreatePage(keys.RISK_QUESTIONS);

    if(createPage ) {
      if(title)  createQuestionObjectLocal(title);
    }


    if (s) {
      // если есть поиск
      this.setState({value: s})
      changeSearch(s)
        .then(() => {
          if(editPage){
            const id = getIdFromCreateRoute(keys.RISK_QUESTIONS);
            if(id){
              //eсли имя выбрано
              setActiveQuestion(id)
            }
          }
        });
    } else {
      getQuestion()
        .then(()=>{
          if(editPage){
            const id = getIdFromCreateRoute(keys.RISK_QUESTIONS);
            if(id){
              setActiveQuestion(id)
            }
          }
        });
      // if (id) {
      //   //если нет имени но есть вибранный обэкт
      //   getQuestion()
      //     .then(() => {
      //       setActiveQuestion(id)
      //     })
      // } else {
      //   getQuestion();
      // }
    }



  }

  handleChange = (value) => {
      const {s} = history.location.query;
      if(s) {
        this.props.riskMapIssuesStore.changeSearch(s, value);
      } else {
        this.props.riskMapIssuesStore.getQuestion(value);
      }
  }

  onClick = (key) => {
    setEditPage(keys.RISK_QUESTIONS, key);
    setParams('title','');
    this.props.riskMapIssuesStore.setActiveQuestion(key);
  }

  render() {
    const {value} = this.state;
    const {riskMapIssuesStore} = this.props;
    const {questions, activeQuestion, changeSearch, page, totalPages, resetActiveQuestion} = riskMapIssuesStore;


    return (
      <div className='page-block'>
        <SearchInput value={value} onChange={({target}) => this.setState({value: target.value})} onSearch={(value)=>{
          changeSearch(value);
          resetActiveQuestion();
          removeCreateEditFromRoutePage(keys.RISK_QUESTIONS);

          setParams('title','');
        }}
        classNameOfSeach='page-block__search'/>

        <div className='page-block__list'>

          {
            questions.map(question => (
              <RiskBlock
                title={question.name}
                keys={question.id}
                onClick={this.onClick}
                activeRisk={activeQuestion}
                key={question.id}
              />
            ))
          }

          {questions && questions.slice().length > 0 && (totalPages > 1) && <Pagination
            current={page}
            defaultPageSize={1}
            onChange={this.handleChange}
            total={totalPages}
          />}
        </div>
      </div>
    );
  }
}


export default RiskQuestionsList;