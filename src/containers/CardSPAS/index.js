import React, { Component } from "react";
import ButtonComponent from "../../components/Button";
import InputComponent from "../../components/Input";
import ProbabilityTable from "../../containers/ProbabilityTable";
import StatusApplication from "../../components/StatusApplication";
import styles from "./styles/index.module.scss";
import PhotoVideoBlock from "../PhotoVideoBlock";
import { inject, observer } from "mobx-react";
import SpasCardsTasks from "../SpasCardsTasks";

@inject("messagesStore")
@observer
class CardSPAS extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  renderAnswers = (answers) => {
    const answersListRender = [];
    for (let i = 1; i < answers.length; i++) {
      answersListRender.push(
        <div key={answers[i].id} className={styles["card-spas__input-block"]}>
          <InputComponent
            class_name_of_styles={styles["card-spas__input"]}
            value={answers[i].answer_text}
            disabled={true}
          />
        </div>
      );
    }

    return answersListRender;
  };

  renderQuestions = (questions) => {
    if (!questions || (questions && questions.length === 0)) return;

    return questions.map((item) => {
      return (
        <React.Fragment key={item.id}>
          <div className={styles["card-spas__input-block"]}>
            <InputComponent
              class_name_of_styles={styles["card-spas__input"]}
              value={item.answers[0].answer_text}
              disabled={true}
              label={item.question}
            />
          </div>
          {this.renderAnswers(item.answers)}
        </React.Fragment>
      );
    });
  };

  setAPerformer = () => {
    const { setUserForSpasCard } = this.props.messagesStore;

    setUserForSpasCard();
  };

  onSelectUser = (value, { key }) => {
    const { setSelectedUserId } = this.props.messagesStore;
    setSelectedUserId(key);
  };

  searchUser = (value) => this.props.messagesStore.getListUsersSearch(value);

  changeStatusMessage = (status) =>
    this.props.messagesStore.setStatusForSpasCard(status);

  render() {
    const {
      spasCardInfo,
      spasCardsStatusLogs,
      spasCardsUsers,
      activeTaskId,
      spasCardsTasks,
      setActiveTaskId,
      setCreatingTask,
      statusMessage,
      setMessageStatus,
    } = this.props.messagesStore;
    const {
      date,
      time,
      facility,
      risk,
      answers,
      files,
      id,
      responsible_user,
      current_user_id,
    } = spasCardInfo;

    const isMyCardSPAS = responsible_user
      ? responsible_user.id === current_user_id
      : null;

    if (!spasCardInfo || !spasCardInfo.date) return null;

    return (
      <div className={["info-description", styles["card-spas"]].join(" ")}>
        <div className={styles["card-spas__title"]}>Карта СПАС</div>
        <StatusApplication
          isMyCardSPAS={isMyCardSPAS}
          setAPerformerClick={this.setAPerformer}
          statusList={spasCardsStatusLogs.slice()}
          classNameButton={styles["card-spas__button"]}
          onSelect={this.onSelectUser}
          dataSource={spasCardsUsers.slice()}
          onSearch={this.searchUser}
          incomingApplicationTitle={id.toString()}
          onClickButtonChangeStatusMessage={this.changeStatusMessage}
          statusMessage={statusMessage}
          onChangeStatusMessage={setMessageStatus}
          showSetUserBlock={isMyCardSPAS && spasCardsTasks.slice().length === 0}
        />

        {responsible_user && responsible_user.fio && (
          <div className={styles["card-spas__input-block"]}>
            <InputComponent
              class_name_of_styles={styles["card-spas__input"]}
              value={responsible_user.fio}
              disabled={true}
              label="Текущий ответственный"
            />
          </div>
        )}
        {isMyCardSPAS && (
          <ButtonComponent
            class_name_of_styles={styles["card-spas__button"]}
            onClick={setCreatingTask}
          >
            Создать задачу
          </ButtonComponent>
        )}
        <SpasCardsTasks
          onClickItem={setActiveTaskId}
          activeItem={activeTaskId}
          spasCardsTasksList={spasCardsTasks.slice()}
        />
        {/*{activeTaskId && <ReportRisk riskItem={activeRiskObject} facilityId={facility.id} />}*/}
        <div className={styles["card-spas__answers-title"]}>
          Ответы карты СПАС
        </div>
        <div className={styles["card-spas__input-standart-block"]}>
          <InputComponent
            class_name_of_styles={styles["card-spas__input"]}
            value={date}
            disabled={true}
            label="Дата"
          />
          <InputComponent
            class_name_of_styles={styles["card-spas__input"]}
            value={time}
            disabled={true}
            label="Время"
          />
        </div>
        {facility && facility.name && (
          <div className={styles["card-spas__input-block"]}>
            <InputComponent
              class_name_of_styles={styles["card-spas__input"]}
              value={facility.name}
              disabled={true}
              label="Буровая база"
            />
          </div>
        )}

        {this.renderQuestions(answers)}
        <ProbabilityTable
          consequences={risk.y}
          probability={risk.x}
          number={risk.number}
        />
        <PhotoVideoBlock data={files.slice()} />
      </div>
    );
  }
}

export default CardSPAS;
