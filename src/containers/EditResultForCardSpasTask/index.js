import React, {Component} from 'react';
import {Element, default as Scroll} from "react-scroll";
import {Button, Form, Icon, Upload} from "antd";
import InputComponent from "../../components/Input";
import ButtonComponent from "../../components/Button";
import styles from './styles/index.module.scss';
import {inject, observer} from "mobx-react";
import SelectComponent from "../../components/Select";

const scroll = Scroll.animateScroll;

const props = {
  name: 'file',
  action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
  headers: {
    authorization: 'authorization-text',
  },
};

const eliminatedArray = [
  {label: 'Да',value: "true" },
  {label: 'Нет',value: "false" },
]

@inject('messagesStore')
@observer

class EditResultForCardSpasTask extends Component {

  state = {
    width: window.innerWidth,
    eliminatedStatus: null,
    fileList: [],
  };

  handleSubmit = e => {


    e.preventDefault();
    this.props.form.validateFields({force: true},(err, values) => {

      if (!err) {
        // const {text, date, time} = values;

        const {comment} = values;
        const {eliminatedStatus} = this.state;

        const eliminated = eliminatedStatus === "true" ? true : false;

        let files = null;

        if(values.files && values.files.fileList.length > 0) {
          files = [];
          values.files.fileList.map(item => {
           files.push(item.originFileObj);
           return item;
          });
        }

        let dataForForm = {
          comment,
          eliminated
        };

        if(files) dataForForm.files = files;

        this.props.messagesStore.editResultOfTask(dataForForm);

        if(this.state.width<=767) {
          scroll.scrollToTop()
        }

      }
    });

  };


  onChangeSelect = (eliminatedStatus) => this.setState({eliminatedStatus});

  onChangeFile = (info) => {
    // if (info.file.status !== 'uploading') {
    //
    // }
    // if (info.file.status === 'done') {
    // } else if (info.file.status === 'error') {
    // }
  }

  componentDidMount() {
    const {activeRiskObject} = this.props.messagesStore;
    const eliminated = activeRiskObject.eliminated;

    if(eliminated) {
      this.setState({eliminatedStatus: eliminatedArray[0].value})
    } else {
      this.setState({eliminatedStatus: eliminatedArray[1].value})
    }
  }

  render() {
    const { getFieldDecorator} = this.props.form;
    const {cancelButtonClicked, activeRiskObject, } = this.props.messagesStore;
    const {eliminatedStatus} = this.state;

    return (
      <div className={['info-description'].join(' ')}>
        <div className='info-description__title'>
          <Element name="scroll-to-subdivision-create" >
            Отчет по устранению риска
          </Element>
        </div>
        <Form onSubmit={this.handleSubmit}>
          <div className='info-description__detail'>
            <Form.Item
              className='info-description__detail-block'
              label='Коментарий:'
            >
              {getFieldDecorator('comment', {
                initialValue: activeRiskObject.comment,
                rules: [
                  {
                    required: true,
                    message: 'Это поле обязательное'
                  }
                ]
              })(
                <InputComponent
                  class_name_of_styles={styles['users__input']}
                />
              )}
            </Form.Item>
            <div className={styles['select-block']}>
            <div className={styles['select-label']}>Риск устранен?</div>
                <SelectComponent
                  value={eliminatedStatus}
                  onChange = {this.onChangeSelect}
                  styles_select_classname='page-block__select'
                  option_array={eliminatedArray}
                />
            </div>

            <Form.Item
              className='info-description__detail-block'
              label='Выберите изображения'
            >
              {getFieldDecorator('files', {
                rules: [
                  // {
                    // required: false,
                    // message: 'Это  обязательное'
                  // },
                ]
              })(
                <Upload
                  {...props}
                  onChange={this.onChangeFile}
                  multiple
                  accept="image/x-png,image/gif,image/jpeg"
                >
                  <Button>
                    <Icon type="upload" /> Добавить изображение
                  </Button>
                </Upload>
              )}
            </Form.Item>
          </div>
          <div className={styles['form-item__info']}>Если вы загрузите новые изображения, старые будут удалены</div>


          <div className={['page-block__action-block', styles['page-block__action-block-custom']].join(" ")}>
            <div className={['page-block__item', 'page-block__item-search'].join(' ')}>
              <ButtonComponent
                class_name_of_styles='page-block__button-custom-yellow'
                htmlType="submit"
              >
                Изменить
              </ButtonComponent>
            </div>
            <div className={['page-block__item', 'page-block__item-button'].join(' ')}>
              <ButtonComponent
                class_name_of_styles={'page-block__button-custom-white'}
                onClick={
                  () => {
                    cancelButtonClicked()
                    if (this.state.width <= 767) {
                      scroll.scrollToTop();
                    }
                  }
                }
              >
                Отменить
              </ButtonComponent>
            </div>
          </div>
        </Form>


      </div>
     );
  }
}

export default Form.create()(EditResultForCardSpasTask);