import React, {Component} from 'react';
import { Pagination, } from 'antd';
import {Redirect} from 'react-router-dom';
import {  animateScroll as scroll, scroller } from 'react-scroll';
import {inject, observer} from "mobx-react";
import SearchInput from '../../components/SearchInput';
import ButtonComponent from '../../components/Button';
import UsersGroupsBlock from '../../components/UsersGroupsBlock';
import ErrorModal from "../../modals/ErrorModal";
import history from "../../modules/history";
import * as routeKeys from '../../keys';
import {
  checkPermission,
  getIdFromCreateRoute,
  isEditPage,
  removeCreateEditFromRoutePage, setParams
} from "../../utils";


@inject('usersGroupsStore')
@observer

class UsersGroupsList extends Component {
  
    constructor() {
      super();
      this.state = {
        width: window.innerWidth,
        searchValue: '',
      };
    }

    componentDidMount() {
        const { getAllUsersGroups,  setSearchField, setActiveUsersGroups} = this.props.usersGroupsStore;

        const editPage = isEditPage(routeKeys.USERS_GROUPS);

        const {s, page, id} = history.location.query;

        if(s){
          let pageNumber = 1;
          if(page) pageNumber = page;
          setSearchField(s,pageNumber)
            .then(()=>{
              if(Number(id) >=0) setActiveUsersGroups(id);
              this.setEditPageFunc(editPage);
            })
          this.setState({searchValue: s});
        } else {
          let pageNumber = 1;
          if(page) pageNumber = page;
          getAllUsersGroups(pageNumber)
            .then(()=>{
              if(Number(id) >=0) setActiveUsersGroups(id);
              this.setEditPageFunc(editPage);
            })
        }
        window.addEventListener("resize", this.updateDimensions);
    }

    setEditPageFunc = (editPage) => {
      if(editPage){
        const id = getIdFromCreateRoute(routeKeys.USERS_GROUPS);
        if(id){
          this.props.usersGroupsStore.editUsersGroups(id);
        }
      }
    }
  
  updateDimensions = () => {
    this.setState({
      width: window.innerWidth
    });
  }
  onClick = (key, id) => {
    removeCreateEditFromRoutePage(routeKeys.USERS_GROUPS);
    setParams('id',id);
    this.props.usersGroupsStore.setActiveUsersGroups(key);
  }
  
  clearSearchText = () => {
    const {s} = history.location.query;
    if(s){
      this.props.usersGroupsStore.setSearchField('');
      this.setState({searchValue: ''});
    }
  }
  handleChange = value => {
    const {s} = history.location.query;
    if(s) {
      this.props.usersGroupsStore.setSearchField(s, value);
    } else {
      this.props.usersGroupsStore.getAllUsersGroups(value);
    }


  };


  render() {
    const {
          usersGroupsList,
          activeUsersGroups,
          showErrorModal,
          errorText,
          setShowModalError,
          creatingUsersGroups,
          setSearchField,
          page,
          totalPages,
          isSuperuser,
          createUsersGroups,
         } = this.props.usersGroupsStore;


     
    const renderSubBlock = usersGroupsList.map((value, index) => {
      return (
              <UsersGroupsBlock
                name={value.name} 
                keys={value.id}
                showEditBlock={isSuperuser}
                id={value.id} 
                onClick={this.onClick}
                activeUsersGroups={activeUsersGroups}
                key={index}
        /> 
      )
    });

    const check = checkPermission(routeKeys.USERS_GROUPS, isSuperuser);

    if(!check) return <Redirect to={routeKeys.ACCESS_PAGE} />
    

      return (
            <div className='page-block'>
                 <div className='page-block__action-block'>
                     <div className={['page-block__item','page-block__item-search'].join(' ')}>
                        <SearchInput 
                          onSearch={(value)=>{setSearchField(value)}}
                          value={this.state.searchValue} 
                          onChange={({target})=>{this.setState({searchValue: target.value})}} 
                          classNameOfSeach = 'page-block__search'
                        />
                     </div>
                   {isSuperuser && <div className={['page-block__item','page-block__item-button'].join(' ')}>
                        <ButtonComponent 
                            class_name_of_styles = 'page-block__button'
                            onClick = {()=>{
                              setParams('id','');
                              this.clearSearchText();
                              createUsersGroups();
                              if(this.state.width<=767 && !creatingUsersGroups  ) { //
                                scroll.scrollToBottom()
                              }
                              else if(this.state.width<=767 && creatingUsersGroups ) { //
                                 scroller.scrollTo('scroll-to-subdivision-create', {
                                  duration: 800,
                                  delay: 0,
                                  smooth: 'easeInOutQuart'
                                })
                             }
                            }}
                        >
                            Создать
                        </ButtonComponent>
                     </div>}
                    
                 </div>

                <div className='page-block__list'>
                  
                    {renderSubBlock}
                  {usersGroupsList && usersGroupsList.length > 0 && (totalPages > 1) &&
                    <Pagination
                      current={page}
                      defaultPageSize={1}
                      onChange={this.handleChange}
                      total={totalPages}
                    />}
                   
              
                 
                  
                </div>
                <ErrorModal visibleModalError={showErrorModal} toggleModalError={()=>setShowModalError(false)} errorText={errorText}/>
            </div>
      );
    }
    componentWillUnmount() {
      window.removeEventListener("resize", this.updateDimensions);
    }
}


export default UsersGroupsList;