import React, { Component } from 'react';
import { Form, } from 'antd';
import * as Scroll from 'react-scroll';
import { inject, observer } from "mobx-react";
import { Element, } from 'react-scroll'
import styles from './styles/index.module.scss';
import InputComponent from '../../components/Input/';
import ButtonComponent from '../../components/Button';
import AutoComplete from "../../components/AutoComplete";

const scroll = Scroll.animateScroll;

@inject('createCategoryRiskStore')
@observer
class EditCategoryRiskForm extends Component {
  state = {
    width: window.innerWidth,
    position: '',
  };
  componentDidMount() {
    window.addEventListener("resize", this.updateDimensions);
    // this.props.createCategoryRiskStore.getRiskItemCategory({positionId:1, riskId: 8, categoryId:1});
  }
  handleSubmit = e => {

    e.preventDefault();
    this.props.form.validateFields((err, values) => {

      if (!err) {
          const {selectedPosition} = this.props.createCategoryRiskStore;

          if(!selectedPosition){
            this.props.form.setFields({
              position: {
                value: values.position,
                errors: [new Error('Это поле обязательное')],
              },
            });
            return;
          }

        this.props.createCategoryRiskStore.editRiskCategory(values);
        if(this.state.width<=767) {
          scroll.scrollToTop()
        }

      }
    });

  };

  searchPosition = value => {
    this.props.createCategoryRiskStore.getListOfPositionSearch(value);
    this.props.createCategoryRiskStore.setSelectedPosition(null);
  }

  updateDimensions = () => {
    this.setState({
      width: window.innerWidth
    });
  }
  render() {
    const { getFieldDecorator} = this.props.form;

    const {positionList, setSelectedPosition, selectedRiskItem, cancelButtonClicked, deleteCategoryItem} = this.props.createCategoryRiskStore;
    const {categoryName, positionName} = selectedRiskItem;


    return (

      <div className='info-description'>
        <div className='info-description__title'>
          <Element name="scroll-to-subdivision-create" >
            Изменение категорий рисков
          </Element>
        </div>
        <Form onSubmit={this.handleSubmit}>
          <div className='info-description__detail'>



            <Form.Item
              className='info-description__detail-block'
              label='Название:'
            >
              {getFieldDecorator('name', {
                initialValue: categoryName,
                rules: [
                  {
                    required: true,
                    message: 'Это поле обязательное'
                  }
                ]
              })(
                <InputComponent
                  class_name_of_styles={styles['users__input']}
                  // disabled={true}
                />
              )}
            </Form.Item>
            <Form.Item
              className='info-description__detail-block'
              label='Должность:'

            >
              {getFieldDecorator('position', {
                initialValue: positionName,
                rules: [
                  {
                    required: true,
                    message: 'Это поле обязательное'
                  }
                ]
              })(
                <AutoComplete
                  onSearch={this.searchPosition}
                  dataSource={positionList.slice()}
                  onSelect={
                    (position,{key})=>{
                      this.setState({position})
                      setSelectedPosition(key);
                    }}
                />
              )}
            </Form.Item>

          </div>

          <div className={['page-block__action-block', styles['page-block__action-block-custom']].join(" ")}>
            <div className={['page-block__item', 'page-block__item-search'].join(' ')}>
              <ButtonComponent
                class_name_of_styles='page-block__button-custom-red'
                onClick={()=>deleteCategoryItem()}
              >
                Удалить
              </ButtonComponent>
            </div>
            <div className={['page-block__item', 'page-block__item-search'].join(' ')}>
              <ButtonComponent
                class_name_of_styles='page-block__button-custom-yellow'
                htmlType="submit"
                //disabled={hasErrors(getFieldsError()) || this.state.editorHtml.replace(/<(.|\n)*?>/g, '').trim().length === 0}
              >
                Сохранить
              </ButtonComponent>
            </div>
            <div className={['page-block__item', 'page-block__item-button'].join(' ')}>
              <ButtonComponent
                class_name_of_styles='page-block__button-custom-white'
                onClick={
                  () => {
                    cancelButtonClicked();
                    if (this.state.width <= 767) {
                      scroll.scrollToTop();
                    }
                  }
                }
              >
                Отменить
              </ButtonComponent>
            </div>
          </div>
        </Form>


      </div>

    );
  }
}


export default Form.create()(EditCategoryRiskForm);