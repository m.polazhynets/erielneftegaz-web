import React, {Component} from 'react';
import {Form} from 'antd';
import {inject, observer,} from 'mobx-react';
import styles from './styles/index.module.scss';
import {hasErrors, removeCreateEditFromRoutePage, setCreatePage, setParams,} from "../../utils";
import InputComponent from '../../components/Input';
import ButtonComponent from '../../components/Button';
import * as keys from '../../keys';

@inject('riskMapIssuesStore')
@observer


class CreateQuestion extends Component {


  handleSubmit = e => {

    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const { createQuestionObjectLocal } = this.props.riskMapIssuesStore;
        const {question} = values;
        setCreatePage(keys.RISK_QUESTIONS);
        setParams('title',question);
        createQuestionObjectLocal(question);
      }
    });
  };

  render() {
    const {
      getFieldDecorator,
      getFieldsError,
    } = this.props.form;

    return (

      <div className='info-description'>
        <Form onSubmit={this.handleSubmit}>
          <div className={['info-description__title', styles['title-custom']].join(" ")}>
            Создать вопрос
          </div>

          <div className='info-description__detail'>

            <div className='info-description__detail-block'>


              <Form.Item
                key={"question"}
                className='info-description__detail-block'
              >
                {getFieldDecorator('question', {
                  rules: [{
                    required: true,
                    message: 'Это поле обязательное'
                  }]
                })(
                  <InputComponent
                    placeholder="Введите вопрос"
                    class_name_of_styles={styles['risk__input']}
                  />
                )}
              </Form.Item>

            </div>

          </div>
          <div className={['page-block__action-block', styles['page-block__action-block-custom']].join(" ")}>
            <div className={['page-block__item', 'page-block__item-search'].join(' ')}>
              <ButtonComponent class_name_of_styles='page-block__button-custom-yellow' htmlType="submit"
                               disabled={hasErrors(getFieldsError())}>
                Сохранить
              </ButtonComponent>
            </div>
            <div className={['page-block__item', 'page-block__item-button'].join(' ')}>
              <ButtonComponent onClick={()=>{
                setParams('title','');
                removeCreateEditFromRoutePage(keys.RISK_QUESTIONS);
              }}
              class_name_of_styles='page-block__button-custom-white'>
                Отменить
              </ButtonComponent>
            </div>
          </div>
        </Form>
      </div>

    );
  }
}

export default Form.create()(CreateQuestion);