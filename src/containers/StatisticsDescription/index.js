import React, {Component} from 'react';
import { Form } from 'antd';

import SelectComponent from "../../components/Select";


class StatisticsDescription extends Component {
        render() {
            const { getFieldDecorator } = this.props.form;
    
            return (

           
                <div className='info-description'>
                    <div className='info-description__title'>
                        Статистика и аналитика
                     </div>
                    
                    <div className='info-description__detail'>
                        <Form onSubmit={(e)=>{
                            e.preventDefault()
                        }}>
                            <Form.Item 
                                key={"quality_violations"} 
                                className='info-description__detail-block'  
                                label='Варианты отчетов:'  
                            >
                                {getFieldDecorator('quality_violations', {
                                    rules: [{
                                        required: true,
                                        message: 'Это поле обязательное'
                                    }]
                                })(
                                    <SelectComponent 
                                        styles_select_classname='page-block__select'
                                        option_array={[
                                            {label: '1',text: 'значение 1' },
                                            {label: '2',text: 'значение 2' },
                                            {label: '3',text: 'значение 3' },
                                        ]} 
                                        placeholder={"Количество нарушений"} 
                                    />
                                )}
                              </Form.Item>
                              <Form.Item 
                                key={"type_violations"} 
                                className='info-description__detail-block'  
                                >
                                      {getFieldDecorator('type_violations', {
                                    rules: [{
                                        required: true,
                                        message: 'Это поле обязательное'
                                    }]
                                })(
                                   
                                    <SelectComponent 
                                        styles_select_classname='page-block__select'
                                        option_array={[
                                            {label: 'Нарушение 1',text: 'значение 1' },
                                            {label: 'Нарушение 2',text: 'значение 2' },
                                            {label: 'Нарушение 3',text: 'значение 3' },
                                        ]} 
                                        placeholder={"Виды нарушений"} 
                                    />
                                )}
                            </Form.Item>
                            <Form.Item 
                                key={"regional_division"} 
                                className='info-description__detail-block' 
                                >
                                 {getFieldDecorator('regional_division', {
                                    rules: [{
                                        required: true,
                                        message: 'Это поле обязательное'
                                    }]
                                })(
                                    <SelectComponent 
                                    styles_select_classname='page-block__select'
                                    option_array={[
                                        {label: 'Регион 1',text: 'значение 1' },
                                        {label: 'Регион 2',text: 'значение 2' },
                                        {label: 'Регион 3',text: 'значение 3' },
                                    ]} 
                                    placeholder={"Региональное деление:"} 
                                />
                                )}
                            </Form.Item>
                          
                      </Form>
                    </div>
                    
                  
                </div>
              
        );
}}
export default Form.create()(StatisticsDescription);