import React, { Component } from 'react';
import { Form, } from 'antd';
import * as Scroll from 'react-scroll';
import { inject, observer } from "mobx-react";
import { Element, } from 'react-scroll'
import styles from './styles/index.module.scss';
import InputComponent from '../../components/Input/';
import ButtonComponent from '../../components/Button';
import AutoComplete from "../../components/AutoComplete";

const scroll = Scroll.animateScroll;

@inject('createCategoryRiskStore')
@observer
class CreateCategoryRiskForm extends Component {
  state = {
    width: window.innerWidth,
    position: '',
  };
  componentDidMount() {
    window.addEventListener("resize", this.updateDimensions);
  }
  handleSubmit = e => {

    e.preventDefault();
    this.props.form.validateFields((err, values) => {

      if (!err) {

        this.props.createCategoryRiskStore.createCategoryRisk(values);
        if(this.state.width<=767) {
          scroll.scrollToTop()
        }

      }
    });

  };

  searchPosition = value => {
    this.props.createCategoryRiskStore.setSelectedPosition(null);
    this.props.createCategoryRiskStore.getListOfPositionSearch(value);
  }

  updateDimensions = () => {
    this.setState({
      width: window.innerWidth
    });
  }

  defineRuleForDol = (rule, value, callback) => {
    const {selectedPosition} = this.props.createCategoryRiskStore;

    if (selectedPosition) {
      callback();
      return;
    }
    callback('Это поле обязательное');
  };

  onBlur = () => {
    const {selectedPosition} =this.props.createCategoryRiskStore;
    const {position} =this.state;

    if(selectedPosition) {
      this.props.form.setFields({
        position: {
          value: position,
          // value: values.responsible_user,
        }
      })
    }


  }


  render() {
    const { getFieldDecorator } = this.props.form;

    const {positionList, setSelectedPosition, cancelButtonClicked} = this.props.createCategoryRiskStore;


    return (

      <div className='info-description'>
        <div className='info-description__title'>
          <Element name="scroll-to-subdivision-create" >
            Создание категорий рисков
          </Element>
        </div>
        <Form onSubmit={this.handleSubmit}>
          <div className='info-description__detail'>



            <Form.Item
              className='info-description__detail-block'
              label='Название:'
            >
              {getFieldDecorator('name', {
                rules: [
                  {
                    required: true,
                    message: 'Это поле обязательное'
                  }
                ]
              })(
                <InputComponent
                  class_name_of_styles={styles['users__input']}
                  // disabled={true}
                />
              )}
            </Form.Item>
            <Form.Item
               className={['info-description__detail-block','autocomlete-input'].join(" ")}
              label='Должность:'

            >
              {getFieldDecorator('position', {
                rules: [
                  {
                    validator: this.defineRuleForDol
                  }
                ]
              })(
                <AutoComplete
                  onSearch={this.searchPosition}
                  dataSource={positionList.slice()}
                  onSelect={
                    (position,{key})=>{
                      this.setState({position})
                      setSelectedPosition(key);
                    }}
                  onBlur={this.onBlur}
                />
              )}
            </Form.Item>

          </div>

          <div className={['page-block__action-block', styles['page-block__action-block-custom']].join(" ")}>
            <div className={['page-block__item', 'page-block__item-search'].join(' ')}>
              <ButtonComponent
                class_name_of_styles='page-block__button-custom-yellow'
                htmlType="submit"
                //disabled={hasErrors(getFieldsError()) || this.state.editorHtml.replace(/<(.|\n)*?>/g, '').trim().length === 0}
              >

                Создать


              </ButtonComponent>
            </div>
            <div className={['page-block__item', 'page-block__item-button'].join(' ')}>
              <ButtonComponent
                class_name_of_styles='page-block__button-custom-white'
                onClick={
                  () => {
                    cancelButtonClicked();
                    if (this.state.width <= 767) {
                      scroll.scrollToTop();
                    }
                  }
                }
              >
                Отменить
              </ButtonComponent>
            </div>
          </div>
        </Form>


      </div>

    );
  }
}


export default Form.create()(CreateCategoryRiskForm);