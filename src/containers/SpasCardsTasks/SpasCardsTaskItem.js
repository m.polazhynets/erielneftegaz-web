import React, {Component} from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import styles from './styles/index.module.scss';
import ButtonComponent from "../../components/Button";
import {inject, observer} from "mobx-react";
import {animateScroll as scroll} from "react-scroll";
import ReportRisk from "../../components/ReportRisk";

const  TASK_STATUS_CHOICES = [
  {status:'open',label: 'В работе'},
    {status:'complete',label: 'Выполнено'},
    {status:'fail',label: 'Не выполнено'}
]

const STATUSES_BLOCK = ['complete', 'fail'];

@inject('messagesStore')
@observer

class SpasCardsTaskItem extends Component {

  state = {
    width: window.innerWidth
  };

  checkStatus = (status) => {
    const index = STATUSES_BLOCK.findIndex(item => item === status);
    return !(index > -1); //block
  }

  onClickBlock = () => {
    const {onClickItem, item} = this.props;
    onClickItem(item.id);
  }

  render() {
    const {setEditTask, cancelSelectionTask, activeRiskObject, spasCardInfo, activeTaskId, setCreatingResult, setEditingResult, setEditingTaskStatus} = this.props.messagesStore;
    const {item, activeItem} = this.props;
    const {id, expiry_date, owner, performer, status, reports, text} = item;
    const {current_user_id} = spasCardInfo;

    const showButtons = activeItem === id;
    const ownerFio = (owner && owner.fio) ? owner.fio : '';
    const performerFio = (performer && performer.fio) ? performer.fio : '';

    const statusLabel = TASK_STATUS_CHOICES.find(item => item.status === status).label;

    // const isMyCardSPAS = (responsible_user) ? responsible_user.id === current_user_id : null;
    const isMyTask = (owner && owner.id) ? (owner.id === current_user_id) : false;
    const isMyReport = (performer && performer.id) ? (performer.id === current_user_id) : false;

    return (
      <div className={styles['main-container']}>
      <div onClick={this.onClickBlock}
           className={classnames([styles['spas-task-item__container']], {'info-block__active-name': activeItem === id})}>
        <div className={styles['spas-task-item__task-title']}>
          <div className={styles['spas-task-item__task-number']}>Задача №{id}</div>
          <div className={styles['spas-task-item__status']}>{statusLabel}</div>
        </div>
        <div className={styles['spas-task-item__expiry-date']}><span>Выполнить до:</span> {expiry_date}</div>
        <div className={styles['spas-task-item__expiry-date']}><span>Назначил:</span> { ownerFio}</div>
        <div className={styles['spas-task-item__expiry-date']}><span>Исполняющий:</span> {performerFio}</div>
        <div className={styles['spas-task-item__expiry-date']}>{text}</div>
      </div>
        {showButtons && <div className={['page-block__action-block', styles['page-block__action-block-custom']].join(" ")}>
          {isMyTask && <div className={['page-block__item', 'page-block__item-search'].join(' ')}>
             <ButtonComponent
              class_name_of_styles={['page-block__button-custom-yellow', styles['custom__button']].join(' ')}
              onClick={()=> {
                setEditTask();
                if (this.state.width > 767) {
                  scroll.scrollToTop()
                }
              }}
            >
              Изменить задачу
            </ButtonComponent>
          </div>}
          {isMyTask &&  <div className={['page-block__item', 'page-block__item-search'].join(' ')}>
             <ButtonComponent
              class_name_of_styles={['page-block__button-custom-yellow', styles['custom__button']].join(' ')}
              onClick={()=> {
                setEditingTaskStatus();
                if (this.state.width > 767) {
                  scroll.scrollToTop()
                }
              }}
            >
              Изменить статус
            </ButtonComponent>
          </div>}
          {((isMyReport) && (reports.length === 0)) && this.checkStatus(status) && <div className={['page-block__item', 'page-block__item-search'].join(' ')}>
            <ButtonComponent
              class_name_of_styles={['page-block__button-custom-yellow', styles['custom__button']].join(' ')}
              onClick={()=> {
                setCreatingResult();
                if (this.state.width > 767) {
                  scroll.scrollToTop()
                }
              }}
            >
              Создать отчет
            </ButtonComponent>
          </div>}
          <div className={['page-block__item', 'page-block__item-search'].join(' ')}>
            <ButtonComponent
              class_name_of_styles='page-block__button-custom-white'
              onClick={
                () => {cancelSelectionTask()}
              }
            >
              Отменить выбор
            </ButtonComponent>
          </div>
        </div>}
        {activeTaskId && activeItem === id && <ReportRisk
          onChangeItem riskItem={activeRiskObject}
          facilityId={spasCardInfo.facility.id}
          onEditButtonClick={()=>setEditingResult(id)}
          isMyReport={isMyReport}
        />}
      </div>
    );
  }
}

SpasCardsTaskItem.propTypes = {
  item: PropTypes.object,
  activeItem: PropTypes.number,
  onClickItem: PropTypes.func,
}

SpasCardsTaskItem.defaultProps = {
  item: {},
  activeItem: -1,
  onClickItem: () => {
  }
}

export default SpasCardsTaskItem;