import React, {Component} from 'react';
import PropTypes from 'prop-types';
import SpasCardsTaskItem from "./SpasCardsTaskItem";
import styles from './styles/index.module.scss';

class SpasCardsTasks extends Component {
  render() {
    const {spasCardsTasksList, onClickItem, activeItem} = this.props;
    return (
      <div className={styles['spas-tasks__container']}>
        {spasCardsTasksList.map(item => {
          return (
            <SpasCardsTaskItem key={item.id} activeItem={activeItem} onClickItem={onClickItem} item={item} />
          )
        })}

      </div>
    );
  }
}

SpasCardsTasks.propTypes = {
  spasCardsTasksList: PropTypes.array,
  activeItem: PropTypes.number,
  onClickItem: PropTypes.func,
};

SpasCardsTasks.defaultProps = {
  spasCardsTasksList: [],
  activeItem: -1,
  onClickItem: ()=>{}
}

export default SpasCardsTasks;