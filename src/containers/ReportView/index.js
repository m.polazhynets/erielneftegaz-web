import React, {Component} from 'react';
import styles from "../StructureSubdivisionList/styles/index.module.scss";
import stylesLocal from "./styles/index.module.scss";
import ButtonComponent from "../../components/Button";
import {inject, observer} from "mobx-react";

@inject('reportsStore')
@observer


class ReportView extends Component {
  render() {
    const {isSuperuser, downloadFile} = this.props.reportsStore;
    return (
        <div className='page-block'>
          {isSuperuser && <div className='page-block__action-block'>
            <div className={styles['upload-file-container']}>
              <p className={styles['title']}>Оперативная отчетность</p>
              <div className={styles['upload-file-container__title']}>Скачать csv файл Оперативной отчетности</div>
              <ButtonComponent
                class_name_of_styles = {['page-block__button-custom-yellow', stylesLocal['button']].join(' ')}
                onClick={()=>downloadFile()}
              >
                Скачать файл
              </ButtonComponent>

            </div>
          </div>}


        </div>
    );
  }
}

export default ReportView;