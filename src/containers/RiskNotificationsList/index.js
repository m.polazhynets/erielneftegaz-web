import React, {Component} from 'react';
import SearchInput from '../../components/SearchInput';
import ButtonComponent from '../../components/Button';
import RiskNotificationBlock from '../../components/RiskNotificationBlock';
import styles from './styles/index.module.scss';
import {inject, observer} from "mobx-react";
import ErrorModal from "../../modals/ErrorModal";
import {
  getIdFromCreateRoute,
  isCreatePage,
  isEditPage,
  removeCreateEditFromRoutePage,
  setCreatePage
} from "../../utils";
import * as keys from "../../keys";
import history from "../../modules/history";

@inject('riskNotificationStore')
@observer

class RiskNotificationsList extends Component {
    state = {
        activeRiskNotificationBlock: undefined,
        searchValue: '',
      };

  componentDidMount() {
    const {getAllRisks, getAllRiskNotification, setCreateRiskNotification, setActiveRiskNotificationItemId} = this.props.riskNotificationStore;

    const {s} = history.location.query;

    const editPage = isEditPage(keys.RISK_NOTIFICATIONS);
    const createPage = isCreatePage(keys.RISK_NOTIFICATIONS);

    let searchText = '';

    if(s) searchText = s;

    const promiseAllRisks = getAllRisks();
    const promiseAllRiskNotification = getAllRiskNotification(searchText);

    Promise.all([promiseAllRisks, promiseAllRiskNotification])
      .then(()=>{
        if(s){
          this.setState({searchValue: s});
        }

        if(createPage){
          setCreateRiskNotification();
        }

        if(editPage){
          const id = Number(getIdFromCreateRoute(keys.RISK_NOTIFICATIONS));
          setActiveRiskNotificationItemId(id);
        }


      });

  }

  renderNotificationList = () => {
    const {setActiveRiskNotificationItemId, activeRiskNotificationItemId, riskNotificationsList} = this.props.riskNotificationStore;

    return riskNotificationsList.map(item => {
     return <RiskNotificationBlock
        title={item.name}
        keys={item.id}
        onClick={()=>{setActiveRiskNotificationItemId(item.id)}}
        activeRiskNotificationBlock={activeRiskNotificationItemId}
        key={item.id}
      />
    })
  }

  render() {
    const {showErrorModal, setShowModalError, errorText, setCreateRiskNotification, setSearchField, isSuperuser} = this.props.riskNotificationStore;

    const { searchValue } = this.state;

      return (
            <div className={['page-block', styles['page-block-loc']].join(' ')}>
                 <div className='page-block__action-block'>
                     <div className={['page-block__item','page-block__item-search'].join(' ')}>
                        <SearchInput
                          onSearch={value =>{
                            // reset();
                            setSearchField(value);
                            removeCreateEditFromRoutePage(keys.RISK_NOTIFICATIONS);
                          }}
                          value={searchValue}
                          onChange={({target})=>{this.setState({searchValue: target.value})}}
                          classNameOfSeach = 'page-block__search'
                        />
                     </div>
                   {isSuperuser && <div className={['page-block__item','page-block__item-button'].join(' ')}>
                        <ButtonComponent onClick={()=>{
                          setCreateRiskNotification()
                          setCreatePage(keys.RISK_NOTIFICATIONS);
                        }} class_name_of_styles = 'page-block__button-custom '>
                            Создать
                        </ButtonComponent>
                     </div>}
                 </div>
               
                <div className='page-block__list'>

                  {this.renderNotificationList()}
                </div>
              <ErrorModal visibleModalError={showErrorModal} toggleModalError={()=>setShowModalError(false)} errorText={errorText}/>
            </div>
      );
    }
}


export default RiskNotificationsList;