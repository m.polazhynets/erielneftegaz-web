import React, { Component } from 'react';
import { Menu, Icon, } from 'antd';
import { withRouter } from 'react-router-dom';
import { inject, observer } from 'mobx-react';
import * as keys from '../../keys';
import history from '../../modules/history.js';
import styles from './styles/index.module.scss';


const arrPaths = [
  {key: '1', name: 'MESSAGES', path:keys.MESSAGES},
  {key: '2', name: 'SUBDIVISION', path:keys.SUBDIVISION},
  {key: '3', name: 'STRUCTURE_SUBDIVISION', path:keys.STRUCTURE_SUBDIVISION},
  {key: '4', name: 'USERS_AD', path:keys.USERS_AD},
  {key: '21', name: 'USERS_MOBILE', path:keys.USERS_MOBILE},
  {key: '20', name: 'USERS_GROUPS', path: keys.USERS_GROUPS},
  {key: '5', name: 'POSITION', path:keys.POSITION},
  {key: '6', name: 'CREATE_CATEGORY_RISKS', path:keys.CREATE_CATEGORY_RISKS},
  {key: '7', name: 'GEOPOSITION', path:keys.GEOPOSITION},
  {key: '8', name: 'RISK_NOTIFICATIONS', path:keys.RISK_NOTIFICATIONS},
  {key: '9', name: 'REPORTS', path:keys.REPORTS},
  {key: '10', name: 'STATISTICS', path:keys.STATISTICS},
  {key: '11', name: 'ADMIN_SETTINGS', path:keys.ADMIN_SETTINGS},
  {key: '12', name: 'TEXT_SECTIONS', path:keys.TEXT_SECTIONS},
  {key: '13', name: 'RISK_QUESTIONS', path:keys.RISK_QUESTIONS},
]

@inject('menuStore', 'riskMapIssuesStore', 'loginStore')
@observer

class SideMenu extends Component {

  componentDidMount() {
    this.props.loginStore.getProfile();

    const { changeActiveTab } = this.props.menuStore;
    const {pathname} = history.location;

    arrPaths.map(value => {
      if (pathname.includes(value.path)){
        changeActiveTab(value.key, value.name);
      }
      return value;
    });
  }

  handleClick = e => {
    const { menuStore, riskMapIssuesStore } = this.props;
    const { changeActiveTab } = menuStore;

    riskMapIssuesStore.reset();


    const { history, } = this.props;
   
    arrPaths.map(value => {
      if(e.key === value.key) {    
        let path = value.name;
        changeActiveTab(value.key, value.name);
        history.replace(keys[`${path}`]);
      }
      return value;
    });
  };
  render() {
    const { activeTab, } = this.props.menuStore;
    const {logOut} = this.props.loginStore;


    return (
    
      <Menu mode="inline" defaultSelectedKeys={[activeTab]} selectedKeys={[activeTab]} className={[styles['side-menu'], 'side-menu-general'].join(" ")} onClick={this.handleClick}>
        <Menu.Item key="1" >
            <span className={[styles['side-menu__item-name'], 'side-menu__item-name-general'].join(" ")}>Сообщения</span>    
        </Menu.Item>
        <Menu.Item key="2"  >
          <span className={[styles['side-menu__item-name'], 'side-menu__item-name-general'].join(" ")}>Подразделения</span>
        </Menu.Item>
        <Menu.Item key="3">
          <span className={[styles['side-menu__item-name'], 'side-menu__item-name-general'].join(" ")} >Структура подразделений</span>
        </Menu.Item>
        <Menu.Item key="21" >
          <span className={[styles['side-menu__item-name'], 'side-menu__item-name-general'].join(" ")}>Пользователи МП</span>
        </Menu.Item>
        <Menu.Item key="4" > 
            <span className={[styles['side-menu__item-name'], 'side-menu__item-name-general'].join(" ")}>Пользователи АD</span>
        </Menu.Item>
        <Menu.Item key="20" >
          <span className={[styles['side-menu__item-name'], 'side-menu__item-name-general'].join(" ")}>Группы пользователей</span>
        </Menu.Item>
        <Menu.Item key="5" >
          <span className={[styles['side-menu__item-name'], 'side-menu__item-name-general'].join(" ")}>Должности</span>
        </Menu.Item>
        <Menu.Item key="6" >
          <span className={[styles['side-menu__item-name'], 'side-menu__item-name-general'].join(" ")}>Создание категорий рисков</span>
        </Menu.Item>
        <Menu.Item key="7" >
            <span className={[styles['side-menu__item-name'], 'side-menu__item-name-general'].join(" ")}>Создание точек и указание геопозиции</span>    
        </Menu.Item>
        <Menu.Item key="8" >
          <span className={[styles['side-menu__item-name'], 'side-menu__item-name-general'].join(" ")}>Настройка оповещений по рискам</span>
        </Menu.Item>
        <Menu.Item key="9" >
          <span className={[styles['side-menu__item-name'], 'side-menu__item-name-general'].join(" ")}>Оперативная отчетность</span>
        </Menu.Item>
        {/*<Menu.Item key="10" >
          <span className={[styles['side-menu__item-name'], 'side-menu__item-name-general'].join(" ")}>Статистика и аналитика</span>
        </Menu.Item>
        <Menu.Item key="11" >
          <span className={[styles['side-menu__item-name'], 'side-menu__item-name-general'].join(" ")}>Изменение настроек администратора системы</span>
        </Menu.Item>*/}
        <Menu.Item key="12" >
            <span className={[styles['side-menu__item-name'], 'side-menu__item-name-general'].join(" ")}>Текстовые разделы</span>    
        </Menu.Item>
        <Menu.Item key="13" >
          <span className={[styles['side-menu__item-name'], 'side-menu__item-name-general'].join(" ")}>Вопросы карты рисков</span>
        </Menu.Item>
        <Menu.Item key="99" onClick={()=>logOut()} >
          <span className={styles['side-menu__logout-text']}>Выход</span>
          <Icon className={[styles['side-menu__logout-icon'],'side-menu__item-name-general'].join(" ")} type={"logout"}/>
        </Menu.Item >
      </Menu>
    
    );
  }
}


export default withRouter(SideMenu);