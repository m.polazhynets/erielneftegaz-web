import React, {Component} from 'react';
import { Pagination, } from 'antd';
import {Redirect} from 'react-router-dom';
import {  animateScroll as scroll, scroller } from 'react-scroll';
import {inject, observer} from "mobx-react";
import SearchInput from '../../components/SearchInput';
import ButtonComponent from '../../components/Button';
import SubdivisionBlock from '../../components/SubdivisionBlock';
import ErrorModal from "../../modals/ErrorModal";
import UploadFile from "../../components/UploadFile";
import history from "../../modules/history";
import styles from './styles/index.module.scss';
import * as routeKeys from '../../keys';
import {
  checkPermission,
  getIdFromCreateRoute,
  isEditPage,
  removeCreateEditFromRoutePage
} from "../../utils";


//const scroll  = Scroll.animateScroll;
@inject('subdivisionStore')
@observer

class SubdivisionList extends Component {
  
    constructor() {
      super();
      this.state = {
        width: window.innerWidth,
        searchValue: '',
        file: [],
      };
    }

    componentDidMount() {
        const { getAllSubdivisions,  setSearchField} = this.props.subdivisionStore;

        const editPage = isEditPage(routeKeys.SUBDIVISION);
        // const createPage = isCreatePage(routeKeys.SUBDIVISION);

        const {s, page} = history.location.query;

        if(s){
          let pageNumber = 1;
          if(page) pageNumber = page;
          setSearchField(s,pageNumber)
            .then(()=>{
              this.setEditPageFunc(editPage);
            })
          this.setState({searchValue: s});
        } else {
          let pageNumber = 1;
          if(page) pageNumber = page;
          getAllSubdivisions(pageNumber)
            .then(()=>{
              this.setEditPageFunc(editPage);
            })
        }
        window.addEventListener("resize", this.updateDimensions);
    }

    setEditPageFunc = (editPage) => {
      if(editPage){
        const id = getIdFromCreateRoute(routeKeys.SUBDIVISION)
        if(id){
          this.props.subdivisionStore.editSubdivision(id);
        }
      }
    }
  
  updateDimensions = () => {
    this.setState({
      width: window.innerWidth
    });
  }
  onClick = (key, id) => {
    removeCreateEditFromRoutePage(routeKeys.SUBDIVISION);
    this.props.subdivisionStore.setActiveSubdivision(key);
  }
  
  clearSearchText = () => {
    const {s} = history.location.query;
    if(s){
      this.props.subdivisionStore.setSearchField('');
      this.setState({searchValue: ''});
    }
  }
  handleChange = value => {
    const {s} = history.location.query;
    if(s) {
      this.props.subdivisionStore.setSearchField(s, value);
    } else {
      this.props.subdivisionStore.getAllSubdivisions(value);
    }


  };

  setFile = (file) => {
    this.setState({file:[file]});
  }

  onRemove = () => this.setState({file: []});

  saveFile = () => {
    const [file] = this.state.file;
    this.props.subdivisionStore.uploadCSVSubdivisions(file.originFileObj);
    this.setState({file: []})
  }


  render() {
   
    const { subdivisionStore } = this.props;
    const { 
          subdivisions, 
          activeSubdivision, 
          showErrorModal, 
          errorText, 
          setShowModalError, 
          createSubdivision, 
          setSearchField,
          page, 
          totalPages,
          creatingSubdivision,
          isSuperuser,
         } = subdivisionStore;

    const {file} = this.state;


     
    const renderSubBlock = subdivisions.map((value, index) => {
      return (
              <SubdivisionBlock
                name={value.name} 
                code={value.code} 
                keys={value.id}
                showEditBlock={isSuperuser}
                id={value.id} 
                onClick={this.onClick} 
                activeSubdivision={activeSubdivision}
                key={index}
        /> 
      )
    });

    const check = checkPermission(routeKeys.SUBDIVISION, isSuperuser);

    if(!check) return <Redirect to={routeKeys.ACCESS_PAGE} />
    

      return (
            <div className='page-block'>
                 <div className='page-block__action-block'>
                     <div className={['page-block__item','page-block__item-search'].join(' ')}>
                        <SearchInput 
                          onSearch={(value)=>{setSearchField(value)}}
                          value={this.state.searchValue} 
                          onChange={({target})=>{this.setState({searchValue: target.value})}} 
                          classNameOfSeach = 'page-block__search'
                        />
                     </div>
                   {isSuperuser && <div className={['page-block__item','page-block__item-button'].join(' ')}>
                        <ButtonComponent 
                            class_name_of_styles = 'page-block__button'
                            onClick = {()=>{ 
                              this.clearSearchText();
                              createSubdivision(true);
                              if(this.state.width<=767 && !creatingSubdivision  ) { //
                                scroll.scrollToBottom()
                              }
                              else if(this.state.width<=767 && creatingSubdivision) { // 
                                 scroller.scrollTo('scroll-to-subdivision-create', {
                                  duration: 800,
                                  delay: 0,
                                  smooth: 'easeInOutQuart'
                                })
                             }
                            }}
                        >
                            Создать
                        </ButtonComponent>
                     </div>}
                    
                 </div>
              {isSuperuser && <div className={styles['upload-file-container']}>
                <div className={styles['upload-file-container__title']}>Выберите csv файл</div>
                  <UploadFile
                    accept=".csv"
                    setFile={this.setFile}
                    onRemove={this.onRemove}
                    fileList={file}
                  />
                {file && file.length > 0 && <div className={['page-block__item','page-block__item-button'].join(' ')}>
                  <ButtonComponent
                    class_name_of_styles = 'page-block__button'
                    onClick = {this.saveFile}
                  >
                    Добавить файл
                  </ButtonComponent>
                </div>}
              </div>}

               
                <div className='page-block__list'>
                  
                    {renderSubBlock}
                  {(subdivisions && subdivisions.length > 0) && (totalPages > 1) &&
                    <Pagination
                      current={page}
                      defaultPageSize={1}
                      onChange={this.handleChange}
                      total={totalPages}
                    />}
                   
              
                 
                  
                </div>
                <ErrorModal visibleModalError={showErrorModal} toggleModalError={()=>setShowModalError(false)} errorText={errorText}/>
            </div>
      );
    }
    componentWillUnmount() {
      window.removeEventListener("resize", this.updateDimensions);
    }
}


export default SubdivisionList;