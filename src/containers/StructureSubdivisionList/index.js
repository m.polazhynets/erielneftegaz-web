import React, {Component} from 'react';
import ButtonComponent from "../../components/Button";
import classNames from 'classnames';
import UploadFile from "../../components/UploadFile";
import {inject, observer} from "mobx-react";
import TreeComponent from "../../components/TreeComponent";
import styles from './styles/index.module.scss';


@inject('structureSubdivisionStore', 'loginStore')
@observer

class StructureSubdivisonList extends Component {

  constructor(props) {
    super(props);
    this.state = {
      file: [],
    }
  }

  componentDidMount() {
    this.props.structureSubdivisionStore.getSubdivisionsStructure();
  }

  setFile = (file) => {
    this.setState({file:[file]});
  }

  onRemove = () => this.setState({file: []});

  saveFile = () => {
    const [file] = this.state.file;
    this.props.structureSubdivisionStore.uploadJSSubdivisionsStructure(file.originFileObj);
    this.setState({file: []})
  }

  renderTreeComponent = (subdivisionsStructureList) => {
    if((!subdivisionsStructureList) || (subdivisionsStructureList && (subdivisionsStructureList.length === 0 || Object.keys(subdivisionsStructureList).length === 0))) return;
    return <TreeComponent treeData={subdivisionsStructureList} />
  }

  render() {

    const {file}= this.state;
    const {subdivisionsStructureList} = this.props.structureSubdivisionStore;
    const {isSuperuser} = this.props.loginStore;

    return (
      <div className='page-block'>
        {isSuperuser && <div className='page-block__action-block'>
          <div className={styles['upload-file-container']}>
            <p className={styles['title']}>Структура подразделений</p>
            <div className={styles['upload-file-container__title']}>Выберите json файл</div>
            <UploadFile
              accept=".json"
              setFile={this.setFile}
              onRemove={this.onRemove}
              fileList={file}
            />
            {file && file.length > 0 && <div className={['page-block__item','page-block__item-button'].join(' ')}>
              <ButtonComponent
                class_name_of_styles = 'page-block__button'
                onClick = {this.saveFile}
              >
                Сохранить файл
              </ButtonComponent>
            </div>}
          </div>
        </div>}
        <div className={classNames('page-block__list',[styles['main-container']])}>
          {subdivisionsStructureList && this.renderTreeComponent(subdivisionsStructureList.slice())}
        </div>

      </div>
    );
  }
}

export default StructureSubdivisonList;
// treeData={subdivisionsStructureList[0]}