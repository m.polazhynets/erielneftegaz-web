import React, {Component} from 'react';
import styles from './styles/index.module.scss';
import {Element} from "react-scroll";
import PropTypes from "prop-types";
import ProbabilityTable from "../ProbabilityTable";

class ShowRiskNotificationView extends Component {
  render() {
    const {notificationItem} = this.props;

    if(!notificationItem.name) return  null;

    return (
      <div>
        <div className={'info-description'}>
          <div className='info-description__title'>
            <Element name="scroll-to-subdivision-create" >
              Настройка
            </Element>
          </div>
          <div className={styles['item-text']}>Название:</div>
          <div className={styles['item-text']}>{notificationItem.name}</div>
          <div className={styles['item-text']}>Оповещение:</div>
          {notificationItem.send_by_sms && <div className={styles['item-text']}>смс</div>}
          {notificationItem.send_by_email && <div className={styles['item-text']}>email</div>}



          {notificationItem.position.map((item,i) => {
              return(
                <div key={i}>
                  <div className={styles['item-text']}>Должность {`${i+1}`}</div>
                  <div className={styles['item-text']}>{item.name}</div>
                </div>
              )
            })
          }

          {notificationItem.groups.map((item,i) => {
            return(
              <div key={i}>
                <div className={styles['item-text']}>Група {`${i+1}`}</div>
                <div className={styles['item-text']}>{item.name}</div>
              </div>
            )
          })
          }


          <ProbabilityTable consequences={notificationItem.risk.x} number={notificationItem.risk.number} probability={notificationItem.risk.y} />
        </div>
      </div>
    );
  }
}

ShowRiskNotificationView.propTypes = {
  notificationItem: PropTypes.object,
}

ShowRiskNotificationView.defaultProps = {
  notificationItem: {}
}

export default ShowRiskNotificationView;