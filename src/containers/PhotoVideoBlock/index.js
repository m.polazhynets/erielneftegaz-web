import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {ImageGroup, Image} from 'react-fullscreen-image'
import styles from './styles/index.module.scss';

class PhotoVideoBlock extends Component {
    render() {
        const {data} = this.props;

        if(data.length === 0) return null;

        return (
            <div className={styles['container']}>
                <div className={styles['title']}>Фотографии и видео:</div>
                <ImageGroup>
                    <ul className={styles["images"]}>
                        {data.map(i => (
                            <li key={i}>
                                <Image
                                    src={i}
                                    alt="image"
                                />
                            </li>
                        ))}
                    </ul>
                </ImageGroup>

            </div>
        );
    }
}

PhotoVideoBlock.propTypes = {
    data: PropTypes.array,
};

PhotoVideoBlock.defaultProps = {
    data: [],
};

export default PhotoVideoBlock;
