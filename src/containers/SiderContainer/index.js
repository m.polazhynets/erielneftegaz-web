import React, {Component} from 'react';
import {Layout} from "antd";
import Logo from "../../components/Logo";
import Profile from "../../components/Profile";
import SideMenu from "../SideMenu";

import '../../styles/styles.scss';


const { Sider } = Layout;

class SiderContainer extends Component {
  render() {
    return (
      <div>
        <Sider
          breakpoint="lg"
          collapsedWidth="0"
          onBreakpoint={broken => {
          }}
          onCollapse={(collapsed, type) => {

          }}
          width={305}
        >
          <div className='side-menu-background'>
            <Logo />
            <Profile />
            <SideMenu />
          </div>

        </Sider>
      </div>
    );
  }
}

export default SiderContainer;