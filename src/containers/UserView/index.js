import React, {Component} from 'react';
import styles from './styles/index.module.scss';
import PropTypes from 'prop-types';


class UserView extends Component {

  renderFacilityArray = () => {
    const {facility} = this.props.item;

    if (!facility || (facility && facility.length === 0)) return;


    const facilityRenderItem = facility.map((item, i) => {
      return <div className={styles['info']} key={item.id}>
        <div className={styles['info__item-text']}>{item.name}</div>
      </div>
    });

    return (
      <div className={styles['info']}>
        <div className={styles['info__item-title']}>Куст/скважина:</div>
        <div className={styles['info__item-text']}>{facilityRenderItem}</div>
      </div>
    )
  }

  render() {
    const {item} = this.props;
    const {fio, phone, subdivision, position} = item;
    return (
      <div className={['info-description', styles['info'], styles['info__container']].join(' ')}>
        <div className={styles['info__container-title']}>Пользователи</div>
        {
          fio && <>
            <div className={styles['info__item-title']}>ФИО</div>
            <div className={styles['info__item-text']}>{fio}</div>
          </>
        }
        {
          position && position.name && <>
            <div className={styles['info__item-title']}>Должность:</div>
            <div className={styles['info__item-text']}>{position.name}</div>
          </>
        }
        {
          phone && <>
            <div className={styles['info__item-title']}>Мобильный телефон:</div>
            <div className={styles['info__item-text']}>{phone}</div>
          </>
        }
        {this.renderFacilityArray()}
        {
          subdivision && subdivision.name && <>
            <div className={styles['info__item-title']}>Подразделения:</div>
            <div className={styles['info__item-text']}>{subdivision.name}</div>
          </>
        }
      </div>
    );
  }
}

UserView.propTypes = {
  item: PropTypes.object,
}

UserView.defaultProps = {
  item: {}
};

export default UserView;