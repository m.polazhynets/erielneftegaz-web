import React, {Component} from 'react';
import PropTypes from 'prop-types';
import styles from './styles/index.module.scss';
import {Element} from "react-scroll";

class UsersGroupsView extends Component {
  render() {
    const {usersGroupName, usersGroupArray} = this.props;

    if(!usersGroupName) return null;

    return (
      <div className={'info-description'}>
        <div className='info-description__title'>
          <Element name="scroll-to-subdivision-create" >
            Группа пользователей
          </Element>
        </div>
        <div className={styles['item-text']}>Название:</div>
        <div className={styles['item-text']}>{usersGroupName}</div>
        {
          usersGroupArray.map((item,i) => {
            return(
              <div key={i}>
              <div className={styles['item-text']}>Пользователь {i+1}</div>
                <div className={styles['item-text']}>{item.label}</div>
              </div>
            )
          })
        }
      </div>
    );
  }
}

UsersGroupsView.propTypes = {
  usersGroupArray: PropTypes.array,
  usersGroupName: PropTypes.string,
}

UsersGroupsView.defaultProps = {
  usersGroupArray: [],
  usersGroupName: '',
}

export default UsersGroupsView;