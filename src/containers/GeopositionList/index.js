import React, {Component} from 'react';
import {Pagination} from 'antd';
import { toJS } from 'mobx';
import {Redirect} from "react-router-dom";
import {animateScroll as scroll, scroller } from 'react-scroll';
import {inject, observer} from "mobx-react";
import SearchInput from '../../components/SearchInput';
import ButtonComponent from '../../components/Button';
import GeopositionBlock from '../../components/GeopositionBlock';
import {
  checkPermission,
  getIdFromCreateRoute,
  isCreatePage,
  isEditPage,
  removeCreateEditFromRoutePage,
  setCreatePage
} from "../../utils";
import * as keys from '../../keys';
import history from "../../modules/history";




@inject('geopositionStore')
@observer

class GeopositionList extends Component {

  constructor(props) {
    super(props);
    this.state = {
      width: window.innerWidth,
      searchValue: '',
    }
  }

  componentDidMount() {

    const {s, page} = history.location.query;
    const {createFacilities, getAllFacilities, setSearchField} = this.props.geopositionStore;

    const editPage = isEditPage(keys.GEOPOSITION);
    const createPage = isCreatePage(keys.GEOPOSITION);

    if (createPage) {
      createFacilities(true);
    }


    if(s){
    let pageNumber = 1;
    if(page) pageNumber = page;
      setSearchField(s,pageNumber)
      .then(()=>{
        this.setEditPageFunc(editPage);
      })
    this.setState({searchValue: s});
    } else {
    let pageNumber = 1;
    if (page) pageNumber = page;
    getAllFacilities(pageNumber)
      .then(()=>{
        this.setEditPageFunc(editPage);
      })
    }

    window.addEventListener("resize", this.updateDimensions);

  }

  setEditPageFunc = (editPage) => {
    if(editPage){
      const id = getIdFromCreateRoute(keys.GEOPOSITION);
      if(id){
        this.props.geopositionStore.editFacilities({id});
      }
    }
  }

  handleChange = value => {
    removeCreateEditFromRoutePage(keys.GEOPOSITION);
    this.props.geopositionStore.reset();
    const {s} = history.location.query;
    if(s) {
      this.props.geopositionStore.setSearchField(s, value);
    } else {
      this.props.geopositionStore.getAllFacilities(value);
    }
  };
 
  onClick = ({key, id, name, country, region, long, lat}) => {
    this.props.geopositionStore.setActiveFacilities({value: false, id});
  }

  updateDimensions = () => {
    this.setState({
      width: window.innerWidth
    });
  }

  createFunc = () => {
    setCreatePage(keys.GEOPOSITION);
    const {creatingFacilities, createFacilities} = this.props.geopositionStore;
    createFacilities(true);
    if (this.state.width <= 767 && !creatingFacilities) {
      scroll.scrollToBottom()
    } else if (this.state.width <= 767 && creatingFacilities) {
      scroller.scrollTo('scroll-to-geoposition-create', {
        duration: 800,
        delay: 0,
        smooth: 'easeInOutQuart'
      })
    }
  }
  

  render() {
    const { activeFacilities, facilities,  page, totalPages, setSearchField, reset, isSuperuser} = this.props.geopositionStore;
    const {searchValue} = this.state;

    const check = checkPermission(keys.GEOPOSITION, isSuperuser);

    if(!check) return <Redirect to={keys.ACCESS_PAGE} />

    const geopositions  =  facilities  && Array.isArray(toJS(facilities))  && toJS(facilities).map((value, index) => {
      return (
              <GeopositionBlock
                name={value.name} 
                country={value.country} 
                region={value.region} 
                long={value.long} 
                lat={value.lat} 
                keys={value.id} 
                id={value.id}
                showEditContainer={isSuperuser}
                onClick={this.onClick}
                activeGeopositionBlock={activeFacilities}
                key={value.country+index}
        /> 
      )
    });
    

      return (
            <div className='page-block'>
                 <div className='page-block__action-block'>
                     <div className={['page-block__item','page-block__item-search'].join(' ')}>
                        <SearchInput
                          onSearch={value =>{
                            reset();
                            setSearchField(value, 1);
                            removeCreateEditFromRoutePage(keys.GEOPOSITION);
                          }}
                          value={searchValue}
                          onChange={({target})=>{this.setState({searchValue: target.value})}}
                          classNameOfSeach = 'page-block__search'
                        />
                     </div>
                   { isSuperuser && <div className={['page-block__item', 'page-block__item-button'].join(' ')}>
                       <ButtonComponent
                         class_name_of_styles='page-block__button'
                         onClick={this.createFunc}
                       >
                         Создать
                       </ButtonComponent>
                     </div>
                   }
                    
                 </div>
               
                <div className='page-block__list'>
                  
                    {  geopositions}

                  {facilities && (facilities.length>0) && (totalPages > 1) &&  <Pagination
                    current={page}
                    defaultPageSize={1}
                    onChange={this.handleChange}
                    total={totalPages}
                  />}
              
                 
                  
                </div>
            </div>
      );
    }
    componentWillUnmount() {
      window.removeEventListener("resize", this.updateDimensions);
      
    }
}


export default GeopositionList;