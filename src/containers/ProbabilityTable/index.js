import React, {Component} from 'react';
import PropTypes from 'prop-types'
import {
  ARRAY_MAP_NUMBERS, CONSEQUENCES_ARRAY, PROBABILITY_ARRAY
} from '../../constants/itemsForProbabilityTable';
import styles from './styles/index.module.scss';
import Item from "./Item";


class ProbabilityTable extends Component {


  isSelectedItem = (x, y) => {
    const {selectedItems} = this.props;

    const index = selectedItems.findIndex(item => (item.x === x && item.y === y));
    return index > -1;
  }


  renderMatrix = () => {

    const {
      isShowMode,
      onClickItem,
      probability,
      consequences
    } = this.props;


    return (
      <div className={styles['matrix']}>
        {ARRAY_MAP_NUMBERS.map((row, i) => (
          <div key={row + i} className={styles['matrix__row']}>
            {row.map((col, j) => (
              <div className={styles['matrix__item']} key={col + j}>
                <Item
                  key={j}
                  number={col}
                  active={(i === consequences && probability === j)}
                  disabledClick={isShowMode}
                  onClick={() => onClickItem(j, i)}
                  selected={this.isSelectedItem(j, i)}
                />
              </div>
            ))}
          </div>
        ))}
      </div>
    );
  }

  renderHeader = () => {
    return (
      <>
        <div className={styles['header-title']}>
          Вероятность
        </div>
        <div className={styles['header']}>

          {PROBABILITY_ARRAY.map((item, i) => {
            return (
              <div key={item + i} className={styles['header__item']}>
                {item}
              </div>
            )
          })}
        </div>
      </>
    )
  }

  renderSide = () => {
    const arrayForRender = CONSEQUENCES_ARRAY.slice().reverse();
    return (
      <div className={styles['side']}>
        <div className={styles['side__title']}>
          Последствия
        </div>
        <div className={styles['side-container']}>
          {arrayForRender.map(item => {
            return (<span key={item} className={styles['side-container__item']}>
                 {item}
             </span>)
          })}
        </div>
      </div>
    )
  }

  render() {
    const {isShowMode, consequences, probability, number} = this.props;
    const textLeft = CONSEQUENCES_ARRAY.slice()[consequences];
    const textRight = PROBABILITY_ARRAY[probability];
    return (
      <div className={styles['container']}>
        {isShowMode && <div className={styles['container__label']}>Насколько высокой была вероятность?</div>}
        <div className={styles['matrix-render']}>

          <div className={styles['matrix-render__side']}>

          </div>
          <div className={styles['']}>
            {this.renderHeader()}
            <div>
              {this.renderSide()}
              {this.renderMatrix()}
            </div>
          </div>

        </div>
        {isShowMode &&
        <div className={styles['container__status']}>Статус: <span className={styles['container__status-label']}>{`${textLeft}/${textRight} (${number})`}</span>
        </div>}
      </div>
    );
  }
}

ProbabilityTable.propTypes = {
  isShowMode: PropTypes.bool,
  onClickItem: PropTypes.func,
  probability: PropTypes.number, //x
  consequences: PropTypes.number, //y
  selectedItems: PropTypes.arrayOf(PropTypes.object),
  number: PropTypes.number,
}

ProbabilityTable.defaultProps = {
  isShowMode: true,
  onClickItem: () => {
  },
  probability: 0,
  consequences: 0,
  selectedItems: [],
  number: 1,
}

export default ProbabilityTable;
