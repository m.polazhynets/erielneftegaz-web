import React from "react";
import classNames from "classnames";
import PropTypes from "prop-types";
import { ARRAY_ITEMS_COLORS } from '../../constants/itemsForProbabilityTable';
import styles from './styles/index.module.scss';

const Item = ({active, number, onClick, disabledClick, selected,  ...props}) => {
    const color = ARRAY_ITEMS_COLORS.find(itemL => {
        if(number >= itemL.minValue && number<= itemL.maxValue){
            return true
        }
        return false;
    }).color;


    return(
        <div onClick={()=>onClick()} className={classNames(styles['block'],{[styles['block_active']]: active,
            [styles['block__can-click']]: !disabledClick , [styles['block__item-gray']]: !disabledClick && !selected  })}
             style={{backgroundColor: color, }}
        >
             <span className={classNames(styles['block__text'])}>{number}</span>
        </div>
    )
};

Item.propTypes = {
    active: PropTypes.bool,
    number: PropTypes.number.isRequired,
    onClick: PropTypes.func,
    disabledClick: PropTypes.bool,
    selected: PropTypes.bool,
};

Item.defaultProps = {
    active: false,
    number: 1,
    onClick: ()=>{},
    disabledClick: true,
    selected: false,
}

export default Item;

