import React, { Component } from 'react';
import { Modal, Form, } from 'antd';
import { inject, observer } from 'mobx-react';
import InputComponent from '../../components/Input';
import '../../styles/styles.scss';
import styles from './styles/index.module.scss';
import UploadFile from "../../components/UploadFile";

@inject('riskMapIssuesStore')
@observer

class CreateAnswerModal extends Component {

    constructor(props) {
        super(props);
        this.state={
            file: [],
        }
    }

    handleOk = e => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                const {answer} = values;
                const { file } = this.state;
                const { createAnswer, toggleModalCreateAnswer, selectedAnswerType } = this.props.riskMapIssuesStore;

                if(selectedAnswerType === 'image'){
                    let image = null;
                    if(file && file.length>0) image = file[0].originFileObj
                    createAnswer(answer, image);
                    this.setState({file: []});
                } else {
                    createAnswer(answer,null);
                }

                this.props.form.resetFields();
                toggleModalCreateAnswer(false);
            }
        })
    };

    handleCancel = e => {
        const { riskMapIssuesStore } = this.props;
        const { toggleModalCreateAnswer } = riskMapIssuesStore;
        this.props.form.resetFields();
        this.setState({file: []});
        toggleModalCreateAnswer(false);
    };

    setFile = (file) => {
        this.setState({file:[file]});
    }

    onRemove = () => this.setState({file: []});

    render() {
        const {file} = this.state;
        const { getFieldDecorator } = this.props.form;
        const { visibleModalCreateAnswer, selectedAnswerType } = this.props.riskMapIssuesStore;
    
        return (
            <div className={styles['modal']}>
                <Modal
                    title="Создать ответ"
                    visible={visibleModalCreateAnswer}
                    onOk={this.handleOk}
                    onCancel={this.handleCancel}
                    cancelText="Отменить"
                    okText="Сохранить"
                    okButtonProps={{ className: ['page-block__button', styles['modal__button']].join(' ') }}
                    cancelButtonProps={{ className: styles['modal__button-cancel'] }}
                    width={461}
                >
                    <div className={styles['modal__input-block']}>
                        <Form.Item
                            key={"answer"}
                            className='info-description__detail-block'
                   
                        >
                            {getFieldDecorator('answer', {
                                rules: [{
                                    required: true,
                                    message: 'Это поле обязательное'
                                }]
                            })(
                                <InputComponent
                                  placeholder="Введите ответ"
                                />
                            )}
                        </Form.Item>

                        {selectedAnswerType === 'image'  &&
                          <div>
                              <div>Выберите фото</div>
                              <UploadFile
                                fileList={file}
                                setFile={this.setFile}
                                onRemove={this.onRemove}
                                title={"Добавить изображение"}
                                accept="image/x-png,image/gif,image/jpeg"
                                // setFile={({file})=>this.setState({file})}
                              />
                          </div>
                            }

                    </div>
                </Modal>
            </div>
        );
    }
}

export default Form.create()(CreateAnswerModal);