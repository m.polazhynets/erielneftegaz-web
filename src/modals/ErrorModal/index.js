import React, { Component } from 'react';
import { Modal, Form, } from 'antd';
import PropTypes from 'prop-types';
import '../../styles/styles.scss';
import styles from './styles/index.module.scss';


class ErrorModal extends Component {

    handleOk = e => {
        const { toggleModalError } = this.props;
        toggleModalError(false);
    };

    handleCancel = e => {
        const { toggleModalError } = this.props;
        toggleModalError(false);
    };

    render() {
        const { visibleModalError, errorText } = this.props;

        return (
            <div className={styles['modal']}>
                <Modal
                    title="Ошибка"
                    visible={visibleModalError}
                    onOk={this.handleOk}
                    onCancel={this.handleCancel}
                    cancelButtonProps={{ style: { display: 'none' } }}
                    okText="Oк"
                    okButtonProps={{ className: ['page-block__button', styles['modal__button']].join(' ') }}
                    width={461}
                >
                    <div className={styles['modal__input-block']}>
                        {errorText}
                    </div>
                </Modal>
            </div>
        );
    }
}

ErrorModal.propTypes = {
    visibleModalError: PropTypes.bool,
    errorText: PropTypes.string,
    toggleModalError: PropTypes.func,
}

ErrorModal.defaultProps = {
    visibleModalError: false,
    errorText: '',
    toggleModalError: ()=>{},
}

export default Form.create()(ErrorModal);