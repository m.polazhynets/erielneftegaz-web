import React, { Component } from 'react';
import { Icon } from 'antd';
import { inject, observer, } from 'mobx-react';

import classNames from 'classnames';
import ReactHtmlParser from 'react-html-parser';
import { DELETE_ICON } from '../../styles/images.js';
import styles from './styles/index.module.scss';
import {setEditPage} from "../../utils";
import * as keysRoutes from "../../keys";

@inject('textSectionStore')
@observer
class TextBlock extends Component {

    render() {
        const { textSectionStore } = this.props;
        const { editTextSection, deleteTextSection, } = textSectionStore;

        const {  keys = undefined, onClick = () => { }, active = '' , title, text, id, showEditBlock = true } = this.props;
        
        return (
          <div className={styles['main-block']}>
              <div onClick={() => onClick(keys, id)}
                   className={classNames([styles['main-block__container']],'info-block', {'info-block__active-name': active === keys})}
                   id={id}>
                  <div className='edit-block'>
                      <div className='info-block__title'>
                          {title}
                      </div>
                  </div>
                  {
                      !!text && <div className='info-block__desc'>
                          {ReactHtmlParser(text)}
                      </div>
                  }
              </div>
              {showEditBlock && <div className={classNames(styles['main-block__buttons-container'],'edit-block__items')}>
                  <div onClick={() => {
                      setEditPage(keysRoutes.TEXT_SECTIONS,id);
                      editTextSection(keys)
                  }} className='edit-block__item-edit'>
                      <Icon type="edit" className='edit-block__icon'/>
                  </div>
                  <div onClick={() => {
                      deleteTextSection(keys)
                  }} className='edit-block__item-delete'>
                      <img src={DELETE_ICON} alt="delete" className='edit-block__delete-icon'/>
                  </div>
              </div>}
          </div>
        );
    }
}
export default TextBlock;