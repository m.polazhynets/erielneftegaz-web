import React, {Component} from 'react';
import PropTypes from 'prop-types';
import styles from './styles/index.module.scss';

class ErrorBlock extends Component {


    render() {
        const {errorText} = this.props;
        return (
            errorText ? (
                    <div className={styles['error-container']}>
                        <p className={styles['error-container__text']}>{errorText}</p>
                    </div>
                )
                : null
        );
    }
}

ErrorBlock.propTypes = {
    errorText: PropTypes.string,
};

ErrorBlock.defaultProps = {
    errorText: '',
};

export default ErrorBlock;