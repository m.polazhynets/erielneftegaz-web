import React, { Component } from 'react';
import classNames from 'classnames';


class StatisticsBlock extends Component {

    render() {
        const { title = "",  keys = undefined, onClick = () => { }, activeStatisticsBlock = '', } = this.props;
        return (

            <div onClick={() => { onClick(keys); }} className={classNames('info-block', { 'info-block__active-name': activeStatisticsBlock === keys })} >
                <div className='info-block__title-light'>
                    {title}
                </div>
             

            </div>

        );
    }
}

export default StatisticsBlock;