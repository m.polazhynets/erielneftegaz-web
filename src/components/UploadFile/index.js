import React, {Component} from 'react';
import PropTypes from 'prop-types';
import styles from './styles/index.module.scss';
import {Button, Icon, message, Upload} from "antd";

const params = {
  name: 'file',
  action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
  headers: {
    authorization: 'authorization-text',
  },
}

class UploadFile extends Component {

  onChange = ({file, fileList}) => {
    if(fileList.length > 0){
      this.props.setFile(file)
    }


    if (file.status !== 'uploading') {

    }
    if (file.status === 'done') {
      // message.success(`${file.name} файл успешно загружен`);
    } else if (file.status === 'error') {
      message.error(`${file.name} произошла ошибка.`);
    }
  }

  render() {
    const {title} = this.props;
    return (
      <div className={styles['report__input-block']}>
        <Upload multiple={false} onChange={this.onChange} {...params} {...this.props}>
          <Button>
            <Icon type="upload" /> {title}
          </Button>
        </Upload>
      </div>
    );
  }
}

UploadFile.propTypes = {
  setFile: PropTypes.func,
  title: PropTypes.string,
}

UploadFile.defaultProps = {
  setFile: ()=> {},
  title: 'Добавить файл'
}

export default UploadFile;