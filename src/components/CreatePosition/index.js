import React, { Component } from 'react';
import { Form, } from 'antd';
import * as Scroll from 'react-scroll';
import { inject, observer } from "mobx-react";
import {Element, } from 'react-scroll'
import styles from './styles/index.module.scss';
import InputComponent from '../Input/';
import ButtonComponent from '../Button';

const scroll = Scroll.animateScroll;

@inject('positionStore')
@observer
class CreatePosition extends Component {
    state = {
        width: window.innerWidth
    };
    componentDidMount() {
        window.addEventListener("resize", this.updateDimensions);
    }
    handleSubmit = e => {
        
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
      
          if (!err) {
           
            this.props.positionStore.addNewPosition(values.code, values.name)
            if(this.state.width<=767) {
                scroll.scrollToTop()
            }
          
          }
        });
       
    };

    updateDimensions = () => {
        this.setState({
            width: window.innerWidth
        });
    }
    render() {
        const { getFieldDecorator } = this.props.form;

        const {cancelCreatePosition} = this.props.positionStore;


        return (

            <div className='info-description'>
                <div className='info-description__title'>
                    <Element name="scroll-to-subdivision-create" >
                         Подразделения
                     </Element>
                     </div>
             <Form onSubmit={this.handleSubmit}>
                <div className='info-description__detail'>
                    

                   
                     <Form.Item
                            className='info-description__detail-block'
                            label='Название:'                       
                        >
                            {getFieldDecorator('name', {
                                rules: [
                                {
                                    required: true,
                                    message: 'Это поле обязательное'
                                }
                            ]
                            })(
                                <InputComponent
                                    class_name_of_styles={styles['users__input']}
                                   // disabled={true}
                                />
                            )}
                    </Form.Item>
                    <Form.Item
                            className='info-description__detail-block'
                            label='Код:'
                            
                        >
                            {getFieldDecorator('code', {
                                rules: [
                                {
                                    required: true,
                                    message: 'Это поле обязательное'
                                }
                            ]
                            })(
                                <InputComponent
                                    class_name_of_styles={styles['users__input']}
                                   // disabled={true}
                                />
                            )}
                        </Form.Item>
                   
                </div>
               
                <div className={['page-block__action-block', styles['page-block__action-block-custom']].join(" ")}>
                    <div className={['page-block__item', 'page-block__item-search'].join(' ')}>
                        <ButtonComponent
                            class_name_of_styles='page-block__button-custom-yellow'
                            htmlType="submit"
                        //disabled={hasErrors(getFieldsError()) || this.state.editorHtml.replace(/<(.|\n)*?>/g, '').trim().length === 0}
                        > 
                
                           Создать
                    
                         
                        </ButtonComponent>
                    </div>
                    <div className={['page-block__item', 'page-block__item-button'].join(' ')}>
                        <ButtonComponent
                            class_name_of_styles='page-block__button-custom-white'
                            onClick={
                                () => {
                                    cancelCreatePosition(false);
                                    if (this.state.width <= 767) {
                                        scroll.scrollToTop();
                                    }
                                }
                            }
                        >
                            Отменить
                        </ButtonComponent>
                    </div>
                </div>
                </Form>


            </div>

        );
    }
}


export default Form.create()(CreatePosition);