import React, { Component } from 'react';
import { Icon } from 'antd';
import { animateScroll as scroll } from 'react-scroll';
import { inject, observer } from "mobx-react";
import classNames from 'classnames';
import { DELETE_ICON } from '../../styles/images.js';
import styles from './styles/index.module.scss';

@inject('subdivisionStore')
@observer
class SubdivisionBlock extends Component {
  state = {
    width: window.innerWidth
  };
  componentDidMount() {
    window.addEventListener("resize", this.updateDimensions);

  }
  updateDimensions = () => {
    this.setState({
      width: window.innerWidth
    });
  }
  render() {

    const { keys = undefined, onClick = () => { }, activeSubdivision = '', name = "", code = "", id = undefined, showEditBlock = true } = this.props;
    const { subdivisionStore } = this.props;
    const { editSubdivision, deleteSubdivision, } = subdivisionStore;
    return (
      <div className={styles['wrapper']}>
        <div onClick={() => { onClick(keys, id, name, code) }} className={classNames('info-block', [styles['wrapper__item']], { 'info-block__active-name': activeSubdivision === keys })} >
          <div className='edit-block'>
            <div className='info-block__title' >
              Название: {name}
            </div>
          </div>
          <div className='info-block__desc'><span>Код:</span> {code}</div>
        </div>
        {showEditBlock && <div className={classNames('edit-block__items', [styles['wrapper__delete-item']])}>
          <div
            onClick={() => {
              editSubdivision(id);
              if (this.state.width <= 767) {
                scroll.scrollToBottom()
              }
            }}
            className='edit-block__item-edit'
          >
            <Icon type="edit" className='edit-block__icon' />
          </div>
          <div onClick={() => { deleteSubdivision(keys) }} className='edit-block__item-delete'>
            <img src={DELETE_ICON} alt="delete" className='edit-block__delete-icon' />
          </div>
        </div>}
      </div>

    );
  }
  componentWillUnmount() {
    window.removeEventListener("resize", this.updateDimensions);

  }

}

export default SubdivisionBlock;