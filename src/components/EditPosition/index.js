import React, { Component } from 'react';
import { Form,} from 'antd';
import { inject, observer } from "mobx-react";
import { hasErrors, } from "../../utils";
import InputComponent from '../Input/';
import ButtonComponent from '../Button';
import styles from './styles/index.module.scss';



@inject('positionStore')
@observer

class EditPosition extends Component {



    handleSubmit = e => {
        e.preventDefault();

        this.props.form.validateFields((err, values) => {
          if (!err) {
            this.props.positionStore.savePosition(values.code,values.name);
          }
        });
    };

    render() {
        const { cancelCreatePosition, activePositionCode, activePositionName, }= this.props.positionStore;
        const {
            getFieldDecorator,
            getFieldsError,
            getFieldError,
            isFieldTouched,
        } = this.props.form;

        const code = "code";
        const codeError = isFieldTouched(code) && getFieldError(code);

        const name = "name";
        const nameError = isFieldTouched(name) && getFieldError(name);
        
        return (
            
            <Form onSubmit={this.handleSubmit}>
                <div className='info-description'>
                  
                
                    <div className='info-description__detail-block'>
                        <Form.Item
                            className='info-description__detail-block'
                            validateStatus={nameError ? "error" : ""}
                            help={nameError || ""}
                            label='Название'
                        >
                            {getFieldDecorator(name, {
                                initialValue: activePositionName,
                                rules: [{
                                    required: true,
                                    message: 'Это поле обязательное'
                                }]
                            })(
                                <InputComponent
                                />
                            )}
                        </Form.Item>

                    </div>

                    <div className='info-description__detail-block'>
                        <Form.Item
                            className='info-description__detail-block'
                            validateStatus={codeError ? "error" : ""}
                            help={codeError || ""}
                            label='Код'
                        >
                            {getFieldDecorator(code, {
                                initialValue: activePositionCode,
                                rules: [{
                                    required: true,
                                    message: 'Это поле обязательное'
                                }]
                            })(
                                <InputComponent
                                //class_name_of_styles={styles['risk__input']}
                                />
                            )}
                        </Form.Item>

                    </div>
                   
                   
                       

                    <div className={['page-block__action-block', styles['page-block__action-block-custom']].join(" ")}>
                    <div className={['page-block__item','page-block__item-search'].join(' ')}>
                        <ButtonComponent 
                            class_name_of_styles = 'page-block__button-custom-yellow' 
                            htmlType="submit"  
                            disabled={hasErrors(getFieldsError())} // || activeTextDescriptionDesc.replace(/<(.|\n)*?>/g, '').trim().length === 0}
                        >
                            Сохранить
                        </ButtonComponent>
                    </div>
                    <div className={['page-block__item','page-block__item-button'].join(' ')}>
                        <ButtonComponent class_name_of_styles = 'page-block__button-custom-white' onClick = {()=>{cancelCreatePosition()}} >
                            Отменить
                        </ButtonComponent>
                    </div>
                </div>
                </div>
            </Form>




        );
    }
}


export default Form.create()(EditPosition);