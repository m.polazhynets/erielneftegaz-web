import React, { Component } from 'react';
import classNames from 'classnames';


class RiskNotificationBlock extends Component {

    render() {
        const { title = "",  keys = undefined, onClick = () => { }, activeRiskNotificationBlock = '', } = this.props;
        return (

            <div onClick={() => { onClick(keys); }} className={classNames('info-block', { 'info-block__active-name': activeRiskNotificationBlock === keys })} >
                <div className='info-block__title-light'>
                    {title}
                </div>
             

            </div>

        );
    }
}

export default RiskNotificationBlock;