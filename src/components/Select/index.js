import React, { Component } from 'react';
import { Select } from 'antd';
import PropTypes from 'prop-types';
import styles from './styles/index.module.scss';

const { Option } = Select;

class SelectComponent extends Component {

    render() {
        const { option_array, styles_select_classname, name, value, ...props } = this.props;
        return (
            <Select {...props} dropdownClassName={styles['select-dropdown']} value={value} className={[styles['select'], styles_select_classname].join(' ')} name={name}>
                {option_array.map((opt, i) => {
                  
                    return (
                        <Option key={i + opt.value} value={opt.value} >{opt.label}</Option>
                    )

                })}
            </Select>
        );
    }

}

SelectComponent.propTypes = {
    option_array: PropTypes.array,
    styles_select_classname: PropTypes.string,
};

SelectComponent.defaultProps = {
    option_array: [],
    styles_select_classname: '',
}

export default SelectComponent;
