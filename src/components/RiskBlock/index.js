import React, { Component } from 'react';
import classNames from 'classnames';


class RiskBlock extends Component {

    render() {
        const { title = "", desc = "", keys = undefined, onClick = () => { }, activeRisk = '' } = this.props;

        return (

            <div onClick={() => { onClick(keys); }} className={classNames('info-block', { 'info-block__active-name': activeRisk === keys })} >
                <div className='info-block__title'>
                    {title}
                </div>
                {
                    desc && <div className='info-block__desc'>
                        {desc}
                    </div>
                }

            </div>

        );
    }
}

export default RiskBlock;