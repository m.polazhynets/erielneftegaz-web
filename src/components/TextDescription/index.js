import React, { Component } from 'react';
import { inject, observer, } from 'mobx-react';
import ReactHtmlParser from 'react-html-parser';
import styles from './styles/index.module.scss';

@inject('textSectionStore')
@observer

class TextDescription extends Component {

    render() {
        const {textSectionStore} = this.props;
        const { activeTextDescriptionTitle, activeTextDescriptionDesc, } = textSectionStore;

        return (

            <div className={['info-description', styles['info-description-custom']].join(" ")}>
                {activeTextDescriptionTitle &&<div className='info-description__title'>
                    {activeTextDescriptionTitle}
                </div>
                }
                {activeTextDescriptionDesc && <div className='info-description__detail'>
                    {ReactHtmlParser(activeTextDescriptionDesc)}
                </div>
                }

            </div>

        );
    }
}

export default TextDescription;