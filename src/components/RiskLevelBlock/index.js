import React, { Component } from 'react';
import { Input } from 'antd';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { } from '../../styles/images.js';
import styles from './styles/index.module.scss';


class RiskLevelBlock extends Component {

    render() {
        const { title ,  keys , onClick , activeRiskBlock, active_style } = this.props;
        return (

            <div onClick={() => { onClick(keys);  }} className={classNames('info-block',  styles['info-block-custom'],  { [styles[active_style]] : activeRiskBlock === keys })} >
                <div className='info-block__title-light'>
                    {title}
                </div>
             

            </div>

        );
    }
}

RiskLevelBlock.propTypes = {
    title: PropTypes.string,
   
};

RiskLevelBlock.defaultProps = {
    title: "",
    keys: undefined,
    onClick: ()=>{},
    activeStartRiskBlock: undefined,
    activeStyle: "",
}
export default RiskLevelBlock;