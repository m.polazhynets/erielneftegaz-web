import React, { Component } from 'react';
import { Form, Select } from 'antd';
import {inject, observer} from "mobx-react";
import { Element,  animateScroll as scroll } from 'react-scroll';
import styles from './styles/index.module.scss';
import InputComponent from '../Input/';
import ButtonComponent from '../Button';


const { Option } = Select;

@inject('geopositionStore')
@observer
class GeopositionDescription extends Component {
    state = {
        width: window.innerWidth
    };
    componentDidMount() {
        window.addEventListener("resize", this.updateDimensions);
    }
    handleSubmit = e => {
        

         e.preventDefault();
         this.props.form.validateFields((err, values) => {
       
           if (!err) {
           
           this.props.geopositionStore.addFacilities(values.geoposition_name, values.geoposition_country, values.geoposition_region,  values.geoposition_longitude, values.geoposition_latitude);
          
            if(this.state.width<=767) {
                this.scrollToTop()
             }
           }
         });
        
     };

     updateDimensions = () => {
        this.setState({
          width: window.innerWidth
        });
      }
      scrollToTop = () => {
        scroll.scrollToTop();
    }

    render() {
        const { getFieldDecorator, getFieldError, isFieldTouched, } = this.props.form;
        const { geopositionStore } = this.props;
        const { createFacilities  } = geopositionStore;

        const geopositionCountry = "geoposition_country";
        const geopositionCountryError = isFieldTouched(geopositionCountry) && getFieldError(geopositionCountry);
        
        return (


            <div className='info-description'>
                <div className='info-description__title'>  
                    <Element name="scroll-to-geoposition-create" >
                        Информация о точке
                     </Element>
                </div>

                <div className='info-description__detail'>
                    <Form onSubmit={this.handleSubmit}>
                        <Form.Item
                            key={"geoposition_name"}
                            className='info-description__detail-block'
                            label='Укажите название:'
                        >
                            {getFieldDecorator('geoposition_name', {
                                rules: [{
                                    required: true,
                                    message: 'Это поле обязательное'
                                }]
                            })(
                                <InputComponent
                                    class_name_of_styles={styles['geoposition__input']}

                                />
                            )}
                        </Form.Item>
                        <Form.Item
                            key={"geoposition_country"}
                            className='info-description__detail-block'
                            label='Укажите страну:'
                            validateStatus={geopositionCountryError ? "error" : ""}
                            help={geopositionCountryError || ""}
                        >
                            {getFieldDecorator('geoposition_country', {
                                rules: [{
                                    required: true,
                                    message: 'Это поле обязательное'
                                }]
                            })(
                             
                                <Select className='page-block__select' name='geoposition_country'>
                                    <Option value="Россия">
                                        Россия
                                    </Option>
                                    
                                </Select>
                            )}
                        </Form.Item>
                        <Form.Item
                            key={"geoposition_region"}
                            className='info-description__detail-block'
                            label='Укажите регион:'
                        >
                            {getFieldDecorator('geoposition_region', {
                                rules: [{
                                    required: true,
                                    message: 'Это поле обязательное'
                                }]
                            })(
                                <InputComponent
                                    class_name_of_styles={styles['geoposition__input']}
                                />
                            )}
                        </Form.Item>

                        <Form.Item
                            key={"geoposition_longitude"}
                            className='info-description__detail-block'
                            label='Укажите долготу:'
                        >
                            {getFieldDecorator('geoposition_longitude', {
                                rules: [{
                                    required: true,
                                    message: 'Это поле обязательное'
                                }]
                            })(
                                <InputComponent
                                    class_name_of_styles={styles['geoposition__input']}
                                />
                            )}
                        </Form.Item>

                        <Form.Item
                            key={"geoposition_latitude"}
                            className='info-description__detail-block'
                            label='Укажите широту:'
                        >
                            {getFieldDecorator('geoposition_latitude', {
                                rules: [{
                                    required: true,
                                    message: 'Это поле обязательное'
                                }]
                            })(
                                <InputComponent
                                    class_name_of_styles={styles['geoposition__input']}
                                />
                            )}
                        </Form.Item>
                        <div className={['page-block__action-block', styles['page-block__action-block-custom']].join(" ")}>
                            <div className={['page-block__item', 'page-block__item-search'].join(' ')}>
                                <ButtonComponent
                                    class_name_of_styles='page-block__button-custom-yellow'
                                    htmlType="submit"
                                //disabled={hasErrors(getFieldsError()) || this.state.editorHtml.replace(/<(.|\n)*?>/g, '').trim().length === 0}
                                >
                                    Создать
                        </ButtonComponent>
                            </div>
                            <div className={['page-block__item', 'page-block__item-button'].join(' ')}>
                                <ButtonComponent
                                    class_name_of_styles='page-block__button-custom-white'
                                    onClick = {
                                        ()=>{ 
                                            createFacilities(false);
                                            if(this.state.width<=767) {
                                               this.scrollToTop()
                                            }
                                        }
                                    }
                                >
                                    Отменить
                                </ButtonComponent>
                              </div>
                        </div>


                    </Form>
                </div>


            </div>

        );
    }
    componentWillUnmount() {
        window.removeEventListener("resize", this.updateDimensions);
    }
}
export default Form.create()(GeopositionDescription);