import React, {Component} from 'react';
import {Input, Icon} from 'antd';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import {} from '../../styles/images.js';
import styles from './styles/index.module.scss';

const {Search} = Input;


class SearchInput extends Component {
  constructor() {
    super();

    this.state = {
      visibleSearch: true,
    }
  }



  onClick = () => {
    this.setState({visibleSearch: true,});
  }

  onBlurFunc = () => {
    const { onSearch, value } = this.props;
    if (!value) {
      this.setState({visibleSearch: false});
      onSearch();
    }
  }

  render() {
    const {visibleSearch} = this.state;
    const {classNameOfSeach = "", onSearch, onChange, ...props} = this.props;

    return (

      <div className={styles['search']}>
        {visibleSearch && <Search
          placeholder="поиск"
          onSearch={onSearch}
          onChange={onChange}
          onBlur={this.onBlurFunc}
          className={classnames([styles['search__item'], classNameOfSeach].join(' , '))}
          {...props}
        />
        }
        {!visibleSearch &&
        <div className={styles['search__block']} onClick={() => {
          this.onClick();
        }}><Icon type="search" style={{color: '#0D3884'}}/> Поиск </div>
        }

      </div>
    );
  }
}

SearchInput.propTypes = {
  classNameOfSeach: PropTypes.string,
  onSearch: PropTypes.func,
  value: PropTypes.string,
  onChange: PropTypes.func,
}

SearchInput.defaultProps = {
  classNameOfSeach: '',
  onSearch: ()=>{},
  value: '',
  onChange: ()=>{},
}


export default SearchInput;