import React, { Component } from "react";
import classNames from "classnames";
import {} from "../../styles/images.js";
import styles from "./styles/index.module.scss";

class Application extends Component {
  render() {
    const {
      title = "",
      type = "new",
      facility = {},
      time = "",
      keys = undefined,
      onClick = () => {},
      activeApplication = "",
      owner = {},
    } = this.props;

    let status = undefined;
    if (type === "new") status = "Новое";
    else if (type === "open") status = "Открыто";
    else if (type === "close") status = "Закрыто";

    return (
      <div
        onClick={() => {
          onClick(keys);
        }}
        className={classNames(styles["application-block"], {
          "info-block__active-name": activeApplication === keys,
        })}
      >
        <div className={styles["application-block__title"]}>
          <div className={styles["application-block__title-name"]}>
            Входящее сообщение №{title}
          </div>
          <div
            className={classNames(
              styles["application-block__title-button"],
              {
                [styles["application-block__title-button-new"]]: type === "new",
              },
              {
                [styles["application-block__title-button-work"]]:
                  type === "open",
              },
              {
                [styles["application-block__title-button-close"]]:
                  type === "close",
              }
            )}
          >
            {status}
          </div>
        </div>
        {owner.fio && (
        <div className={styles["application-block__desc"]}>
          <span>Автор:</span> {owner.fio}
        </div>)}
        {owner.position && owner.position.name && (
        <div className={styles["application-block__desc"]}>
          <span>Должность:</span> {owner.position.name}
        </div>)}
        {owner.subdivision && owner.subdivision.name && (
        <div className={styles["application-block__desc"]}>
          <span>Подразделение:</span> {owner.subdivision.name}
        </div>)}
        {facility && facility.name && (
        <div className={styles["application-block__desc"]}>
          <span>Куст/Скважина:</span> {facility.name}
        </div>)}
        <div className={styles["application-block__desc"]}>
          <span>Время и дата:</span> {time}
        </div>
      </div>
    );
  }
}

export default Application;
