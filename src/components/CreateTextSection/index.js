import React, { Component } from 'react';
import { Form } from 'antd';
import ReactQuill from 'react-quill';
import { inject, observer } from "mobx-react";
import { Element, animateScroll as scroll } from 'react-scroll'
import {hasErrors, removeCreateEditFromRoutePage,} from "../../utils";
import InputComponent from '../Input/';
import ButtonComponent from '../Button';
import * as keys from '../../keys';
import styles from './styles/index.module.scss';


const modules = {
    toolbar: [
        [{ 'header': '1' }, { 'header': '2' }, { 'font': [] }],
        [{ size: [] }],
        ['bold', 'italic', 'underline', 'strike', 'blockquote'],
        [{ 'list': 'ordered' }, { 'list': 'bullet' },
        { 'indent': '-1' }, { 'indent': '+1' }],
        // ['link', 'image', 'video'],
        ['clean']
    ],
    clipboard: {
        // toggle to add extra line breaks when pasting HTML:
        matchVisual: false,
    }
}
/* 
 * Quill editor formats
 * See https://quilljs.com/docs/formats/
 */
const formats = [
    'header', 'font', 'size',
    'bold', 'italic', 'underline', 'strike', 'blockquote',
    'list', 'bullet', 'indent',
    // 'link', 'image', 'video'
]

@inject('textSectionStore')
@observer
//activeTextDescriptionTitle: undefined,
//activeTextDescriptionDesc: undefined,
class CreateTextSection extends Component {

    state = {
     // editorHtml: this.props.textSectionStore.editingText ? this.props.textSectionStore.activeTextDescriptionDesc : '',
       editorHtml: '',
       width: window.innerWidth,
    };

    componentDidMount() {
        window.addEventListener("resize", this.updateDimensions);

      }
    
    componentDidUpdate() {

        //this.props.textSectionStore.editingText && this.props.form.setFieldsValue({'text_section_title': this.props.textSectionStore.activeTextDescriptionDesc});

        if(this.state.editorHtml.replace(/<(.|\n)*?>/g, '').trim().length === 0) { //поле пустое
            //textarea is still empty
          
        }
    }

    handleChange = (html) => {
        this.setState({ editorHtml: html });
    }

    handleSubmit = e => {

        e.preventDefault();
        this.props.form.validateFields((err, values) => {
          if (!err) {
          
            this.props.textSectionStore.addNewTextSection(values.text_section_title, this.state.editorHtml)
            if(this.state.width<=767) {
                scroll.scrollToTop()
            }
          }
        });
    };
    updateDimensions = () => {
        this.setState({
          width: window.innerWidth
        });
      }
    render() {
   
        const { textSectionStore } = this.props;
        const { cancelCreateTextSection, } = textSectionStore;
        const {
            getFieldDecorator,
            getFieldsError,
            getFieldError,
            isFieldTouched,
        } = this.props.form;

        const textSectionTitle = "text_section_title";
        const textSectionTitleError = isFieldTouched(textSectionTitle) && getFieldError(textSectionTitle);


        return (

            <Form onSubmit={this.handleSubmit}>
                <div className='info-description'>
                    <Element name="scroll-to-text-section-create" >     </Element> 
                    
                    <div className='info-description__detail-block'>
                        <Form.Item
                            key={textSectionTitle}
                            className='info-description__detail-block'
                            validateStatus={textSectionTitleError ? "error" : ""}
                            help={textSectionTitleError || ""}
                            label='Заголовок'
                        >
                            {getFieldDecorator(textSectionTitle, {
                                rules: [{
                                    required: true,
                                    message: 'Это поле обязательное'
                                }]
                            })(
                                <InputComponent
                                    placeholder="Введите заголовок"
                                //class_name_of_styles={styles['risk__input']}
                                />
                            )}
                        </Form.Item>

                    </div>
                  
                   

                         <div className='info-description__detail-block'>
                            <div className='label label-required'> Описание: </div>
                            <ReactQuill
                                theme='snow'
                                onChange={this.handleChange}
                                value={this.state.editorHtml}
                                modules={modules}
                                formats={formats}
                                className='gl-custom'
                            />
                        </div>
                      
                    <div className={['page-block__action-block', styles['page-block__action-block-custom']].join(" ")}>
                    <div className={['page-block__item','page-block__item-search'].join(' ')}>
                        <ButtonComponent 
                            class_name_of_styles = 'page-block__button-custom-yellow' 
                            htmlType="submit"  
                            disabled={hasErrors(getFieldsError()) || this.state.editorHtml.replace(/<(.|\n)*?>/g, '').trim().length === 0}
                        >
                                Создать
                        </ButtonComponent>
                    </div>
                    <div className={['page-block__item','page-block__item-button'].join(' ')}>
                        <ButtonComponent 
                            class_name_of_styles = 'page-block__button-custom-white' 
                            onClick = {()=>{
                              removeCreateEditFromRoutePage(keys.TEXT_SECTIONS);
                                cancelCreateTextSection();
                                if(this.state.width<=767) {
                                    scroll.scrollToTop()
                                }
                            }} 
                        >
                            Отменить
                        </ButtonComponent>
                    </div>
                </div>
                </div>
            </Form>




        );
    }
    componentWillUnmount() {
        window.removeEventListener("resize", this.updateDimensions);
    }
}


export default Form.create()(CreateTextSection);