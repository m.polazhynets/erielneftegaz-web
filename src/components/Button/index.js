import React, {Component} from 'react';
import { Button } from 'antd';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import styles from './styles/index.module.scss';

class ButtonComponent extends Component {
        

        render() {
                const {props} = this;
                const {class_name_of_styles} = this.props;

                return (
                    <Button  className={classnames([styles['button'], class_name_of_styles].join(' '))} {...props} />
                );
        }
};

ButtonComponent.propTypes= {
        class_name_of_styles: PropTypes.string,
}

ButtonComponent.defaultProps = {
        class_name_of_styles: '',
}

export default ButtonComponent;
