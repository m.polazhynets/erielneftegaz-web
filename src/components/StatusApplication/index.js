import React, {Component} from 'react';
import PropTypes from 'prop-types';
import AutoComplete from "../AutoComplete";
import SelectComponent from '../../components/Select';
import ButtonComponent from '../../components/Button';
import styles from './styles/index.module.scss';
import {Form} from "antd";

class StatusApplication extends Component {

  constructor(props) {
    super(props);
    this.state = {
      statusMessage: undefined,
    }
  }

  renderStatusArray = () => {
    const {statusList} = this.props;

    return statusList.map(item => {
      return (
        <div key={item.id} className={[styles['status'],styles['status__container']].join(' ')}>
          <div className={styles['status__history-block']}>
            <div className={styles['status__history-block-status']}>
              {item.status}
            </div>
            <div className={styles['status__history-block-date']}>
              {item.changed}
            </div>
          </div>
          <div className={styles['status__history-name']}>
            {item.user_name}
          </div>
        </div>
      )
    });
  }

  handleSubmit = (e) => {
    e.preventDefault();

    this.props.form.validateFields((err, values) => {
      if (!err) {
        const {setAPerformerClick} = this.props;
        setAPerformerClick();
      } else {

        this.props.form.setFields({
          responsible_user: {
            value: values.responsible_user,
            errors: [new Error('Это поле обязательное')],
          }
        })
      }
    })
  }

    render() {
        const { getFieldDecorator} = this.props.form;
        const {
          classNameButton,
          onSearch,
          dataSource,
          onSelect,
          incomingApplicationTitle,
          onClickButtonChangeStatusMessage,
          onChangeStatusMessage,
          statusMessage,
          showSetUserBlock,
          statusList,
          isMyCardSPAS,
        } = this.props;

        return (
          <div className={styles['status']}>
            <div className={styles['status__id']}>
              Входящее сообщение №{incomingApplicationTitle}
            </div>

            {isMyCardSPAS && <div>
              <div className='label'>Изменить статус сообщения</div>
              <div className={styles['status__select-block']}>
                <SelectComponent
                  value={statusMessage}
                  onChange={onChangeStatusMessage}
                  placeholder="Cтатус сообщения"
                  styles_select_classname='page-block__select'
                  option_array={[
                    {label: 'В работе', value: 'open'},
                    {label: 'Закрыто', value: 'close'},
                  ]}
                />
              </div>
            </div>}
            {statusMessage && <ButtonComponent onClick={() => {
              onClickButtonChangeStatusMessage(statusMessage)
              this.setState({statusMessage: undefined})
            }} className={[styles['status__button'], classNameButton].join(" ")}>Изменить статус </ButtonComponent>}
            {(statusList.length > 0) && <div className='label'>История статуса карты:</div>}
            {this.renderStatusArray()}

            {showSetUserBlock && <Form onSubmit={this.handleSubmit}>
              <div className='info-description__detail'>

                <Form.Item
                  className='info-description__detail-block'
                  label='Ответственный:'
                >
                  {getFieldDecorator('responsible_user', {
                    initialValue: '',
                    rules: [
                      {
                        required: true,
                        message: 'Это поле обязательное'
                      },
                    ]
                  })(
                    <AutoComplete
                      customClassName='page-block__select'
                      onSearch={onSearch}
                      dataSource={dataSource}
                      onSelect={onSelect}
                    />
                  )}
                </Form.Item>

                <ButtonComponent htmlType="submit"
                  className={[
                    styles['status__button'], classNameButton, 'page-block__button-custom-yellow'
                  ].join(" ")}
                >
                  Назначить ответственного
                </ButtonComponent>
              </div>
            </Form>}
          </div>


               );
            }
}

StatusApplication.propTypes = {
  statusList: PropTypes.array,
  setAPerformerClick: PropTypes.func,
  incomingApplicationTitle: PropTypes.string,
  onSearch: PropTypes.func,
  dataSource: PropTypes.array,
  onSelect: PropTypes.func,
  onClickButtonChangeStatusMessage: PropTypes.func,
  statusMessage: PropTypes.string,
  onChangeStatusMessage: PropTypes.func,
  showSetUserBlock: PropTypes.bool,
  isMyCardSPAS: PropTypes.bool,
}

StatusApplication.defaultProps = {
  statusList: [],
  setAPerformerClick: ()=>{},
  incomingApplicationTitle: '',
  onSearch: ()=>{},
  dataSource: [],
  onSelect: ()=>{},
  onClickButtonChangeStatusMessage: ()=>{},
  onChangeStatusMessage: ()=>{},
  showSetUserBlock: true,
  isMyCardSPAS: false,
}

export default Form.create()(StatusApplication);
