import React from 'react';
import { Tree } from 'antd';

const { TreeNode } = Tree;


class TreeComponent extends React.Component {

  renderTreeNodes = data =>{
    return (
      data.map(item => {
        if (item.children) {
          return (
            <TreeNode title={item.name} key={item.id} dataRef={item}>
              {this.renderTreeNodes(item.children)}
            </TreeNode>
          );
        }
        return <TreeNode key={item.id} {...item} dataRef={item} />;
      })
  )
  }

  render() {
    const {treeData} = this.props;


    return <Tree className={'custom-tree'}>{this.renderTreeNodes(treeData)}</Tree>;
  }
}

export default TreeComponent;