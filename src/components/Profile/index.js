import React, { Component } from 'react';
import styles from './styles/index.module.scss';
import {inject, observer} from "mobx-react";

@inject('loginStore')
@observer

class Profile extends Component {

    render() {
        const {currentUserInfo} = this.props.loginStore;

        const name = currentUserInfo.fio;
        const position = currentUserInfo && currentUserInfo.position && currentUserInfo.position.name ? currentUserInfo.position.name : ''

        return (
            <div className={styles['profile']}>

                  <div className={styles['profile__name']}>{name}</div>
                        <div className={styles['profile__position']}>{position}</div>

            </div>

        );
    }
}

export default Profile;