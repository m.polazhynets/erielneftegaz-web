import React, { Component } from 'react';
import {Icon} from 'antd';
import { animateScroll as scroll} from 'react-scroll';
import {inject, observer} from "mobx-react";
import classNames from 'classnames';
import { DELETE_ICON } from '../../styles/images.js';
import * as routeKeys from '../../keys';
import styles from './styles/index.module.scss';
import {removeCreateEditFromRoutePage, setEditPage} from "../../utils";

@inject('geopositionStore')
@observer
class GeopositionBlock extends Component {
     state = {
          width: window.innerWidth
    };
    componentDidMount() {
      window.addEventListener("resize", this.updateDimensions);

    }
    updateDimensions = () => {
     this.setState({
       width: window.innerWidth
     });
   }

   editBlockClick = () => {
     const {id=undefined, } = this.props;
     const {editFacilities } = this.props.geopositionStore;
     setEditPage(routeKeys.GEOPOSITION, id);
     editFacilities({value: true,id});
     if(this.state.width<=767) {
       scroll.scrollToBottom()
     }
   }

     render() {

          const { keys = undefined, onClick = () => { }, activeGeopositionBlock = '', name = "", country = "", region = "", long="", lat="",id=undefined, showEditContainer = true, } = this.props;
          const { deleteFacility } = this.props.geopositionStore;


       return (
         <div className={styles['wrapper']}>

           <div
             onClick={() => {
               onClick({id})
             }}
             className={classNames('info-block', {'info-block__active-name': activeGeopositionBlock === keys})}>
             <div className={classNames('edit-block')}>
               <div className='info-block__title'>
                 Буровая/База: {name}
               </div>

             </div>
             <div className='info-block__desc'><span>Страна:</span> {country}</div>
             <div className='info-block__desc'><span>Регион:</span> {region}</div>
             <div className='info-block__desc'><span>Долгота:</span> {long}</div>
             <div className='info-block__desc'><span>Широта:</span> {lat}</div>
           </div>
           {showEditContainer && <div className={classNames('edit-block__items', styles['wrapper__edit-block'])}>
             <div
               onClick={this.editBlockClick}
               className='edit-block__item-edit'
             >
               <Icon type="edit" className='edit-block__icon'/>
             </div>
             <div
               onClick={() => {
                 removeCreateEditFromRoutePage(routeKeys.GEOPOSITION);
                 deleteFacility(id);
               }}
               className='edit-block__item-delete'
             >
               <img src={DELETE_ICON} alt="delete" className='edit-block__delete-icon'/>
             </div>
           </div>}
         </div>

       );
     }
     componentWillUnmount() {
          window.removeEventListener("resize", this.updateDimensions);
          
     }

}

export default GeopositionBlock;