import React, { Component } from 'react';
import { Form, Select } from 'antd';
import { inject, observer } from "mobx-react";
import {animateScroll as scroll } from 'react-scroll';
import styles from './styles/index.module.scss';
import * as routeKeys from '../../keys';
import InputComponent from '../Input/';
import ButtonComponent from '../Button';
import {removeCreateEditFromRoutePage} from "../../utils";


const { Option } = Select;

@inject('geopositionStore')
@observer
class EditGeoposition extends Component {
    state = {
        width: window.innerWidth
    };
    componentDidMount() {
        window.addEventListener("resize", this.updateDimensions);
    }
    handleSubmit = e => {

        e.preventDefault();
        this.props.form.validateFields((err, values) => {
        
            if (!err) {

                this.props.geopositionStore.updateFacilities(values.edit_geoposition_name, values.edit_geoposition_country, values.edit_geoposition_region, values.edit_geoposition_long, values.edit_geoposition_lat);
                if(this.state.width<=767) {
                    scroll.scrollToTop();
                 }
            }
        });
    };

    render() {
        const { getFieldDecorator } = this.props.form;
        const { geopositionStore } = this.props;
        const { nameEditFacilities, countryEditFacilities, regionEditFacilities, regionLongFacilities, regionLatFacilities, reset} = geopositionStore;

        return (


            <div className='info-description'>
                <div className='info-description__title'>
                    Информация о точке
                     </div>

                <div className='info-description__detail'>
                    <Form onSubmit={this.handleSubmit}>
                        <Form.Item
                            className='info-description__detail-block'
                            label='Укажите название:'
                        >
                            {getFieldDecorator('edit_geoposition_name', {
                                initialValue: nameEditFacilities,
                                rules: [{
                                    required: true,
                                    message: 'Это поле обязательное'
                                }]
                            })(
                                <InputComponent
                                    class_name_of_styles={styles['geoposition__input']}

                                />
                            )}
                        </Form.Item>

                        <Form.Item
                            className='info-description__detail-block'
                            label='Укажите страну:'
                  
                        >
                            {getFieldDecorator('edit_geoposition_country', {
                                initialValue: countryEditFacilities,
                                rules: [{
                                    required: true,
                                    message: 'Это поле обязательное'
                                }]
                            })(
                          
                                <Select className='select-input' name='geoposition_country'>
                                    <Option value="russia">
                                        Россия
                                    </Option>
                                    
                                </Select>
                            )}
                        </Form.Item>
                        <Form.Item
                            className='info-description__detail-block'
                            label='Укажите регион:'
                        >
                            {getFieldDecorator('edit_geoposition_region', {
                               initialValue: regionEditFacilities,
                                rules: [{
                                    required: true,
                                    message: 'Это поле обязательное'
                                }]
                            })(
                                <InputComponent
                                    class_name_of_styles={styles['geoposition__input']}
                                />
                            )}
                        </Form.Item>

                        <Form.Item
                            className='info-description__detail-block'
                            label='Укажите долготу:'
                        >
                            {getFieldDecorator('edit_geoposition_long', {
                               initialValue: regionLongFacilities,
                                rules: [{
                                    required: true,
                                    message: 'Это поле обязательное'
                                }]
                            })(
                                <InputComponent
                                    class_name_of_styles={styles['geoposition__input']}
                                />
                            )}
                        </Form.Item>


                        <Form.Item
                            className='info-description__detail-block'
                            label='Укажите широту:'
                        >
                            {getFieldDecorator('edit_geoposition_lat', {
                               initialValue: regionLatFacilities,
                                rules: [{
                                    required: true,
                                    message: 'Это поле обязательное'
                                }]
                            })(
                                <InputComponent
                                    class_name_of_styles={styles['geoposition__input']}
                                />
                            )}
                        </Form.Item>

                        <div className={['page-block__action-block', styles['page-block__action-block-custom']].join(" ")}>
                            <div className={['page-block__item', 'page-block__item-search'].join(' ')}>
                                <ButtonComponent
                                    class_name_of_styles='page-block__button-custom-yellow'
                                    htmlType="submit"
                                //disabled={hasErrors(getFieldsError()) || this.state.editorHtml.replace(/<(.|\n)*?>/g, '').trim().length === 0}
                                >
                                   Сохранить
                        </ButtonComponent>
                            </div>
                            <div className={['page-block__item', 'page-block__item-button'].join(' ')}>
                                <ButtonComponent
                                    class_name_of_styles='page-block__button-custom-white'
                                    onClick={() => { 
                                        reset();
                                        removeCreateEditFromRoutePage(routeKeys.GEOPOSITION)
                                        if(this.state.width<=767) {
                                            scroll.scrollToTop();
                                         }
                                    }}
                                >
                                    Отменить
                                </ButtonComponent>
                            </div>
                        </div>


                    </Form>
                </div>


            </div>

        );
    }
    componentWillUnmount() {
        window.removeEventListener("resize", this.updateDimensions);
    }
}
export default Form.create()(EditGeoposition);