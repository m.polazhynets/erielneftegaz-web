import React, { Component } from 'react';
import { Form } from 'antd';
import * as Scroll from 'react-scroll';
import { Element, } from 'react-scroll'
import styles from './styles/index.module.scss';
import InputComponent from '../Input/';
import ButtonComponent from '../Button';
import AutoComplete from "../AutoComplete";
import MultiSelect from "../MultiSelect";
import { inject, observer } from "mobx-react";
import { PHONE_NUMBER } from "../../utils";
import { toJS } from "mobx";

const scroll = Scroll.animateScroll;

@inject('usersStore')
@observer

class UserDescription extends Component {
  state = {
    width: window.innerWidth,
    uuid: 0,
    selectedItemsFacility: [],
    subdivision: '',
    position: '',
  };
  componentDidMount() {
    window.addEventListener("resize", this.updateDimensions);
  };


  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields(({ force: true }), (err, values) => {

      if (!err) {
        const { setActiveFacility, editUser, selectedItemsFacility, clearEditMode } = this.props.usersStore;
        setActiveFacility(selectedItemsFacility);
        editUser(values);
        this.props.form.resetFields();
        clearEditMode();

        if (this.state.width <= 767) {
          scroll.scrollToTop()
        }
      }
    });

  };

  updateDimensions = () => {
    this.setState({
      width: window.innerWidth
    });
  }

  defineRuleForMultiSelect =  (rule, value, callback) => {
    const { selectedItemsFacility } = this.props.usersStore;

    if (selectedItemsFacility.slice().length > 0) {
      callback();
    } else {
      callback('Это поле обязательное');
    }
  };

  defineRuleForDol = (rule, value, callback) => {
    const { selectedPosition } = this.props.usersStore;

    if (selectedPosition) {
      callback();
      return;
    }
    callback('Это поле обязательное');
  };

  defineRuleForSubDiv = (rule, value, callback) => {
    const { selectedSubdivision } = this.props.usersStore;

    if (selectedSubdivision) {
      callback();
      return;
    }
    callback('Это поле обязательное');
  };

  handleChangeSelect = (list) => {
    this.props.usersStore.setSelectedItemsFacility(list);
  }

  phoneValidator = (rule, value, callback) => {
    const valid = PHONE_NUMBER(value)

    if (valid) callback();
    if (!valid) callback('Это поле обязательное');
  }



  searchFacility = value => this.props.usersStore.getPaginatedData('facility', value);

  searchSubdivision = value => {
    this.props.usersStore.getPaginatedData('subdivision', value);
    this.props.usersStore.setSelectedSubdivision(null);
  }

  searchPosition = value => {
    this.props.usersStore.getPaginatedData('position', value);
    this.props.usersStore.setSelectedPosition(null);
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    const { selectedUserData, cancelActive, wellList, fetching, selectedItemsFacility, positionList, subdivisionList,
      setSelectedPosition, setSelectedSubdivision,

    } = this.props.usersStore;

    const { phone, subdivision, fio, position } = toJS(selectedUserData);




    return (

      <div className='info-description'>
        <div className='info-description__title'>
          <Element name="scroll-to-user-create" >
            Пользователи
                     </Element>
        </div>
        <Form onSubmit={this.handleSubmit}>
          <div className='info-description__detail'>

            <Form.Item
              className='info-description__detail-block'
              label='ФИО:'
            >
              {getFieldDecorator('fio', {
                initialValue: fio,
                rules: [
                  {
                    required: true,
                    message: 'Это поле обязательное',
                  }
                ]
              })(
                <InputComponent
                  class_name_of_styles={styles['users__input']}
                  disabled
                />
              )}
            </Form.Item>


            <Form.Item
              className='info-description__detail-block'
              label='Должность:'
            >
              {getFieldDecorator('position', {
                initialValue: (position && position.name) ? position.name : '',
                rules: [
                  {
                    required: true,
                    message: 'Это поле обязательное'
                  },
                  {
                    validator: this.defineRuleForDol
                  }
                ]
              })(
                <AutoComplete
                  onSearch={this.searchPosition}
                  dataSource={positionList.slice()}
                  onSelect={(position, { key }) => {
                    setSelectedPosition(key);
                    this.setState({ position })
                  }}
                />
              )}
            </Form.Item>


            <Form.Item
              className='info-description__detail-block'
              label='Мобильный телефон:'
            >
              {getFieldDecorator('phone', {
                initialValue: phone,
                rules: [
                  {
                    required: true,
                    message: 'Это поле обязательное'
                  },
                  {
                    pattern: PHONE_NUMBER,
                    message: "Неправильный формат номера!"
                  }
                ]
              })(
                <InputComponent
                  class_name_of_styles={styles['users__input']}
                  maxLength={12}
                />
              )}
            </Form.Item>


            {/*                    <Form.Item
                            className='info-description__detail-block'
                            label='Буровая/База:'
                        >
                            {getFieldDecorator('branch', {
                                initialValue: facility,
                                rules: [
                                {
                                    required: true,
                                    message: 'Это поле обязательное'
                                }
                            ]
                            })(
                                <InputComponent
                                    class_name_of_styles={styles['users__input']}
                                />
                            )}
                    </Form.Item>*/}

            <Form.Item
              // className='info-description__detail-block'
              label='Куст/скважина:'
            >
              {getFieldDecorator('branch', {
                rules: [
                  // {
                  //   required: true,
                  //   message: 'Это поле обязательное'
                  // },
                  {
                    validator: this.defineRuleForMultiSelect,
                  }

                ]
              })(
                <MultiSelect
                  requestFunc={this.searchFacility}
                  options={wellList.slice()}
                  fetching={fetching}
                  handleChange={this.handleChangeSelect}
                  valueArray={selectedItemsFacility.slice()}
                />
              )}
            </Form.Item>
            <Form.Item
              className='info-description__detail-block'
              label='Подразделения:'
            >
              {getFieldDecorator('subdivision', {
                initialValue: (subdivision && subdivision.name) ? subdivision.name : '',
                rules: [
                  {
                    required: true,
                    message: 'Это поле обязательное'
                  },
                  {
                    validator: this.defineRuleForSubDiv
                  }
                ]
              })(
                <AutoComplete
                  onSearch={this.searchSubdivision}
                  dataSource={subdivisionList.slice()}
                  onSelect={
                    (subdivision, { key }) => {
                      this.setState({ subdivision })
                      setSelectedSubdivision(key);
                    }}
                />

              )}
            </Form.Item>

          </div>


          <div className={['page-block__action-block', styles['page-block__action-block-custom']].join(" ")}>
            <div className={['page-block__item', 'page-block__item-search'].join(' ')}>
              <ButtonComponent
                class_name_of_styles='page-block__button-custom-yellow'
                htmlType="submit"
              // disabled={hasErrors(getFieldsError())}
              >
                Изменить
                        </ButtonComponent>
            </div>
            <div className={['page-block__item', 'page-block__item-button'].join(' ')}>
              <ButtonComponent
                class_name_of_styles='page-block__button-custom-white'
                onClick={
                  () => {
                    cancelActive();
                    if (this.state.width <= 767) {
                      scroll.scrollToTop();
                    }
                  }
                }
              >
                Отменить
                        </ButtonComponent>
            </div>
          </div>
        </Form>


      </div>

    );
  }
}


export default Form.create()(UserDescription);
