import React, {Component} from 'react';
import { Input  } from 'antd';
import PropTypes from 'prop-types';
import { DELETE_ICON } from '../../styles/images.js';
import styles from './styles/index.module.scss';


class InputComponent extends Component {
    render() {
        const { value, disabled, class_name_of_styles, label, delete_icon, delete_func, ...props} = this.props;
            return (
                <div>
                    {label && <div className='label'> {label} </div>}
                    <Input {...props} className={[styles['input'],class_name_of_styles].join(' ')} value={value} disabled={disabled} />
                    {delete_icon && 
                        <span className={styles['delete-block']}>
                             <img src={DELETE_ICON} alt="delete" className={styles['delete-block__icon']} onClick={()=> delete_func()}/>
                        </span>
                    }
                </div>
        );
    }
}


InputComponent.propTypes = {
    delete_func: PropTypes.func,
    delete_icon: PropTypes.bool,
};

InputComponent.defaultProps = {
    delete_func: ()=>{},
    delete_icon: false,
}

export default InputComponent;
