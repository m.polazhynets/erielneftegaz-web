import React, { Component } from 'react';
import { LOGO, } from '../../styles/images.js';
import styles from './styles/index.module.scss';

class Logo extends Component {

    render() {
        return (
            <>
                <div className={styles['logo']}>
                    <img alt={"logo"} src={LOGO} className={styles['logo__block']} />
                </div>
                <div className={styles['logo-line']} />
            </>
        );
    }
}

export default Logo;