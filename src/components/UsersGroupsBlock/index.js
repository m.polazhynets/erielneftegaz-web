import React, { Component } from 'react';
import { Icon } from 'antd';
import {animateScroll as scroll} from 'react-scroll';
import {inject, observer} from "mobx-react";
import classNames from 'classnames';
import { DELETE_ICON } from '../../styles/images.js';
import styles from './styles/index.module.scss';
import {setParams} from "../../utils";

@inject('usersGroupsStore')
@observer
class UsersGroupsBlock extends Component {
     state = {
          width: window.innerWidth
    };
    componentDidMount() {
      window.addEventListener("resize", this.updateDimensions);

    }

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateDimensions);

  }

    updateDimensions = () => {
     this.setState({
       width: window.innerWidth
     });
   }
     render() {

          const { keys = undefined, onClick = () => { }, activeUsersGroups = '', name = "", id=undefined, showEditBlock=true  } = this.props;
       const { editUsersGroups, deleteUsersGroup, } = this.props.usersGroupsStore;
           return (
              <div className={styles['wrapper']}>
               <div onClick={() => { onClick(keys, id ) }}  className={classNames('info-block',[styles['wrapper__item']], { 'info-block__active-name': activeUsersGroups === keys })} >
                    <div className='edit-block'>
                         <div className='info-block__title' >
                              {name}
                         </div>
                    </div>
               </div>
                {showEditBlock && <div className={classNames('edit-block__items', [styles['wrapper__delete-item']])}>
                  <div
                    onClick={() => {
                      setParams('id','');
                      editUsersGroups(id);
                      if(this.state.width<=767) {
                        scroll.scrollToBottom()
                      }
                    }}
                    className='edit-block__item-edit'
                  >
                    <Icon type="edit" className='edit-block__icon' />
                  </div>
                  <div onClick={() => {
                    setParams('id','')
                    deleteUsersGroup(keys)
                  }}
                       className='edit-block__item-delete'>
                    <img src={DELETE_ICON} alt="delete" className='edit-block__delete-icon' />
                  </div>
                </div>}
              </div>

          );
     }


}

export default UsersGroupsBlock;