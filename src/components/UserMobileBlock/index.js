import React, { Component } from 'react';
import classNames from 'classnames';

class UserMobileBlock extends Component {

    render() {
        const { title = "", isActive= false, id = undefined, onClick = () => { }, isActiveUserBlock = '', } = this.props;

        const statusText = isActive ? 'активен' : 'заблокирован';
        return (

            <div onClick={() => { onClick(id); }} className={classNames('info-block', { 'info-block__active-name': isActiveUserBlock })} >
                <div className='info-block__title-light'>
                    {title}
                </div>
                     <div className='info-block__desc'>
                       <span>статус: </span>
                       {statusText}
                    </div>

            </div>

        );
    }
}

export default UserMobileBlock;