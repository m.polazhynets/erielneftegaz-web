import React, {Component} from 'react';
import { Input} from 'antd';
import PropTypes from 'prop-types';
import InputComponent from '../../components/Input';
import PhotoVideoBlock from "../../containers/PhotoVideoBlock";
import styles from './styles/index.module.scss';
import ButtonComponent from "../Button";
import {animateScroll as scroll} from "react-scroll";

const { TextArea } = Input;

class ReportRisk extends Component {

  state = {
    width: window.innerWidth
  };

    render() {
        const { riskItem, facilityId, onEditButtonClick, isMyReport } = this.props;


        if(!riskItem.id) return null;

        const {files, eliminated, comment} = riskItem;

        return (


                <div className={styles['report']}>
                     <div className={styles['report__id']}>
                        Отчет по устранению риска
                    </div>

                    <div className={styles['report__input-block']}>
                        <InputComponent class_name_of_styles={styles['report__input']} value={`Буровая № ${facilityId}`} disabled={true} label='Буровая/база'/>
                    </div>
                    <div className={styles['report__input-block']}>
                        <InputComponent class_name_of_styles={styles['report__input']} value={eliminated ? "Да" : "Нет"} disabled={true} label='Риск устранен?'/>
                    </div>
                    <div className={styles['report__input-block']}>
                        <div className='label'> Комментарий </div>
                        <TextArea rows={5} className={styles['report__input']} value={comment} disabled={true} />
                    </div>

                  {/*  <div className={styles['report__input-block']}>
                        <Upload {...props}>
                            <Button>
                                <Icon type="upload" /> Сохранить файл
                            </Button>
                        </Upload>
                    </div>*/}
                    <PhotoVideoBlock data={files.slice()}/>
                  {isMyReport && <div className={['page-block__action-block', styles['page-block__action-block-custom']].join(" ")}>
                    <div className={['page-block__item', 'page-block__item-search'].join(' ')}>
                      <ButtonComponent
                        class_name_of_styles='page-block__button-custom-yellow'
                        onClick={()=> {
                          onEditButtonClick();
                          if (this.state.width > 767) {
                            scroll.scrollToTop()
                          }
                        }}
                      >
                        Изменить отчет
                      </ButtonComponent>
                    </div>
                  </div>}
                </div>
               
            
               );
            }
}

ReportRisk.propTypes = {
  riskItem: PropTypes.object,
  facilityId: PropTypes.number,
  onEditButtonClick: PropTypes.func,
  isMyReport: PropTypes.bool,
}

ReportRisk.defaultProps = {
  riskItem: {},
  facilityId: 1,
  onEditButtonClick: ()=>{},
  isMyReport: false,
}

export default ReportRisk;