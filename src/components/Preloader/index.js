import React from "react";
import {Spin} from "antd";
import PropTypes from "prop-types";

import styles from './styles/index.module.scss';

const Preloader = ({loading}) => {
  return (
    loading && <div className={styles['preloader-wrapper']}>
      <Spin size="large"/>
    </div>
  );
};

Preloader.propTypes = {
  loading: PropTypes.bool,
}

Preloader.defaultProps = {
  loading: false,
}

export default Preloader;
