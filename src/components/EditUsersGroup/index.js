import React, { Component } from 'react';
import { Form } from 'antd';
import { inject, observer } from "mobx-react";
import InputComponent from '../Input/';
import ButtonComponent from '../Button';
import styles from './styles/index.module.scss';
import {Element} from "react-scroll";
import MultiSelect from "../MultiSelect";



@inject('usersGroupsStore')
@observer

class EditUsersGroup extends Component {

    defineRuleForMultiSelect = (rule, value, callback) => {
        const {selectedItemsUsers} = this.props.usersGroupsStore;

        if (selectedItemsUsers.length > 0) {
            callback();
            return;
        }
        callback('Это поле обязательное');
    };

    searchUsers = value => this.props.usersGroupsStore.searchUsers(value);

    handleChangeSelect = (list) => {
        this.props.usersGroupsStore.setSelectedItemsUsers(list);
    }
   

    handleSubmit = e => {

        e.preventDefault();
        this.props.form.validateFields({force: true},(err, values) => {
          if (!err) {
            this.props.usersGroupsStore.saveUsersGroups(values.name);
          }
        });
    };

    render() {
        const {
            cancelCreateUsersGroups,
            activeUsersGroupsName,
            usersList,
            fetching,
            selectedItemsUsers
        } =  this.props.usersGroupsStore;
        const {
            getFieldDecorator,
        } = this.props.form;

        return (


                <div className='info-description'>
                    <div className='info-description__title'>
                        <Element name="scroll-to-subdivision-create" >
                            Группа пользователей
                        </Element>
                    </div>
                         <Form onSubmit={this.handleSubmit}>
                
                    <div className='info-description__detail-block'>
                        <Form.Item
                            className='info-description__detail-block'
                            label='Название'
                        >
                            {getFieldDecorator('name', {
                                initialValue: activeUsersGroupsName,
                                rules: [{
                                    required: true,
                                    message: 'Это поле обязательное'
                                }]
                            })(
                                <InputComponent
                                />
                            )}
                        </Form.Item>

                    </div>

                    <div className='info-description__detail-block'>
                        <Form.Item
                            className='info-description__detail-block'
                            label='Пользователи'
                        >
                            {getFieldDecorator('users', {
                                rules: [
                                  {
                                    validator: this.defineRuleForMultiSelect,
                                  }

                                ]
                            })(
                              <MultiSelect
                                requestFunc={this.searchUsers}
                                options={usersList.slice()}
                                fetching={fetching}
                                handleChange={this.handleChangeSelect}
                                valueArray={selectedItemsUsers.slice()}
                              />
                            )}
                        </Form.Item>

                    </div>
                   
                   
                       

                    <div className={['page-block__action-block', styles['page-block__action-block-custom']].join(" ")}>
                    <div className={['page-block__item','page-block__item-search'].join(' ')}>
                        <ButtonComponent 
                            class_name_of_styles = 'page-block__button-custom-yellow' 
                            htmlType="submit"  
                        >
                            Изменить
                        </ButtonComponent>
                    </div>
                    <div className={['page-block__item','page-block__item-button'].join(' ')}>
                        <ButtonComponent class_name_of_styles = 'page-block__button-custom-white' onClick = {()=>{cancelCreateUsersGroups()}} >
                            Отменить
                        </ButtonComponent>
                    </div>
                </div>
            </Form>

          </div>


        );
    }
}


export default Form.create()(EditUsersGroup);