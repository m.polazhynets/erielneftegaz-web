import React from 'react';
import PropTypes from 'prop-types';
import { Select, Spin } from 'antd';
import debounce from 'lodash/debounce';

const { Option } = Select;

class MultiSelectComponent extends React.Component {
  constructor(props) {
    super(props);
    this.callRequest = debounce(this.callRequest, 800);
  }


  callRequest = value => {
    this.props.requestFunc(value);
  };




  render() {
    const {options, fetching, handleChange, placeholder, valueArray } = this.props;


    return (
      <Select
        onFocus={this.callRequest}
        mode="multiple"
        labelInValue
        value={valueArray}
        placeholder={placeholder}
        notFoundContent={fetching ? <Spin size="small" /> : null}
        filterOption={false}
        onSearch={this.callRequest}
        onChange={handleChange}
       style={{ width: '100%' }}
      >
        {options.map(opt => (
          <Option key={opt.id}>{opt.name}</Option>
        ))}
      </Select>
    );
  }
}

MultiSelectComponent.propTypes = {
  requestFunc: PropTypes.func.isRequired,
  fetching: PropTypes.bool,
  // options: PropTypes.array.isRequired,
  handleChange: PropTypes.func.isRequired,
  placeholder: PropTypes.string,
  valueArray: PropTypes.array,
}

MultiSelectComponent.defaultProps = {
  requestFunc: ()=>{},
  fetching: false,
  // options: [],
  handleChange: ()=>{},
  placeholder: 'Введите значение',
  valueArray: [],
}

export default MultiSelectComponent;
