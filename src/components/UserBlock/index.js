import React, { Component } from 'react';
import classNames from 'classnames';


class UserBlock extends Component {

    render() {
        const { title = "", desc = "", id = undefined, onClick = () => { }, isActiveUserBlock = '', } = this.props;
        return (

            <div onClick={() => { onClick(id); }} className={classNames('info-block', { 'info-block__active-name': isActiveUserBlock })} >
                <div className='info-block__title-light'>
                    {title}
                </div>
                {
                    desc && <div className='info-block__desc'>
                        {desc}
                    </div>
                }

            </div>

        );
    }
}

export default UserBlock;