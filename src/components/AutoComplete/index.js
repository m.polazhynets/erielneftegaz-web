import React, {Component} from 'react';
import PropTypes from 'prop-types';
import debounce from 'lodash/debounce';
import {AutoComplete, } from "antd";
import styles from './styles/index.module.scss';

const { Option } = AutoComplete;

class AutoCompleteComponent extends Component {

  constructor(props) {
    super(props);
    this.onSearch = debounce(this.onSearch, 800);
  }

  onSearch = (value) => {
    this.props.onSearch(value);
  }


  onFocus = () => {
    let value = this.props.value;

    if(!value) value = "";

    this.onSearch(value);

  }

  render() {
    const {dataSource, customClassName, onSelect, onChange, ...props } = this.props;

    return (
      <div>
       <AutoComplete

          className={[styles['autocomplete'], customClassName].join(' ')}
          onFocus={this.onFocus}
          onSearch={this.onSearch}
          onSelect={onSelect}
          onChange={onChange}
          {...props}
        >
         {dataSource.map(opt =>   <Option key={opt.key} value={opt.label}>
           {opt.label}
         </Option>)}
       </AutoComplete>
      </div>
    );
  }
}

AutoCompleteComponent.propTypes = {
  dataSource: PropTypes.array,
  onSearch: PropTypes.func,
  onSelect: PropTypes.func,
  onChange: PropTypes.func,
  customClassName: PropTypes.string,
};

AutoCompleteComponent.defaultProps = {
  dataSource: [],
  onSearch: ()=>{},
  onSelect: ()=>{},
  onChange: ()=>{},
  customClassName: '',
}

export default AutoCompleteComponent;