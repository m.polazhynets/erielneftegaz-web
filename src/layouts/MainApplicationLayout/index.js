import React, {Component} from 'react';
import {Layout} from 'antd';


class MainApplicationLayout extends Component {
  render() {
    const {children} = this.props;
    return (
      <Layout >
        {children}
      </Layout>
    );
  }
}

export default MainApplicationLayout;