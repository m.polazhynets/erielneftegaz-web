import React, {Component} from 'react';
import {
    Layout,
} from 'antd';
import classnames from 'classnames';
import styles from './styles/index.module.scss';


const { Content } = Layout;


class RegistrationFormLayout extends Component {
    render() {
        const { children } = this.props;
        return (
           <Content className={classnames([styles['form-layout-container']])}>
               <div className={classnames([styles['registration-form']])}>
                   {children}
               </div>
           </Content>
        );
    }
}

export default RegistrationFormLayout;