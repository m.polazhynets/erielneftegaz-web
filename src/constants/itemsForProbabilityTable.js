const GREEN_ITEM = {
    color: '#4CD964'
};

const YELLOW_ITEM = {
    color: '#FFCC00'
};

const ORANGE_ITEM = {
    color: '#FF3B30'
};

const RED_ITEM = {
    color: '#A50000'
};

export const ARRAY_ITEMS_COLORS = [
    {
        minValue: 1,
        maxValue: 3,
        color: GREEN_ITEM.color,
    },
    {
        minValue: 4,
        maxValue: 6,
        color: YELLOW_ITEM.color,
    },
    {
        minValue: 8,
        maxValue: 12,
        color: ORANGE_ITEM.color,
    },
    {
        minValue: 15,
        maxValue: 25,
        color: RED_ITEM.color,
    }
];

export const ARRAY_MAP_NUMBERS = [
    [1,2,3,4,5],
    [2,4,6,8,10],
    [3,6,9,12,15],
    [4,8,12,15,20],
    [5,10,15,20,25],
];

export const PROBABILITY_ARRAY = ["Очень маловер.", "Маловер.", "Возможная", "Вероятная", "Оч. вероятн. " ];

export const CONSEQUENCES_ARRAY= ["Легкая", "Небольшая","Средняя", "Большие", "Катастроф." ];
