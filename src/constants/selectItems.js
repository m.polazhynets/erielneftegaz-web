export const valueForSelectAnswers = [
  {label: 'Один ответ',value: "radio", },
  {label: 'Несколько ответов',value: 'checkbox' },
  {label: 'Несколько ответов (картинки)',value: 'image' },
  {label: 'Собственный ответ',value: 'text' },
  {label: 'Оценка риска',value: 'risk' },
];